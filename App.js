import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Image} from 'react-native';
import { createBottomTabNavigator, createStackNavigator } from 'react-navigation';
import Screen1 from './Components/screen1';
import Screen2 from './Components/screen2';
import Screen3 from './Components/screen3';
import Screen4 from './Components/screen4';
import Screen5 from './Components/screen5';
import TeamInfo from './Components/teamInfo';
import ScreenNews from './Components/screennews';
import Schedule from './Components/schedule';
import Roster from './Components/roster';
import PlayerInfo from './Components/playerinfo';
import GameInfo from './Components/gameinfo';
import Videos from './Components/videos';
import Players from './Components/players';
import Leaders from './Components/leaders';
import About from './Components/about';
import Youtube from './Components/youtube';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const DrawerNav = createStackNavigator({
  TeamInfo: {
    screen: TeamInfo,
  },
  Schedule: {
    screen: Schedule,
  },
  Roster: {
    screen: Roster,
  },
  PlayerInfo: {
    screen: PlayerInfo,
  }
})

const DrawerNavTest = createStackNavigator({
  Home: {
    screen: Screen1,
  },
  ScreenNews: {
    screen: ScreenNews,
  },
})

const DrawerNavTeams = createStackNavigator({
  Teams: {
    screen: Screen3,
  },
  TeamInfo: {
    screen: TeamInfo,
  },
  Schedule: {
    screen: Schedule,
  },
  Roster: {
    screen: Roster,
  },
})

const DrawerNavMore = createStackNavigator({
  More: {
    screen: Screen5,
  },
  Videos: {
    screen: Videos,
  },
  Players: {
    screen: Players,
  },
  Leaders: {
    screen: Leaders,
  },
  About: {
    screen: About,
  },
  Youtube: {
    screen: Youtube,
  },
})

const DrawerNavGames = createStackNavigator({
  Games: {
    screen: Screen2,
  },
})

const DrawerNavStandings = createStackNavigator({
  Standings: {
    screen: Screen4,
  },
})



const myTabs = createBottomTabNavigator({
  
  Latest: {
    screen: DrawerNavTest,
           navigationOptions: ({navigation}) => ({
            tabBarOnPress: ({previousScene, scene, jumpToIndex, navigation, defaultHandler}) => {
              if (navigation.state.index === 0) {
                const navigationInRoute = navigation.getChildNavigation(navigation.state.routes[0].key);
        
                if (!!navigationInRoute && navigationInRoute.isFocused() && !!navigationInRoute.state.params && !!navigationInRoute.state.params.scrollToTop) {
                  navigationInRoute.state.params.scrollToTop();                                                                        
                }
                else {
                  navigation.navigate(navigation.state.key)
                }
              }
              defaultHandler();
            },
             tabBarIcon: ({ tintColor }) => (
               <Icon name="format-list-bulleted" style={{ marginTop: 5 }} size={25} color={tintColor}/>
             )
           })
  },
  Games: {
    screen: DrawerNavGames,                                                                                                      
    navigationOptions: ({navigation}) => ({
      tabBarOnPress: ({previousScene, scene, jumpToIndex, navigation, defaultHandler}) => {
        if (navigation.state.index === 0) {
          const navigationInRoute = navigation.getChildNavigation(navigation.state.routes[0].key);
  
          if (!!navigationInRoute && navigationInRoute.isFocused() && !!navigationInRoute.state.params && !!navigationInRoute.state.params.scrollToTop) {
            navigationInRoute.state.params.scrollToTop();                                                                        
          }
          else {
            navigation.navigate(navigation.state.key)
          }
        }                 
        defaultHandler();
      },
      tabBarIcon: ({ tintColor }) => (
        <Icon name="basketball" style={{ marginTop: 5 }} size={25} color={tintColor}/>          
      )
    })

  },
  Teams: {
    screen: DrawerNavTeams,
           navigationOptions: {
             tabBarIcon: ({ tintColor }) => (
               <Icon name="account-group" style={{ marginTop: 5 }} size={25} color={tintColor}/>
             )
           }
  },
  
  Standings: {
    screen: DrawerNavStandings,
    navigationOptions: {
      tabBarIcon: ({ tintColor }) => (
        <Icon name="chart-areaspline" style={{ marginTop: 5 }} size={25} color={tintColor}/>
      )
    }
  },
  More: {
    screen: DrawerNavMore,
           navigationOptions: {
             tabBarIcon: ({ tintColor }) => (
               <Icon name="dots-horizontal" style={{ marginTop: 5 }} size={25} color={tintColor}/>
             )
           }
  },
  
}, {
  tabBarOptions: {
    activeTintColor: '#CE1A19', 
    inactiveTintColor: '#232323'
  }
});

const RootNav = createStackNavigator({
  Tabs: {
    screen: myTabs,
    navigationOptions: {
      header: null,
    }
  },
  GameInfo: {
    screen: GameInfo,
  },
  PlayerInfo: {
    screen: PlayerInfo,
  },
  Youtube: {
    screen: Youtube,
  },
});


export default RootNav;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
});
