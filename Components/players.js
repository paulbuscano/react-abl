import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, ImageBackground, ScrollView, Image, StatusBar, Button, WebView, Dimensions, ActivityIndicator, TouchableOpacity, FlatList} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Flag from 'react-native-flags';

export default class players extends Component{

  static navigationOptions = ({ navigation }) => ( {
    headerBackground: (
      <Image
        style={{width: '100%', height: '100%'}}
        source={ require('./src/bg-header.jpg') }
      />
    ),
    headerStyle: {
      backgroundColor: '#000000',
      ...Platform.select({
        ios: {     
        
        },
        android: {
          // height: 80,
          // paddingTop: 20
        },
      }),
    },
    headerTintColor: '#fff',
    title: 'PLAYERS',
    headerTitleStyle: {
      fontWeight: 'bold',
      flex: 1,
      ...Platform.select({
        ios: {     
        
        },
        android: {
         // marginTop: 200,
        
        },
      }),
    }, 
  });

  constructor(props) {
    super(props)
    this.state = {
      isLoading: true,
      dataKnights: [],
      dataSlingers: [],
      dataDreamers: [],
      dataEastern: [],
      dataBears: [],
      dataVampire: [],
      dataHeat: [],
      dataAlab: [],
      dataDragons: [],
      dataWarriors: [],
      
      dataTeams: [],
      newArray: [],
      opening: false,
      dataPlayersInfo: [],
    }; 
  }

  componentDidMount(){
    this.getData();
  }

  getData = async () => {
    let comid = 26702;

    // var playersAll = 'https://api.wh.geniussports.com/v1/basketball/statistics/personcompetition/competitions/'+ comid +'?ak=eebd8ae256142ac3fd24bd2003d28782&periodNumber=0&fields=images,personId,teamId,firstName,familyName,defaultShirtNumber,defaultPlayingPosition&limit=200';
    var playersAll = 'https://api.wh.geniussports.com/v1/basketball/competitions/'+ comid +'/playerdefaults?ak=eebd8ae256142ac3fd24bd2003d28782&fields=personId,playerTeamId,firstName,familyName,shirtNumber,playingPosition&limit=200';
   
    var requestTeams = 'https://api.wh.geniussports.com/v1/basketball/competitions/'+ comid +'/teams?ak=eebd8ae256142ac3fd24bd2003d28782&fields=images,teamId,teamCode&&limit=100';

   return fetch(playersAll).then((response) => response.json()).then((responseJson)  => {
      let people;
          function sortByKey(array, key) {
            return array.sort(function(a, b) {
                var x = a[key]; var y = b[key];
                return ((x < y) ? -1 : ((x > y) ? 1 : 0)); 
            });
        }
        people = sortByKey(responseJson.response.data, 'familyName');
        this.setState({
          dataPlayersInfo: people,
          isRefreshing: false,
        });
    }).then(()=>{ 
        return fetch(requestTeams).then((response) => response.json()).then((responseJson) => {
         this.setState({
            isLoading: false,
            isRefreshing: false,
            dataTeams: responseJson.response.data,
         });
     }).catch((error) => {
          console.error(error);
          this.setState({
            isLoading: false,
            isRefreshing: false,
            showErrorPage: true,
          });
        });
    }).catch((error) => {
        console.error(error);
        this.setState({
          isLoading: false,
          isRefreshing: false,
          showErrorPage: true,
        });
    });
  }

    _open (item) {
      if (!this.state.opening) {
        this.props.navigation.push('PlayerInfo', {playerid: item.personId, teamid: item.playerTeamId})
        this.setState({ opening: true })
        setTimeout(() => {
          this.setState({ opening: false })
        }, 1000)
      }
    }
 
    renderRow = ({item, index}) => { 

      let lastNameTest = item.familyName;
      let convertedLastName = lastNameTest.charAt(0).toUpperCase() + lastNameTest.slice(1);
      let teamLogo;
      let teamCode;
      let fullPosition;
      let test;

      let standings = this.state.dataTeams.map((ey, key) => {
          if(item.playerTeamId == ey.teamId) {
              teamLogo = ey.images.logo.T1.url;
              teamCode = ey.teamCode;
          }
      });
    
      if(item.playingPosition == 'F') {
          fullPosition = 'FORWARD';
      }
      if(item.playingPosition == 'G') {
          fullPosition = 'GUARD';
      }
      if(item.playingPosition == 'C') {
          fullPosition = 'CENTER';
      }

      if (item.images  == null) {
         // test = 'https://img.wh.sportingpulseinternational.com/16d773bd4cae752437e856ce82c06102T1.png';
          test = 'https://aseanbasketballleague.com/wp-content/uploads/abl-app/players1920/' + item.personId  + '.png';
      } else {
          test = item.images.photo.S1.url;
      }

      return <TouchableOpacity onPress={this._open.bind(this, item)}>
              <View style={ styles.row }>
                  <View style={ styles.rowInfo }>
                    <View style={ styles.authorPlayers }>
                        <Image 
                        style={ styles.userPic }
                        source={{ 
                            uri: test }}
                        />
                            <View style={{flex: 1,flexDirection: 'column',justifyContent: 'center',}}>          
                                    <View>
                                        <Text style={ styles.rosterName }>{item.firstName} {convertedLastName}</Text>            
                                    </View>
                                    <View>
                                        <Text style={ styles.rosterPosition }>#{item.shirtNumber} &#8226; {fullPosition} &#8226; {teamCode}</Text>      
                                    </View>
                            </View>
                    </View>
                    <View style={{width: 30 + '%',flexDirection: 'row', justifyContent: 'flex-end', alignItems: "center"}}>
                        <Image 
                            style={ styles.userPicTeam }
                            source={{ 
                                uri: teamLogo }}
                        />
                    </View>
                  </View>
              </View>
            </TouchableOpacity>

    }
    render() {

      console.log(this.state.dataTeams, 'datateams');

    const shadowStyle = {
        shadowOffset:{  width: 10,  height: 10,  },
        shadowColor: 'black',
        shadowOpacity: 1.0,
    }

      if(this.state.isLoading) {
        return (
          <View style={styles.containerLoading}>
            <ActivityIndicator size="large" color="#fff" />
          </View>
        )
      } else {
        
        console.log(this.state.dataPlayersInfo, 'dataPlayersInfo');

      return(
        
        <ScrollView>
            <View style={{ backgroundColor: '#e3e3e3',}}>
                <View style={ styles.group }>
                  <FlatList
                    style={styles.container}
                    data={this.state.dataPlayersInfo}
                    renderItem={this.renderRow}
                    keyExtractor={(item, index) => index.toString()}
                  />
                </View>
            </View>
        </ScrollView>  

      )
    }//end else
    }//end render
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e3e3e3',
    },
    row: {
        backgroundColor: 'white',
        marginBottom: 7,
        elevation:2,
        shadowOffset: { width: 2, height: 2 },
        shadowColor: "grey",
        shadowOpacity: 0.5,
        shadowRadius: 2,
    },
    rosterPosition: {
      color: '#999',
    },
    containerLoading: {
      flex: 1,
      backgroundColor: '#000000',
      justifyContent: 'center',
    },
    rosterName: {
      color: '#000',
      fontWeight: '700',
    },                                                                                                                                                                                                                                                                                  
    teamNick: {
        color: '#000',
        fontWeight: '700',
    },
    author: {
      flexDirection: "row", 
      alignItems: "center",
      width: 50 + '%',
    },
    authorPlayers: {
        flexDirection: "row", 
        alignItems: "center",
        width: 70 + '%',
      },
    userPic: {
      height: 40,
      width: 40,
      borderRadius: 40,
      marginRight: 10,
      borderRadius: 20,                             
    },
    userPicTeam: {
        height: 40,
        width: 40,
        marginRight: 10,
    },
    playerPic: {
      width: 40,
      height: 40,
      borderRadius: 20,
    },
    leadersScore: {
      fontSize: 18,
      color: '#1f1f1f'
    },
    subLabel: {
      fontSize: 15,
      fontWeight: '700',
      color: '#999'
    },
    recordsScore: {
      color: '#1f1f1f'
    },
    recordsLabel: {
      color: '#1f1f1f',
      fontWeight: '700'
    },
    rowLabel: {
      flexDirection: 'row',
      paddingLeft: 15,
      paddingRight: 15,
      paddingTop: 10,
    },
    rowInfo: {
      flexDirection: 'row',
      paddingLeft: 15,
      paddingRight: 15,
      paddingBottom: 10,
      paddingTop: 10,
    },
    value: {
      color: '#999'
    },
    infoLabel: {
      color: '#999',
      fontWeight: '700'
    },
    label: {
      fontWeight: '700',
      color: '#1f1f1f',
      fontSize: 18
    },
    standing: {
      color: '#fff'
    },
    teamName: {
      color: '#ffffff',
      fontSize: 18,
      fontWeight: '700'
    },
    linearGradient: {
      backgroundColor: "transparent",
      position: "absolute",
      top: 0,
      bottom: 0,
      left: 0,
      right: 0
    },
    teamPic: {
      width: 80,
      height: 80
    }
});