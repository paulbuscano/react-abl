import React, {Component} from 'react';
import {TouchableWithoutFeedback, PixelRatio, Platform, StyleSheet, Text, View, ImageBackground, ScrollView, Image, StatusBar, Button, WebView, ActivityIndicator, TouchableOpacity, Linking, FlatList} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import moment from 'moment';
import YouTube from 'react-native-youtube';

export default class youtube extends Component {

  static navigationOptions = ({ navigation }) => ({
    headerBackground: (
      <Image
        style={{ width: '100%', height: '100%' }}
        source={require('./src/bg-header.jpg')}
      />
    ),
    headerStyle: {
      backgroundColor: '#000000',
      ...Platform.select({
        ios: {

        },
        android: {
          // height: 80,
          // paddingTop: 20
        },
      }),
    },
    headerTintColor: '#fff',
    title: 'VIDEOS',
    headerTitleStyle: {
      fontWeight: 'bold',
      flex: 1,
      ...Platform.select({
        ios: {

        },
        android: {
          // marginTop: 200,

        },
      }),
    },
  });
      
    constructor(props) {
        super(props);
        this.state = {
            video: this.props.navigation.state.params.video,
            isReady: false,
            status: null,
            quality: null,
            error: null,
            isPlaying: true,
            isLooping: true,
            duration: 0,
            currentTime: 0,
            fullscreen: false,
            containerMounted: false,
            containerWidth: null,
            visible: true,
            opacity: 1,
            opening: false,
            isMounted: false,
        }
    }


    componentDidMount() {
      this.setState({isMounted: true})
   }

    componentWillUnmount(){
      this.state.isMounted = false
    }


    hideSpinner() {
      this.setState({
        visible: false,
        opacity: 0,
      });
    }

    render() {
     
      var browser;

      if (Platform.OS === 'ios') {

        browser =   <WebView  
                      originWhitelist={['*']}
                      source={{uri: "https://www.youtube.com/embed/" + this.state.video}}
                      style={{marginTop: 0, backgroundColor: '#000000',}}
                      onLoad={() => this.hideSpinner()}
                      />
                      {this.state.visible && (
                        <ActivityIndicator
                          animating={true}
                          style={{ position: "absolute", top: 50 + '%', left: 45 + '%', alignItems: 'center', justifyContent: 'center' , opacity: this.state.opacity}}
                          size="large"
                          color="#000000"
                    />
                    )}
      } else {
        browser  = <YouTube
                    videoId={this.state.video}
                    apiKey="AIzaSyBU7865vDB9p9APyxKtJC2bjrpf7XqiVbo"  // The YouTube video ID
                    play={true}             // control playback of video with true/false
                    fullscreen={true}       // control whether the video should play in fullscreen or inline
                    loop={true}            // control whether the video should loop when ended
                    onReady={e => this.setState({ isReady: true })}
                    onChangeState={e => this.setState({ status: e.state })}
                    onChangeQuality={e => this.setState({ quality: e.quality })}
                    onError={e => this.setState({ error: e.error })}
                    style={{ alignSelf: 'stretch', height: 300 }}
                    />
                   
      }  

        return(
            <View style={styles.container}> 
                  {browser}
            </View>
        )
    }

}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#000000',
      overflow:'hidden',
    },       
    text: {
      padding: 10,
    },      
    textBorder: {
      borderBottomWidth: 1, 
      borderBottomColor: '#e3e3e3'
    },   
});