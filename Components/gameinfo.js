import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, ImageBackground, ScrollView, Image, StatusBar, Button, WebView, Dimensions, TouchableOpacity, ActivityIndicator, Linking, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Flag from 'react-native-flags';
import moment from 'moment';
import axios from 'axios';

export default class gameinfo extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerBackground: (
            <Image
                style={{ width: '100%', height: '100%' }}
                source={require('./src/bg-header.jpg')}
            />
        ),
        headerStyle: {
            backgroundColor: 'transparent',
            ...Platform.select({
                ios: {

                },
                android: {
                    //   height: 80,
                    //   paddingTop: 20
                },
            }),
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
            flex: 1,
            ...Platform.select({
                ios: {

                },
                android: {
                    // marginTop: 200,

                },
            }),
        },
    });

    constructor(props) {
        super(props)

        this.state = {
            activeIndex: 0,
            visible: true,
            matchstatus: this.props.navigation.state.params.matchstatus,
            matchid: this.props.navigation.state.params.matchid,
            matchtime: this.props.navigation.state.params.matchtime,
            lefteam: this.props.navigation.state.params.lefteam,
            rightteam: this.props.navigation.state.params.rightteam,
            isLoading: true,
            dataSource: [],
            standingInfo: [],
            matchInfo: [],
            leftInfo: [],
            rightInfo: [],
            boxLeftInfo: [],
            boxRightInfo: [],
            playInfo: [],
            playInfoLeft: [],
            playInfoRight: [],

            playInfotest: [],
            opening: false,
            leadersLeft: [],
            leadersRight: [],

            //for schedule
            headtoheadInfo: [],
            seasonleaderpointsleftInfo: [],
            seasonleaderassistsleftInfo: [],
            seasonleaderreboundsleftInfo: [],
            seasonleaderpointsrightInfo: [],
            seasonleaderassistsrightInfo: [],
            seasonleaderreboundsrightInfo: [],

            //boxscore test                   
            boxInfoLeft: [],
            boxInfoRight: [],
            showMe: true,
            videoInfo: [],  
            isMounted: false,    
        }
    }

    async componentDidMount() {
        this.setState({isMounted: true})
        if (this.state.matchstatus == "SCHEDULED") {
            this.getSchedule();
        }

        if (this.state.matchstatus == "COMPLETE" || this.state.matchstatus == "FINISHED" || this.state.matchstatus == "IN_PROGRESS") {
            this.getComplete();
        }
    }

    componentWillUnmount(){
        this.state.isMounted = false
    }

    getVideo = async () => {

        const request_video = await axios.get('https://aseanbasketballleague.com/wp-json/wp/v2/youtubeticket/');

        let what = request_video.data;
        let testBoxLeft = what.map((val, key) => {

            let str = val.link;
            let res = str.split("/");
            let testId = res[4];

            if (testId == this.state.matchid) {

                let renderText = val.content.rendered;
                let renderTrim = renderText.substring(renderText.lastIndexOf("<p>")+3,renderText.lastIndexOf("</p>"));
                if (this.state.isMounted) {
                this.setState({
                    videoInfo: renderTrim,
                })
            }
            }
        });

        console.log(this.state.videoInfo, 'videoInfo');
    }

    _openVideo() {
        if (!this.state.opening) {
          this.props.navigation.push('Youtube', { video: this.state.videoInfo })
          this.setState({ opening: true })
          setTimeout(() => {
            this.setState({ opening: false })
          }, 1000)
        }
    }

    getSchedule = async () => {

        let comid = 26702;

        try {

            const request_1_url = await axios.get('https://api.wh.geniussports.com/v1/basketball/matches/' + this.state.matchid + '/competitors?ak=eebd8ae256142ac3fd24bd2003d28782&fields=teamId,completionStatus,scoreString');
            const request_standing = await axios.get('https://api.wh.geniussports.com/v1/basketball/competitions/' + comid + '/standings?ak=eebd8ae256142ac3fd24bd2003d28782&fields=images,competitorId,won,lost');

            //for schedule
            const request_headtohead = await axios.get('https://api.wh.geniussports.com/v1/basketball/matches/headtohead/teams/' + this.state.lefteam + '/versus/' + this.state.rightteam + '?ak=eebd8ae256142ac3fd24bd2003d28782&competitionId=26702&fields=matchNumber,matchStatus,matchTime,competitors,matchId');
            const request_seasonLeadersPointsLeft = await axios.get('https://api.wh.geniussports.com/v1/basketball/competitions/' + comid + '/leaders/sPointsAverage?ak=eebd8ae256142ac3fd24bd2003d28782&teamId=' + this.state.lefteam + '&periodNumber=0&fields=images,familyName,sPointsAverage,firstName&limit=1');
            const request_seasonLeadersAssistsLeft = await axios.get('https://api.wh.geniussports.com/v1/basketball/competitions/' + comid + '/leaders/sAssistsAverage?ak=eebd8ae256142ac3fd24bd2003d28782&teamId=' + this.state.lefteam + '&periodNumber=0&fields=images,familyName,sAssistsAverage,firstName&limit=1');
            const request_seasonLeadersReboundsLeft = await axios.get('https://api.wh.geniussports.com/v1/basketball/competitions/' + comid + '/leaders/sReboundsTotalAverage?ak=eebd8ae256142ac3fd24bd2003d28782&teamId=' + this.state.lefteam + '&periodNumber=0&fields=images,familyName,sReboundsTotalAverage,firstName&limit=1');

            const request_seasonLeadersPointsRight = await axios.get('https://api.wh.geniussports.com/v1/basketball/competitions/' + comid + '/leaders/sPointsAverage?ak=eebd8ae256142ac3fd24bd2003d28782&teamId=' + this.state.rightteam + '&periodNumber=0&fields=images,familyName,sPointsAverage,firstName&limit=1');
            const request_seasonLeadersAssistsRight = await axios.get('https://api.wh.geniussports.com/v1/basketball/competitions/' + comid + '/leaders/sAssistsAverage?ak=eebd8ae256142ac3fd24bd2003d28782&teamId=' + this.state.rightteam + '&periodNumber=0&fields=images,familyName,sAssistsAverage,firstName&limit=1');
            const request_seasonLeadersReboundsRight = await axios.get('https://api.wh.geniussports.com/v1/basketball/competitions/' + comid + '/leaders/sReboundsTotalAverage?ak=eebd8ae256142ac3fd24bd2003d28782&teamId=' + this.state.rightteam + '&periodNumber=0&fields=images,familyName,sReboundsTotalAverage,firstName&limit=1');

            if (this.state.isMounted) {
            this.setState({
                dataSource: request_1_url.data.response.data,
                standingInfo: request_standing.data.response.data,
                //for schedule
                headtoheadInfo: request_headtohead.data.response.data,
                seasonleaderpointsleftInfo: request_seasonLeadersPointsLeft.data.response.data,
                seasonleaderpointsrightInfo: request_seasonLeadersPointsRight.data.response.data,
                seasonleaderassistsleftInfo: request_seasonLeadersAssistsLeft.data.response.data,
                seasonleaderassistsrightInfo: request_seasonLeadersAssistsRight.data.response.data,
                seasonleaderreboundsleftInfo: request_seasonLeadersReboundsLeft.data.response.data,
                seasonleaderreboundsrightInfo: request_seasonLeadersReboundsRight.data.response.data,
                isLoading: false,
            })
        }
        
        } catch (error) {
            console.log(error);
            this.setState({
                isLoading: false,
                showErrorPage: true,
            });
        }
    }

    _open(item) {
        if (!this.state.opening) {
            this.props.navigation.push('GameInfo', { matchstatus: item.matchStatus, matchid: item.matchId, matchtime: item.matchTime, lefteam: item.competitors[0].competitorId, rightteam: item.competitors[1].competitorId })
            this.setState({ opening: true })
            setTimeout(() => {
                this.setState({ opening: false })
            }, 1000)
        }
    }

    getComplete = async () => {

        let comid = 26702;

        try {

            const request_1_url = await axios.get('https://api.wh.geniussports.com/v1/basketball/matches/' + this.state.matchid + '/competitors?ak=eebd8ae256142ac3fd24bd2003d28782&fields=teamId,completionStatus,scoreString');
            const request_standing = await axios.get('https://api.wh.geniussports.com/v1/basketball/competitions/' + comid + '/standings?ak=eebd8ae256142ac3fd24bd2003d28782&fields=images,competitorId,won,lost');
            const request_match = await axios.get('https://api.wh.geniussports.com/v1/basketball/statistics/teammatch/matches/' + this.state.matchid + '?ak=eebd8ae256142ac3fd24bd2003d28782&periodType=REGULAR&periodNumber=0&fields=sFreeThrowsPercentage,sThreePointersPercentage,sFieldGoalsPercentage,sAssists,sReboundsTotal,sReboundsOffensive,sReboundsDefensive,sSteals,sBlocks,sTurnovers,sFoulsTotal');
            const request_quarterLeft = await axios.get('https://api.wh.geniussports.com/v1/basketball/statistics/teammatch/matches/' + this.state.matchid + '/teams/' + this.state.lefteam + '?ak=eebd8ae256142ac3fd24bd2003d28782&fields=linkDetail,sPoints');
            const request_quarterRight = await axios.get('https://api.wh.geniussports.com/v1/basketball/statistics/teammatch/matches/' + this.state.matchid + '/teams/' + this.state.rightteam + '?ak=eebd8ae256142ac3fd24bd2003d28782&fields=linkDetail,sPoints');

            // const request_boxLeft = await axios.get('https://api.wh.geniussports.com/v1/basketball/statistics/personmatch/matches/' + this.state.matchid + '/teams/' + this.state.lefteam + '?ak=eebd8ae256142ac3fd24bd2003d28782&limit=100');
            // const request_boxRight = await axios.get('https://api.wh.geniussports.com/v1/basketball/statistics/personmatch/matches/' + this.state.matchid + '/teams/' + this.state.rightteam + '?ak=eebd8ae256142ac3fd24bd2003d28782&limit=100');

            //const request_play = await axios.get('https://api.wh.geniussports.com/v1/basketball/matches/'+ this.state.matchid +'/actions?ak=eebd8ae256142ac3fd24bd2003d28782&limit=5000');
            // const request_playLeft = await axios.get('https://api.wh.geniussports.com/v1/basketball/competitions/22679/matches/' + this.state.matchid + '/teams/' + this.state.lefteam + '/persons?ak=eebd8ae256142ac3fd24bd2003d28782&limit=100');
            // const request_playRight = await axios.get('https://api.wh.geniussports.com/v1/basketball/competitions/22679/matches/' + this.state.matchid + '/teams/' + this.state.rightteam + '/persons?ak=eebd8ae256142ac3fd24bd2003d28782&limit=100');

            const request_leadersLeft = await axios.get('https://api.wh.geniussports.com/v1/basketball/statistics/personmatch/matches/' + this.state.matchid + '/teams/' + this.state.lefteam + '?ak=eebd8ae256142ac3fd24bd2003d28782&periodNumber=0&fields=familyName,sPoints,firstName,images,sReboundsTotal,sAssists&limit=30');
            const request_leadersRight = await axios.get('https://api.wh.geniussports.com/v1/basketball/statistics/personmatch/matches/' + this.state.matchid + '/teams/' + this.state.rightteam + '?ak=eebd8ae256142ac3fd24bd2003d28782&periodNumber=0&fields=familyName,sPoints,firstName,images,sReboundsTotal,sAssists&limit=30');
            if (this.state.isMounted) {
            this.setState({
                dataSource: request_1_url.data.response.data,
                standingInfo: request_standing.data.response.data,
                leftInfo: request_quarterLeft.data.response.data,
                rightInfo: request_quarterRight.data.response.data,
                // boxLeftInfo: request_boxLeft.data.response.data,
                // boxRightInfo: request_boxRight.data.response.data,
                //playInfotest: request_play.data.response.data,
                // playInfoLeft: request_playLeft.data.response.data,
                // playInfoRight: request_playRight.data.response.data,
                matchInfo: request_match.data.response.data,
                isLoading: false,
                leadersLeft: request_leadersLeft.data.response.data,
                leadersRight: request_leadersRight.data.response.data,
            })
        }
        
        } catch (error) {
            console.log(error);
            this.setState({
                isLoading: false,
                showErrorPage: true,
            });
        }
    }

    //events
    renderEvented = (item, index) => {
        return (
            <View style={{ paddingBottom: 20, }}>
                <FlatList
                    //horizontal
                    pagingEnabled
                    scrollEnabled
                    showsHorizontalScrollIndicator={false}
                    scrollEventThrottle={1000}
                    snapToAlignment="center"
                    style={{ overflow: 'visible' }}
                    data={this.state.playInfotest.sort((a, b) => b.actionNumber - a.actionNumber)}
                    keyExtractor={(item, index) => `${item.id}`}
                    renderItem={({ item, index }) => this.renderEvents(item, index)}
                />
            </View>
        )
    }

    renderBox = () => {

        let teamLeft = this.state.dataSource[0].teamId;
        let teamRight = this.state.dataSource[1].teamId;

        if (teamLeft == '88269') {
            teamCodeLeft = 'HKE';
            teamNameLeft = 'EASTERN';
        }
        if (teamLeft == '124987') {
            teamCodeLeft = 'FUB';
            teamNameLeft = 'BRAVES';
        }
        if (teamLeft == '88263') {
            teamCodeLeft = 'FMD';
            teamNameLeft = 'DREAMERS';
        }
        if (teamLeft == '102547') {
            teamCodeLeft = 'MBB';
            teamNameLeft = 'BEARS';
        }
        if (teamLeft == '25496') {
            teamCodeLeft = 'MNV';
            teamNameLeft = 'VAMPIRE';
        }
        if (teamLeft == '100') {
            teamCodeLeft = 'SGH';
            teamNameLeft = 'HEAT';
        }
        if (teamLeft == '88264') {
            teamCodeLeft = 'SAP';
            teamNameLeft = 'ALAB';
        }
        if (teamLeft == '79') {
            teamCodeLeft = 'SGS';
            teamNameLeft = 'SLINGERS';
        }
        if (teamLeft == '104') {
            teamCodeLeft = 'KLD';
            teamNameLeft = 'DRAGONS';
        }
        if (teamLeft == '102548') {
            teamCodeLeft = 'MWW';
            teamNameLeft = 'WARRIORS';
        }
        if (teamRight == '88269') {
            teamCodeRight = 'HKE';
            teamNameRight = 'EASTERN';
        }
        if (teamRight == '124987') {
            teamCodeRight = 'FUB';
            teamNameRight = 'BRAVES';
        }
        if (teamRight == '88263') {
            teamCodeRight = 'FMD';
            teamNameRight = 'DREAMERS';
        }
        if (teamRight == '102547') {
            teamCodeRight = 'MBB';
            teamNameRight = 'BEARS';
        }
        if (teamRight == '25496') {
            teamCodeRight = 'MNV';
            teamNameRight = 'VAMPIRE';
        }
        if (teamRight == '100') {
            teamCodeRight = 'SGH';
            teamNameRight = 'HEAT';
        }
        if (teamRight == '88264') {
            teamCodeRight = 'SAP';
            teamNameRight = 'ALAB';
        }
        if (teamRight == '79') {
            teamCodeRight = 'SGS';
            teamNameRight = 'SLINGERS';
        }
        if (teamRight == '104') {
            teamCodeRight = 'KLD';
            teamNameRight = 'DRAGONS';
        }
        if (teamRight == '102548') {
            teamCodeRight = 'MWW';
            teamNameRight = 'WARRIORS';
        }

        let starterLeft = [];
        let benchLeft = [];
        let starterRight = [];
        let benchRight = [];
        let wowStLeft = 0;
        let wiStLeft = 0;
        let wowStRight = 0;
        let wiStRight = 0;

        let testBoxLeft = this.state.boxLeftInfo.map((val, key) => {

            if (val.isStarter == 1) {
                starterLeft[wowStLeft] = val;
                wowStLeft++;
            }
            if (val.isStarter == 0) {
                benchLeft[wiStLeft] = val;
                wiStLeft++;
            }
        });

        let testBoxRight = this.state.boxRightInfo.map((val, key) => {

            if (val.isStarter == 1) {
                starterRight[wowStRight] = val;
                wowStRight++;
            }
            if (val.isStarter == 0) {
                benchRight[wiStRight] = val;
                wiStRight++;
            }
        });

        //box score starter left team 
        let boxStarterLeft = starterLeft.map((val, key) => {
            return <View key={key} style={{ backgroundColor: key % 2 === 0 ? '#e8e8e8' : 'white', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderRightWidth: 1, borderRightColor: '#797979' }}>
                <View style={styles.columnViewStatsImage}>
                    <Image
                        style={styles.playerPic}
                        source={{
                            uri: val.images.photo.T1.url
                        }}
                    />
                </View>
                <View style={styles.columnViewStatsPlayerName}>
                    <Text style={styles.recordsLabelStatsPlayerName}>{val.firstName}</Text>
                    <Text style={styles.recordsLabelStatsPlayerName}>{val.familyName}</Text>
                </View>
            </View>
        });

        //box score bench left team 
        boxBenchLeft = benchLeft.map((val, key) => {
            return <View key={key} style={{ backgroundColor: key % 2 === 0 ? '#e8e8e8' : 'white', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderRightWidth: 1, borderRightColor: '#797979' }}>
                <View style={styles.columnViewStatsImage}>
                    <Image
                        style={styles.playerPic}
                        source={{
                            uri: val.images.photo.T1.url
                        }}
                    />
                </View>
                <View style={styles.columnViewStatsPlayerName}>
                    <Text style={styles.recordsLabelStatsPlayerName}>{val.firstName}</Text>
                    <Text style={styles.recordsLabelStatsPlayerName}>{val.familyName}</Text>
                </View>
            </View>

        });

        //box score starter left team stats
        let boxStarterStatsLeft = starterLeft.map((val, key) => {

            let testMinutes = val.sMinutes.toString();
            let finalMinutes = testMinutes.split('.')[0];

            //compute field goal
            let testFgLeft = val.sFieldGoalsPercentage.toString();

            let finalFgLeft;

            if (testFgLeft === '1') {
                finalFgLeft = '100';
            } else if (testFgLeft === '0') {
                finalFgLeft = '0';
            } else {
                let resFgLeft = testFgLeft.substring(2, 4);
                if (resFgLeft.length > 1) {
                    finalFgLeft = resFgLeft;
                } else {
                    finalFgLeft = resFgLeft + '0';
                }
            }

            //compute 3pt%
            let testThreePointLeft = val.sThreePointersPercentage.toString();

            let finalThreePointLeft;

            if (testThreePointLeft === '1') {
                finalThreePointLeft = '100';
            } else if (testThreePointLeft === '0') {
                finalThreePointLeft = '0';
            } else {
                let resThreePointLeft = testThreePointLeft.substring(2, 4);
                if (resThreePointLeft.length > 1) {
                    finalThreePointLeft = resThreePointLeft;
                } else {
                    finalThreePointLeft = resThreePointLeft + '0';
                }
            }

            //compute ft%
            let testFtLeft = val.sFreeThrowsPercentage.toString();

            let finalFtLeft;

            if (testFtLeft === '1') {
                finalFtLeft = '100';
            } else if (testFtLeft === '0') {
                finalFtLeft = '0';
            } else {
                let resFtLeft = testFtLeft.substring(2, 4);
                if (resFtLeft.length > 1) {
                    finalFtLeft = resFtLeft;
                } else {
                    finalFtLeft = resFtLeft + '0';
                }
            }


            return <View style={{ flexDirection: 'row', alignItems: 'center', height: 60, backgroundColor: key % 2 === 0 ? '#e8e8e8' : 'white' }}>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}> </Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}> </Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}> </Text>
                </View>
                {/* <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{val.TVName}</Text>
                </View> */}
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{finalMinutes}</Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{val.sPoints}</Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{val.sAssists}</Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{val.sReboundsTotal}</Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{finalFgLeft}</Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{val.sBlocks}</Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{val.sSteals}</Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{finalThreePointLeft}</Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{finalFtLeft}</Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{val.sTurnovers}</Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{val.sFoulsPersonal}</Text>
                </View>
            </View>

        });

        //box score bench left team stats
        let boxBenchStatsLeft = benchLeft.map((val, key) => {

            let testMinutes = val.sMinutes.toString();
            let finalMinutes = testMinutes.split('.')[0];

            //compute field goal
            let testFgLeft = val.sFieldGoalsPercentage.toString();

            let finalFgLeft;

            if (testFgLeft === '1') {
                finalFgLeft = '100';
            } else if (testFgLeft === '0') {
                finalFgLeft = '0';
            } else {
                let resFgLeft = testFgLeft.substring(2, 4);
                if (resFgLeft.length > 1) {
                    finalFgLeft = resFgLeft;
                } else {
                    finalFgLeft = resFgLeft + '0';
                }
            }

            //compute 3pt%
            let testThreePointLeft = val.sThreePointersPercentage.toString();

            let finalThreePointLeft;

            if (testThreePointLeft === '1') {
                finalThreePointLeft = '100';
            } else if (testThreePointLeft === '0') {
                finalThreePointLeft = '0';
            } else {
                let resThreePointLeft = testThreePointLeft.substring(2, 4);
                if (resThreePointLeft.length > 1) {
                    finalThreePointLeft = resThreePointLeft;
                } else {
                    finalThreePointLeft = resThreePointLeft + '0';
                }
            }

            //compute ft%
            let testFtLeft = val.sFreeThrowsPercentage.toString();

            let finalFtLeft;

            if (testFtLeft === '1') {
                finalFtLeft = '100';
            } else if (testFtLeft === '0') {
                finalFtLeft = '0';
            } else {
                let resFtLeft = testFtLeft.substring(2, 4);
                if (resFtLeft.length > 1) {
                    finalFtLeft = resFtLeft;
                } else {
                    finalFtLeft = resFtLeft + '0';
                }
            }


            return <View style={{ flexDirection: 'row', alignItems: 'center', height: 60, backgroundColor: key % 2 === 0 ? '#e8e8e8' : 'white' }}>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}> </Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}> </Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}> </Text>
                </View>
                {/* <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{val.TVName}</Text>
                </View> */}
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{finalMinutes}</Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{val.sPoints}</Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{val.sAssists}</Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{val.sReboundsTotal}</Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{finalFgLeft}</Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{val.sBlocks}</Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{val.sSteals}</Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{finalThreePointLeft}</Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{finalFtLeft}</Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{val.sTurnovers}</Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{val.sFoulsPersonal}</Text>
                </View>
            </View>

        });

        //start bench
        //box score starter left team 
        let boxStarterRight = starterRight.map((val, key) => {
            return <View key={key} style={{ backgroundColor: key % 2 === 0 ? '#e8e8e8' : 'white', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderRightWidth: 1, borderRightColor: '#797979' }}>
                <View style={styles.columnViewStatsImage}>
                    <Image
                        style={styles.playerPic}
                        source={{
                            uri: val.images.photo.T1.url
                        }}
                    />
                </View>
                <View style={styles.columnViewStatsPlayerName}>
                    <Text style={styles.recordsLabelStatsPlayerName}>{val.firstName}</Text>
                    <Text style={styles.recordsLabelStatsPlayerName}>{val.familyName}</Text>
                </View>
            </View>

        });

        //box score bench left team 
        let boxBenchRight = benchRight.map((val, key) => {
            return <View key={key} style={{ backgroundColor: key % 2 === 0 ? '#e8e8e8' : 'white', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderRightWidth: 1, borderRightColor: '#797979' }}>
                <View style={styles.columnViewStatsImage}>
                    <Image
                        style={styles.playerPic}
                        source={{
                            uri: val.images.photo.T1.url
                        }}
                    />
                </View>
                <View style={styles.columnViewStatsPlayerName}>
                    <Text style={styles.recordsLabelStatsPlayerName}>{val.firstName}</Text>
                    <Text style={styles.recordsLabelStatsPlayerName}>{val.familyName}</Text>
                </View>
            </View>

        });

        //box score starter left team stats
        let boxStarterStatsRight = starterRight.map((val, key) => {

            let testMinutes = val.sMinutes.toString();
            let finalMinutes = testMinutes.split('.')[0];

            //compute field goal
            let testFgLeft = val.sFieldGoalsPercentage.toString();

            let finalFgLeft;

            if (testFgLeft === '1') {
                finalFgLeft = '100';
            } else if (testFgLeft === '0') {
                finalFgLeft = '0';
            } else {
                let resFgLeft = testFgLeft.substring(2, 4);
                if (resFgLeft.length > 1) {
                    finalFgLeft = resFgLeft;
                } else {
                    finalFgLeft = resFgLeft + '0';
                }
            }

            //compute 3pt%
            let testThreePointLeft = val.sThreePointersPercentage.toString();

            let finalThreePointLeft;

            if (testThreePointLeft === '1') {
                finalThreePointLeft = '100';
            } else if (testThreePointLeft === '0') {
                finalThreePointLeft = '0';
            } else {
                let resThreePointLeft = testThreePointLeft.substring(2, 4);
                if (resThreePointLeft.length > 1) {
                    finalThreePointLeft = resThreePointLeft;
                } else {
                    finalThreePointLeft = resThreePointLeft + '0';
                }
            }

            //compute ft%
            let testFtLeft = val.sFreeThrowsPercentage.toString();

            let finalFtLeft;

            if (testFtLeft === '1') {
                finalFtLeft = '100';
            } else if (testFtLeft === '0') {
                finalFtLeft = '0';
            } else {
                let resFtLeft = testFtLeft.substring(2, 4);
                if (resFtLeft.length > 1) {
                    finalFtLeft = resFtLeft;
                } else {
                    finalFtLeft = resFtLeft + '0';
                }
            }


            return <View style={{ flexDirection: 'row', alignItems: 'center', height: 60, backgroundColor: key % 2 === 0 ? '#e8e8e8' : 'white' }}>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}> </Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}> </Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}> </Text>
                </View>
                {/* <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{val.TVName}</Text>
                </View> */}
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{finalMinutes}</Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{val.sPoints}</Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{val.sAssists}</Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{val.sReboundsTotal}</Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{finalFgLeft}</Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{val.sBlocks}</Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{val.sSteals}</Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{finalThreePointLeft}</Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{finalFtLeft}</Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{val.sTurnovers}</Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{val.sFoulsPersonal}</Text>
                </View>
            </View>

        });

        //box score bench left team stats
        let boxBenchStatsRight = benchRight.map((val, key) => {

            let testMinutes = val.sMinutes.toString();
            let finalMinutes = testMinutes.split('.')[0];

            //compute field goal
            let testFgLeft = val.sFieldGoalsPercentage.toString();

            let finalFgLeft;

            if (testFgLeft === '1') {
                finalFgLeft = '100';
            } else if (testFgLeft === '0') {
                finalFgLeft = '0';
            } else {
                let resFgLeft = testFgLeft.substring(2, 4);
                if (resFgLeft.length > 1) {
                    finalFgLeft = resFgLeft;
                } else {
                    finalFgLeft = resFgLeft + '0';
                }
            }

            //compute 3pt%
            let testThreePointLeft = val.sThreePointersPercentage.toString();

            let finalThreePointLeft;

            if (testThreePointLeft === '1') {
                finalThreePointLeft = '100';
            } else if (testThreePointLeft === '0') {
                finalThreePointLeft = '0';
            } else {
                let resThreePointLeft = testThreePointLeft.substring(2, 4);
                if (resThreePointLeft.length > 1) {
                    finalThreePointLeft = resThreePointLeft;
                } else {
                    finalThreePointLeft = resThreePointLeft + '0';
                }
            }

            //compute ft%
            let testFtLeft = val.sFreeThrowsPercentage.toString();

            let finalFtLeft;

            if (testFtLeft === '1') {
                finalFtLeft = '100';
            } else if (testFtLeft === '0') {
                finalFtLeft = '0';
            } else {
                let resFtLeft = testFtLeft.substring(2, 4);
                if (resFtLeft.length > 1) {
                    finalFtLeft = resFtLeft;
                } else {
                    finalFtLeft = resFtLeft + '0';
                }
            }

            return <View style={{ flexDirection: 'row', alignItems: 'center', height: 60, backgroundColor: key % 2 === 0 ? '#e8e8e8' : 'white' }}>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}> </Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}> </Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}> </Text>
                </View>
                {/* <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{val.TVName}</Text>
                </View> */}
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{finalMinutes}</Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{val.sPoints}</Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{val.sAssists}</Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{val.sReboundsTotal}</Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{finalFgLeft}</Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{val.sBlocks}</Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{val.sSteals}</Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{finalThreePointLeft}</Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{finalFtLeft}</Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{val.sTurnovers}</Text>
                </View>
                <View style={styles.columnViewStats}>
                    <Text style={styles.recordsLabelStats}>{val.sFoulsPersonal}</Text>
                </View>
            </View>

        });

        return (
            <View>
                <View style={styles.rowLabel}>
                    <Text style={styles.label}>{teamNameLeft}</Text>
                </View>
                <View style={styles.rowView}>
                    <View style={{ flexDirection: 'column', position: 'absolute', backgroundColor: 'white', zIndex: 10 }}>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={styles.columnViewStatsFix}>
                                <Text style={styles.recordsLabelStatsFix}>PLAYER</Text>
                            </View>
                            <View style={styles.columnViewStatsFix}>
                                <Text style={styles.recordsLabelStatsFix}> </Text>
                            </View>
                        </View>
                        {boxStarterLeft}
                        <View style={{ borderTopColor: '#797979', borderTopWidth: 1 }}>
                            {boxBenchLeft}
                        </View>
                    </View>

                    <ScrollView horizontal={true}>
                        <View style={{ flexDirection: 'column' }}>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={styles.columnViewStats}>
                                    <Text style={styles.recordsLabelStats}> </Text>
                                </View>
                                <View style={styles.columnViewStats}>
                                    <Text style={styles.recordsLabelStats}> </Text>
                                </View>
                                <View style={styles.columnViewStats}>
                                    <Text style={styles.recordsLabelStats}> </Text>
                                </View>
                                {/* <View style={styles.columnViewStats}>
                                    <Text style={styles.recordsLabelStats}>P</Text>
                                </View> */}
                                <View style={styles.columnViewStats}>
                                    <Text style={styles.recordsLabelStats}>MIN</Text>
                                </View>
                                <View style={styles.columnViewStats}>
                                    <Text style={styles.recordsLabelStats}>PTS</Text>
                                </View>
                                <View style={styles.columnViewStats}>
                                    <Text style={styles.recordsLabelStats}>AST</Text>
                                </View>
                                <View style={styles.columnViewStats}>
                                    <Text style={styles.recordsLabelStats}>REB</Text>
                                </View>
                                <View style={styles.columnViewStats}>
                                    <Text style={styles.recordsLabelStats}>FG%</Text>
                                </View>
                                <View style={styles.columnViewStats}>
                                    <Text style={styles.recordsLabelStats}>BLK</Text>
                                </View>
                                <View style={styles.columnViewStats}>
                                    <Text style={styles.recordsLabelStats}>STL</Text>
                                </View>
                                <View style={styles.columnViewStats}>
                                    <Text style={styles.recordsLabelStats}>3PT%</Text>
                                </View>
                                <View style={styles.columnViewStats}>
                                    <Text style={styles.recordsLabelStats}>FT%</Text>
                                </View>
                                <View style={styles.columnViewStats}>
                                    <Text style={styles.recordsLabelStats}>TO</Text>
                                </View>
                                <View style={styles.columnViewStats}>
                                    <Text style={styles.recordsLabelStats}>PF</Text>
                                </View>
                            </View>
                            {boxStarterStatsLeft}
                            <View style={{ borderTopColor: '#797979', borderTopWidth: 1 }}>
                                {boxBenchStatsLeft}
                            </View>
                        </View>
                    </ScrollView>
                </View>
                <View style={styles.rowLabel}>
                    <Text style={styles.label}>{teamNameRight}</Text>
                </View>
                <View style={styles.rowView}>
                    <View style={{ flexDirection: 'column', position: 'absolute', backgroundColor: 'white', zIndex: 10 }}>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={styles.columnViewStatsFix}>
                                <Text style={styles.recordsLabelStatsFix}>PLAYER</Text>
                            </View>
                            <View style={styles.columnViewStatsFix}>
                                <Text style={styles.recordsLabelStatsFix}> </Text>
                            </View>
                        </View>
                        {boxStarterRight}
                        <View style={{ borderTopColor: '#797979', borderTopWidth: 1 }}>
                            {boxBenchRight}
                        </View>
                    </View>

                    <ScrollView horizontal={true}>
                        <View style={{ flexDirection: 'column' }}>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={styles.columnViewStats}>
                                    <Text style={styles.recordsLabelStats}> </Text>
                                </View>
                                <View style={styles.columnViewStats}>
                                    <Text style={styles.recordsLabelStats}> </Text>
                                </View>
                                <View style={styles.columnViewStats}>
                                    <Text style={styles.recordsLabelStats}> </Text>
                                </View>
                                {/* <View style={styles.columnViewStats}>
                                    <Text style={styles.recordsLabelStats}>P</Text>
                                </View> */}
                                <View style={styles.columnViewStats}>
                                    <Text style={styles.recordsLabelStats}>MIN</Text>
                                </View>
                                <View style={styles.columnViewStats}>
                                    <Text style={styles.recordsLabelStats}>PTS</Text>
                                </View>
                                <View style={styles.columnViewStats}>
                                    <Text style={styles.recordsLabelStats}>AST</Text>
                                </View>
                                <View style={styles.columnViewStats}>
                                    <Text style={styles.recordsLabelStats}>REB</Text>
                                </View>
                                <View style={styles.columnViewStats}>
                                    <Text style={styles.recordsLabelStats}>FG%</Text>
                                </View>
                                <View style={styles.columnViewStats}>
                                    <Text style={styles.recordsLabelStats}>BLK</Text>
                                </View>
                                <View style={styles.columnViewStats}>
                                    <Text style={styles.recordsLabelStats}>STL</Text>
                                </View>
                                <View style={styles.columnViewStats}>
                                    <Text style={styles.recordsLabelStats}>3PT%</Text>
                                </View>
                                <View style={styles.columnViewStats}>
                                    <Text style={styles.recordsLabelStats}>FT%</Text>
                                </View>
                                <View style={styles.columnViewStats}>
                                    <Text style={styles.recordsLabelStats}>TO</Text>
                                </View>
                                <View style={styles.columnViewStats}>
                                    <Text style={styles.recordsLabelStats}>PF</Text>
                                </View>
                            </View>
                            {boxStarterStatsRight}
                            <View style={{ borderTopColor: '#797979', borderTopWidth: 1 }}>
                                {boxBenchStatsRight}
                            </View>
                        </View>
                    </ScrollView>
                </View>
            </View>
        )
    }

    renderEvents = (item, index) => {

        let result;
        let actionResult;
        let periodResult;


        if (item.actionType == 'game') {
            if (item.subType == 'end') {
                result = 'GAME END';
            } else {
                result = 'GAME START';
            }

            return <View style={styles.rowInfoEnd}>
                <Text style={styles.labelQuartsz}>{result}</Text>
            </View>
        }

        else if (item.actionType == 'period') {
            if (item.subType == 'end') {
                result = 'PERIOD END';
            } else {
                result = 'PERIOD START';
            }

            return <View style={styles.rowInfoEnd}>
                <Text style={styles.labelQuartsz}>{result}</Text>
            </View>

        } else {

            if (item.periodType == 'REGULAR') {
                periodResult = 'Q';
            } else {
                periodResult = 'OT';
            }

            if (item.success == 1) {
                actionResult = 'made';
            }

            if (item.success == 0) {
                actionResult = 'missed';
            }

            if (item.periodType == 'REGULAR') {
                periodResult = 'Q';
            } else {
                periodResult = 'OT';
            }

            let trimTime = item.clock.substring(0, 5);

            let teamCodePlay;

            if (item.teamId == 88269) {
                teamCodePlay = 'HKE';
            }
            if (item.teamId == 124987) {
                teamCodePlay = 'FUB';
            }
            if (item.teamId == 88263) {
                teamCodePlay = 'FMD';
            }
            if (item.teamId == 102547) {
                teamCodePlay = 'MBB';
            }
            if (item.teamId == 25496) {
                teamCodePlay = 'MNV';
            }
            if (item.teamId == 100) {
                teamCodePlay = 'SGH';
            }
            if (item.teamId == 88264) {
                teamCodePlay = 'SAP';
            }
            if (item.teamId == 79) {
                teamCodePlay = 'SGS';
            }
            if (item.teamId == 104) {
                teamCodePlay = 'KLD';
            }
            if (item.teamId == 102548) {
                teamCodePlay = 'MWW';
            }

            let isPlayer;

            let playersLeft = this.state.playInfoLeft.map((ey, key) => {
                if (item.personId == ey.personId) {
                    isPlayer = ey.images.photo.T1.url;
                }
            });

            let playersRight = this.state.playInfoRight.map((ey, key) => {
                if (item.personId == ey.personId) {
                    isPlayer = ey.images.photo.T1.url;
                }
            });

            return (
                <View style={styles.rowInfo}>
                    <View style={{ width: 25 + '%', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', borderRightWidth: 1, borderRightColor: '#e3e3e3' }}>
                        <Text style={styles.infoLabelScoresLabelPbp}>{periodResult} {item.period} {trimTime}</Text>
                        <Text style={styles.infoLabelScoresLabelPbpNo}>{teamCodePlay}</Text>
                        <Text style={styles.infoLabelScoresLabelPbpNo}>{item.score1}-{item.score2}</Text>
                    </View>
                    <View style={{ width: 75 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', paddingLeft: 5, flexWrap: "wrap" }}>
                        <Image
                            style={styles.userPic}
                            source={{
                                uri: isPlayer
                            }}
                        />
                        <Text style={styles.infoLabelScoresLabelPbpNoText}>{item.familyName} {item.subType} {item.actionType} {actionResult}</Text>
                    </View>
                </View>
            )
        }
    }

    //events
    renderHeadtoHead = (item, index) => {
        return (
            <View style={styles.groupheadtohead}>
                <View style={styles.rowLabelheadtohead}>
                    <Text style={styles.label}>HEAD TO HEAD</Text>
                </View>
                <FlatList
                    //horizontal
                    pagingEnabled
                    scrollEnabled
                    showsHorizontalScrollIndicator={false}
                    scrollEventThrottle={1000}
                    snapToAlignment="center"
                    style={{ overflow: 'visible' }}
                    data={this.state.headtoheadInfo.sort((a, b) => b.matchNumber - a.matchNumber)}
                    keyExtractor={(item, index) => `${item.id}`}
                    renderItem={({ item, index }) => this.renderEventsHeatoHead(item, index)}
                />
            </View>
        )
    }

    renderEventsHeatoHead = (item, index) => {

        if (item.matchStatus == 'COMPLETE' || item.matchStatus == 'FINISHED') {

            return <TouchableOpacity onPress={this._open.bind(this, item)}>
                <View style={styles.card}>
                    <View style={{ flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: '#e3e3e3' }}>
                        <Text style={styles.date}>{moment(item.matchTime).format('ddd - MMM D').toUpperCase()}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', paddingTop: 10 }}>
                        <View style={{ width: 70 + '%', paddingLeft: 15, }}>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ width: 70 + '%', height: 50 }}>
                                    <View style={styles.author}>
                                        <Image
                                            style={styles.userPic}
                                            source={{
                                                uri: item.competitors[0].images.logo.T1.url
                                            }}
                                        />
                                        <Text style={styles.team}>{item.competitors[0].teamNickname}</Text>
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', width: 25 + '%', height: 40 }}>
                                    <Text styles={styles.standing}></Text>
                                    <Text style={styles.scoreHeadtohead}>{item.competitors[0].scoreString}</Text>
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ width: 70 + '%', height: 50 }}>
                                    <View style={styles.author}>
                                        <Image
                                            style={styles.userPic}
                                            source={{
                                                uri: item.competitors[1].images.logo.T1.url
                                            }}
                                        />
                                        <Text style={styles.team}>{item.competitors[1].teamNickname}</Text>
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', width: 25 + '%', height: 40 }}>
                                    <Text styles={styles.standing}></Text>
                                    <Text style={styles.scoreHeadtohead}>{item.competitors[1].scoreString}</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ width: 30 + '%' }}>
                            <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                                <View style={{ width: 50, height: 15 }} />
                                <View style={{ justifyContent: 'center', alignItems: 'center', width: 50, height: 15 }}>
                                    <Text style={styles.final}>FINAL</Text>
                                </View>
                                <View style={{ justifyContent: 'center', alignItems: 'center', width: 50, height: 20 }}>
                                    <Icon name="chevron-right" size={25} />
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        }
    }


    getplaybyplay = async () => {

        let comid = 26702;

        const request_play = await axios.get('https://api.wh.geniussports.com/v1/basketball/matches/' + this.state.matchid + '/actions?ak=eebd8ae256142ac3fd24bd2003d28782&scoring=1&fields=actionNumber,actionType,subType,periodType,clock,teamId,personId,score1,score2,familyName,success,period&limit=5000');
        const request_playLeft = await axios.get('https://api.wh.geniussports.com/v1/basketball/competitions/' + comid + '/matches/' + this.state.matchid + '/teams/' + this.state.lefteam + '/persons?ak=eebd8ae256142ac3fd24bd2003d28782&fields=images,personId&limit=100');
        const request_playRight = await axios.get('https://api.wh.geniussports.com/v1/basketball/competitions/' + comid + '/matches/' + this.state.matchid + '/teams/' + this.state.rightteam + '/persons?ak=eebd8ae256142ac3fd24bd2003d28782&fields=images,personId&limit=100');
        if (this.state.isMounted) {
        this.setState({
            playInfotest: request_play.data.response.data,
            playInfoLeft: request_playLeft.data.response.data,
            playInfoRight: request_playRight.data.response.data,
        })
    }
    }

    getboxscore = async () => {
        const request_boxLeft = await axios.get('https://api.wh.geniussports.com/v1/basketball/statistics/personmatch/matches/' + this.state.matchid + '/teams/' + this.state.lefteam + '?ak=eebd8ae256142ac3fd24bd2003d28782&fields=images,isStarter,firstName,familyName,sMinutes,sFieldGoalsPercentage,sThreePointersPercentage,sFreeThrowsPercentage,TVName,sPoints,sAssists,sReboundsTotal,sBlocks,sSteals,sTurnovers,sFoulsPersonal&limit=100');
        const request_boxRight = await axios.get('https://api.wh.geniussports.com/v1/basketball/statistics/personmatch/matches/' + this.state.matchid + '/teams/' + this.state.rightteam + '?ak=eebd8ae256142ac3fd24bd2003d28782&fields=images,isStarter,firstName,familyName,sMinutes,sFieldGoalsPercentage,sThreePointersPercentage,sFreeThrowsPercentage,TVName,sPoints,sAssists,sReboundsTotal,sBlocks,sSteals,sTurnovers,sFoulsPersonal&limit=100');

        setTimeout(() => {
            this.setState({
                showMe: false,
            })
        }, 3000)
        if (this.state.isMounted) {
        this.setState({
            boxLeftInfo: request_boxLeft.data.response.data,
            boxRightInfo: request_boxRight.data.response.data,
        })
    }
    }

    segmentClicked = (index) => {
        this.setState({
            activeIndex: index
        })
    }

    renderSection = () => {
        if (this.state.isLoading) {
            return (
                <View style={{
                    flex: 1,
                    flexDirection: 'column',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    backgroundColor: '#000000',
                }}>
                    <View style={{ width: 100 + '%', }}>
                        <ImageBackground source={require('./src/bg-header.jpg')} style={{ width: '100%', height: 'auto', resizeMode: 'cover' }}>
                            <Text style={styles.headerText}>GAMES</Text>
                        </ImageBackground>
                    </View>
                    <View>
                        <ActivityIndicator size="large" color="#fff" />
                    </View>
                    <View style={{ backgroundColor: 'black' }} />
                </View>
            )
        } else {

            let starterLeft = [];

            let benchLeft = [];

            let starterRight = [];

            let benchRight = [];

            let teamNameLeft;
            let teamNameRight;

            let teamCodeLeft;
            let teamCodeRight;
            let teamLeft;
            let teamRight;
            let resFtLeft;
            let resFtRight;
            let resThreeLeft;
            let resThreeRight;
            let resFieldLeft;
            let resFieldRight;

            let leftAssists;
            let RightAssists;

            let leftReboundsTotal;
            let RightReboundsTotal;

            let leftReboundsOffensive;
            let RightReboundsOffensive;

            let leftsReboundsDefensive;
            let RightsReboundsDefensive;

            let LeftSteals;
            let RightSteals;

            let LeftBlocks;
            let RightBlocks;

            let LeftTurnovers;
            let RightTurnovers;

            let LeftFoulsTotal;
            let RightFoulsTotal;

            let test = [];
            let testOt = [];

            let testRight = [];
            let testOtRight = [];

            let quarterOneLeft;
            let quarterTwoLeft;
            let quarterThreeLeft;
            let quarterFourLeft;
            let totalLeft;

            //ot 11 score left
            let otOneLeft;

            let otOneScoreLeft;

            // ot 2 score left
            let otTwoLeft;

            let otTwoScoreLeft;

            let otThreeLeft;

            let otThreeScoreLeft;

            //right score team
            let quarterOneRight;
            let quarterTwoRight;
            let quarterThreeRight;
            let quarterFourRight;
            let totalRight;

            // ot 1 score right
            let otOneRight;

            let otOneScoreRight;

            // ot 2 score right
            let otTwoRight;

            let otTwoScoreRight;

            // box score
            let boxStarterLeft;
            let boxBenchLeft;

            let boxStarterStatsLeft;
            let boxBenchStatsLeft;

            let boxStarterRight;
            let boxBenchRight;

            let boxStarterStatsRight;
            let boxBenchStatsRight;

            let newPlay = [];

            if (this.state.matchInfo.length > 0) {

                let playIncrement = 0;
                let playTest = this.state.playInfo.map((val, key) => {

                    if (val.actionType != 'clock') {
                        newPlay[playIncrement] = val;
                        playIncrement++;
                    }
                });

                ftLeft = this.state.matchInfo[0].sFreeThrowsPercentage.toString();
                resFtLeft = ftLeft.substr(2, 2) + '%';

                ftRight = this.state.matchInfo[1].sFreeThrowsPercentage.toString();
                resFtRight = ftRight.substr(2, 2) + '%';

                threeLeft = this.state.matchInfo[0].sThreePointersPercentage.toString();
                resThreeLeft = threeLeft.substr(2, 2);
                if (resThreeLeft.length < 2) {
                    resThreeLeftFinal = resThreeLeft + '0%';
                } else {
                    resThreeLeftFinal = resThreeLeft + '%';
                }

                threeRight = this.state.matchInfo[1].sThreePointersPercentage.toString();
                resThreeRight = threeRight.substr(2, 2);
                if (resThreeRight.length < 2) {
                    resThreeRightFinal = resThreeRight + '0%';
                } else {
                    resThreeRightFinal = resThreeRight + '%';
                }

                fieldLeft = this.state.matchInfo[0].sFieldGoalsPercentage.toString();
                resFieldLeft = fieldLeft.substr(2, 2);

                if(resFieldLeft.length > 1) {
                    resFieldLeft = resFieldLeft + '%';
                } else {
                    resFieldLeft = resFieldLeft + '0%';
                }

                console.log(fieldLeft, 'fieldLeft');

                fieldRight = this.state.matchInfo[1].sFieldGoalsPercentage.toString();
                resFieldRight = fieldRight.substr(2, 2);

                if(resFieldRight.length > 1) {
                    resFieldRight = resFieldRight + '%';
                } else {
                    resFieldRight = resFieldRight + '0%';
                }

                leftAssists = this.state.matchInfo[0].sAssists;
                RightAssists = this.state.matchInfo[1].sAssists;

                leftReboundsTotal = this.state.matchInfo[0].sReboundsTotal;
                RightReboundsTotal = this.state.matchInfo[1].sReboundsTotal;                   

                leftReboundsOffensive = this.state.matchInfo[0].sReboundsOffensive;
                RightReboundsOffensive = this.state.matchInfo[1].sReboundsOffensive;

                leftsReboundsDefensive = this.state.matchInfo[0].sReboundsDefensive;
                RightsReboundsDefensive = this.state.matchInfo[1].sReboundsDefensive;

                LeftSteals = this.state.matchInfo[0].sSteals;
                RightSteals = this.state.matchInfo[1].sSteals;

                LeftBlocks = this.state.matchInfo[0].sBlocks;
                RightBlocks = this.state.matchInfo[1].sBlocks;

                LeftTurnovers = this.state.matchInfo[0].sTurnovers;
                RightTurnovers = this.state.matchInfo[1].sTurnovers;

                LeftFoulsTotal = this.state.matchInfo[0].sFoulsTotal;
                RightFoulsTotal = this.state.matchInfo[1].sFoulsTotal;

                let wow = 0;
                let wi = 0;

                let wowRight = 0;
                let wiRight = 0;

                let wowStLeft = 0;
                let wiStLeft = 0;

                let wowStRight = 0;
                let wiStRight = 0;

                let scoreLeft = this.state.leftInfo.map((val, key) => {
                    let str = val.linkDetail;
                    let res = str.split("/");
                    if (res[10] == "REGULAR") {
                        test[wow] = val;
                        wow++;
                    }
                    if (res[10] == "OVERTIME") {
                        testOt[wi] = val;
                        wi++;
                    }
                });

                let scoreRight = this.state.rightInfo.map((val, key) => {
                    let str = val.linkDetail;
                    let res = str.split("/");
                    if (res[10] == "REGULAR") {
                        testRight[wowRight] = val;
                        wowRight++;
                    }
                    if (res[10] == "OVERTIME") {
                        testOtRight[wiRight] = val;
                        wiRight++;
                    }
                });

                quarterOneLeft = test[1].sPoints;
                quarterTwoLeft = test[2].sPoints;
                quarterThreeLeft = test[3].sPoints;
                quarterFourLeft = test[4].sPoints;

                totalLeft = test[0].sPoints;


                quarterOneRight = testRight[1].sPoints;
                quarterTwoRight = testRight[2].sPoints;
                quarterThreeRight = testRight[3].sPoints;
                quarterFourRight = testRight[4].sPoints;

                totalRight = testRight[0].sPoints;


                if (testOt.length > 0) {

                    otOneLeft = <View style={styles.columnViewStats}>
                        <Text style={styles.recordsLabelStatsTeamScoreOne}>{testOt[0].sPoints}</Text>
                    </View>
                    otOneLeftLabel = <View style={styles.columnViewStats}>
                        <Text style={styles.recordsLabelStatsTeam}>OT</Text>
                    </View>

                    otOneRight = <View style={styles.columnViewStats}>
                        <Text style={styles.recordsLabelStatsTeamScoreOne}>{testOtRight[0].sPoints}</Text>
                    </View>

                } else {
                    otOneLeft = <View></View>;
                    otOneLeftLabel = <View></View>;
                    otOneRight = <View></View>;
                }

                let testBoxLeft = this.state.boxLeftInfo.map((val, key) => {

                    if (val.isStarter == 1) {
                        starterLeft[wowStLeft] = val;
                        wowStLeft++;
                    }
                    if (val.isStarter == 0) {
                        benchLeft[wiStLeft] = val;
                        wiStLeft++;
                    }
                });

                let testBoxRight = this.state.boxRightInfo.map((val, key) => {

                    if (val.isStarter == 1) {
                        starterRight[wowStRight] = val;
                        wowStRight++;
                    }
                    if (val.isStarter == 0) {
                        benchRight[wiStRight] = val;
                        wiStRight++;
                    }
                });

                //box score starter left team 
                boxStarterLeft = starterLeft.map((val, key) => {
                    return <View key={key} style={{ backgroundColor: key % 2 === 0 ? '#e8e8e8' : 'white', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderRightWidth: 1, borderRightColor: '#797979' }}>
                        <View style={styles.columnViewStatsImage}>
                            <Image
                                style={styles.playerPic}
                                source={{
                                    uri: val.images.photo.T1.url
                                }}
                            />
                        </View>
                        <View style={styles.columnViewStatsPlayerName}>
                            <Text style={styles.recordsLabelStatsPlayerName}>{val.firstName}</Text>
                            <Text style={styles.recordsLabelStatsPlayerName}>{val.familyName}</Text>
                        </View>
                    </View>
                });

                //box score starter left team stats
                boxStarterStatsLeft = starterLeft.map((val, key) => {

                    let testMinutes = val.sMinutes.toString();
                    let finalMinutes = testMinutes.split('.')[0];

                    //compute field goal
                    let testFgLeft = val.sFieldGoalsPercentage.toString();

                    let finalFgLeft;

                    if (testFgLeft === '1') {
                        finalFgLeft = '100';
                    } else if (testFgLeft === '0') {
                        finalFgLeft = '0';
                    } else {
                        let resFgLeft = testFgLeft.substring(2, 4);
                        if (resFgLeft.length > 1) {
                            finalFgLeft = resFgLeft;
                        } else {
                            finalFgLeft = resFgLeft + '0';
                        }
                    }

                    //compute 3pt%
                    let testThreePointLeft = val.sThreePointersPercentage.toString();

                    let finalThreePointLeft;

                    if (testThreePointLeft === '1') {
                        finalThreePointLeft = '100';
                    } else if (testThreePointLeft === '0') {
                        finalThreePointLeft = '0';
                    } else {
                        let resThreePointLeft = testThreePointLeft.substring(2, 4);
                        if (resThreePointLeft.length > 1) {
                            finalThreePointLeft = resThreePointLeft;
                        } else {
                            finalThreePointLeft = resThreePointLeft + '0';
                        }
                    }

                    //compute ft%
                    let testFtLeft = val.sFreeThrowsPercentage.toString();

                    let finalFtLeft;

                    if (testFtLeft === '1') {
                        finalFtLeft = '100';
                    } else if (testFtLeft === '0') {
                        finalFtLeft = '0';
                    } else {
                        let resFtLeft = testFtLeft.substring(2, 4);
                        if (resFtLeft.length > 1) {
                            finalFtLeft = resFtLeft;
                        } else {
                            finalFtLeft = resFtLeft + '0';
                        }
                    }


                    return <View style={{ flexDirection: 'row', alignItems: 'center', height: 60, backgroundColor: key % 2 === 0 ? '#e8e8e8' : 'white' }}>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        {/* <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.TVName}</Text>
                        </View> */}
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{finalMinutes}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sPoints}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sAssists}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sReboundsTotal}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{finalFgLeft}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sBlocks}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sSteals}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{finalThreePointLeft}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{finalFtLeft}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sTurnovers}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sFoulsPersonal}</Text>
                        </View>
                    </View>

                });

                //box score bench left team 
                boxBenchLeft = benchLeft.map((val, key) => {
                    return <View key={key} style={{ backgroundColor: key % 2 === 0 ? '#e8e8e8' : 'white', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderRightWidth: 1, borderRightColor: '#797979' }}>
                        <View style={styles.columnViewStatsImage}>
                            <Image
                                style={styles.playerPic}
                                source={{
                                    uri: val.images.photo.T1.url
                                }}
                            />
                        </View>
                        <View style={styles.columnViewStatsPlayerName}>
                            <Text style={styles.recordsLabelStatsPlayerName}>{val.firstName}</Text>
                            <Text style={styles.recordsLabelStatsPlayerName}>{val.familyName}</Text>
                        </View>
                    </View>

                });

                //box score bench left team stats
                boxBenchStatsLeft = benchLeft.map((val, key) => {

                    let testMinutes = val.sMinutes.toString();
                    let finalMinutes = testMinutes.split('.')[0];

                    //compute field goal
                    let testFgLeft = val.sFieldGoalsPercentage.toString();

                    let finalFgLeft;

                    if (testFgLeft === '1') {
                        finalFgLeft = '100';
                    } else if (testFgLeft === '0') {
                        finalFgLeft = '0';
                    } else {
                        let resFgLeft = testFgLeft.substring(2, 4);
                        if (resFgLeft.length > 1) {
                            finalFgLeft = resFgLeft;
                        } else {
                            finalFgLeft = resFgLeft + '0';
                        }
                    }

                    //compute 3pt%
                    let testThreePointLeft = val.sThreePointersPercentage.toString();

                    let finalThreePointLeft;

                    if (testThreePointLeft === '1') {
                        finalThreePointLeft = '100';
                    } else if (testThreePointLeft === '0') {
                        finalThreePointLeft = '0';
                    } else {
                        let resThreePointLeft = testThreePointLeft.substring(2, 4);
                        if (resThreePointLeft.length > 1) {
                            finalThreePointLeft = resThreePointLeft;
                        } else {
                            finalThreePointLeft = resThreePointLeft + '0';
                        }
                    }

                    //compute ft%
                    let testFtLeft = val.sFreeThrowsPercentage.toString();

                    let finalFtLeft;

                    if (testFtLeft === '1') {
                        finalFtLeft = '100';
                    } else if (testFtLeft === '0') {
                        finalFtLeft = '0';
                    } else {
                        let resFtLeft = testFtLeft.substring(2, 4);
                        if (resFtLeft.length > 1) {
                            finalFtLeft = resFtLeft;
                        } else {
                            finalFtLeft = resFtLeft + '0';
                        }
                    }


                    return <View style={{ flexDirection: 'row', alignItems: 'center', height: 60, backgroundColor: key % 2 === 0 ? '#e8e8e8' : 'white' }}>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        {/* <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.TVName}</Text>
                        </View> */}
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{finalMinutes}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sPoints}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sAssists}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sReboundsTotal}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{finalFgLeft}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sBlocks}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sSteals}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{finalThreePointLeft}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{finalFtLeft}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sTurnovers}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sFoulsPersonal}</Text>
                        </View>
                    </View>

                });

                //box score starter left team 
                boxStarterLeft = starterLeft.map((val, key) => {
                    return <View key={key} style={{ backgroundColor: key % 2 === 0 ? '#e8e8e8' : 'white', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderRightWidth: 1, borderRightColor: '#797979' }}>
                        <View style={styles.columnViewStatsImage}>
                            <Image
                                style={styles.playerPic}
                                source={{
                                    uri: val.images.photo.T1.url
                                }}
                            />
                        </View>
                        <View style={styles.columnViewStatsPlayerName}>
                            <Text style={styles.recordsLabelStatsPlayerName}>{val.firstName}</Text>
                            <Text style={styles.recordsLabelStatsPlayerName}>{val.familyName}</Text>
                        </View>
                    </View>

                });

                //box score starter left team stats
                boxStarterStatsLeft = starterLeft.map((val, key) => {

                    let testMinutes = val.sMinutes.toString();
                    let finalMinutes = testMinutes.split('.')[0];

                    //compute field goal
                    let testFgLeft = val.sFieldGoalsPercentage.toString();

                    let finalFgLeft;

                    if (testFgLeft === '1') {
                        finalFgLeft = '100';
                    } else if (testFgLeft === '0') {
                        finalFgLeft = '0';
                    } else {
                        let resFgLeft = testFgLeft.substring(2, 4);
                        if (resFgLeft.length > 1) {
                            finalFgLeft = resFgLeft;
                        } else {
                            finalFgLeft = resFgLeft + '0';
                        }
                    }

                    //compute 3pt%
                    let testThreePointLeft = val.sThreePointersPercentage.toString();

                    let finalThreePointLeft;

                    if (testThreePointLeft === '1') {
                        finalThreePointLeft = '100';
                    } else if (testThreePointLeft === '0') {
                        finalThreePointLeft = '0';
                    } else {
                        let resThreePointLeft = testThreePointLeft.substring(2, 4);
                        if (resThreePointLeft.length > 1) {
                            finalThreePointLeft = resThreePointLeft;
                        } else {
                            finalThreePointLeft = resThreePointLeft + '0';
                        }
                    }

                    //compute ft%
                    let testFtLeft = val.sFreeThrowsPercentage.toString();

                    let finalFtLeft;

                    if (testFtLeft === '1') {
                        finalFtLeft = '100';
                    } else if (testFtLeft === '0') {
                        finalFtLeft = '0';
                    } else {
                        let resFtLeft = testFtLeft.substring(2, 4);
                        if (resFtLeft.length > 1) {
                            finalFtLeft = resFtLeft;
                        } else {
                            finalFtLeft = resFtLeft + '0';
                        }
                    }


                    return <View style={{ flexDirection: 'row', alignItems: 'center', height: 60, backgroundColor: key % 2 === 0 ? '#e8e8e8' : 'white' }}>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        {/* <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.TVName}</Text>
                        </View> */}
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{finalMinutes}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sPoints}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sAssists}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sReboundsTotal}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{finalFgLeft}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sBlocks}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sSteals}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{finalThreePointLeft}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{finalFtLeft}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sTurnovers}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sFoulsPersonal}</Text>
                        </View>
                    </View>

                });

                //box score bench left team 
                boxBenchLeft = benchLeft.map((val, key) => {
                    return <View key={key} style={{ backgroundColor: key % 2 === 0 ? '#e8e8e8' : 'white', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderRightWidth: 1, borderRightColor: '#797979' }}>
                        <View style={styles.columnViewStatsImage}>
                            <Image
                                style={styles.playerPic}
                                source={{
                                    uri: val.images.photo.T1.url
                                }}
                            />
                        </View>
                        <View style={styles.columnViewStatsPlayerName}>
                            <Text style={styles.recordsLabelStatsPlayerName}>{val.firstName}</Text>
                            <Text style={styles.recordsLabelStatsPlayerName}>{val.familyName}</Text>
                        </View>
                    </View>

                });

                //box score bench left team stats
                boxBenchStatsLeft = benchLeft.map((val, key) => {

                    let testMinutes = val.sMinutes.toString();
                    let finalMinutes = testMinutes.split('.')[0];

                    //compute field goal
                    let testFgLeft = val.sFieldGoalsPercentage.toString();

                    let finalFgLeft;

                    if (testFgLeft === '1') {
                        finalFgLeft = '100';
                    } else if (testFgLeft === '0') {
                        finalFgLeft = '0';
                    } else {
                        let resFgLeft = testFgLeft.substring(2, 4);
                        if (resFgLeft.length > 1) {
                            finalFgLeft = resFgLeft;
                        } else {
                            finalFgLeft = resFgLeft + '0';
                        }
                    }

                    //compute 3pt%
                    let testThreePointLeft = val.sThreePointersPercentage.toString();

                    let finalThreePointLeft;

                    if (testThreePointLeft === '1') {
                        finalThreePointLeft = '100';
                    } else if (testThreePointLeft === '0') {
                        finalThreePointLeft = '0';
                    } else {
                        let resThreePointLeft = testThreePointLeft.substring(2, 4);
                        if (resThreePointLeft.length > 1) {
                            finalThreePointLeft = resThreePointLeft;
                        } else {
                            finalThreePointLeft = resThreePointLeft + '0';
                        }
                    }

                    //compute ft%
                    let testFtLeft = val.sFreeThrowsPercentage.toString();

                    let finalFtLeft;

                    if (testFtLeft === '1') {
                        finalFtLeft = '100';
                    } else if (testFtLeft === '0') {
                        finalFtLeft = '0';
                    } else {
                        let resFtLeft = testFtLeft.substring(2, 4);
                        if (resFtLeft.length > 1) {
                            finalFtLeft = resFtLeft;
                        } else {
                            finalFtLeft = resFtLeft + '0';
                        }
                    }


                    return <View style={{ flexDirection: 'row', alignItems: 'center', height: 60, backgroundColor: key % 2 === 0 ? '#e8e8e8' : 'white' }}>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        {/* <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.TVName}</Text>
                        </View> */}
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{finalMinutes}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sPoints}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sAssists}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sReboundsTotal}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{finalFgLeft}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sBlocks}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sSteals}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{finalThreePointLeft}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{finalFtLeft}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sTurnovers}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sFoulsPersonal}</Text>
                        </View>
                    </View>

                });

                //start bench
                //box score starter left team 
                boxStarterRight = starterRight.map((val, key) => {
                    return <View key={key} style={{ backgroundColor: key % 2 === 0 ? '#e8e8e8' : 'white', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderRightWidth: 1, borderRightColor: '#797979' }}>
                        <View style={styles.columnViewStatsImage}>
                            <Image
                                style={styles.playerPic}
                                source={{
                                    uri: val.images.photo.T1.url
                                }}
                            />
                        </View>
                        <View style={styles.columnViewStatsPlayerName}>
                            <Text style={styles.recordsLabelStatsPlayerName}>{val.firstName}</Text>
                            <Text style={styles.recordsLabelStatsPlayerName}>{val.familyName}</Text>
                        </View>
                    </View>

                });

                //box score starter left team stats
                boxStarterStatsRight = starterRight.map((val, key) => {

                    let testMinutes = val.sMinutes.toString();
                    let finalMinutes = testMinutes.split('.')[0];

                    //compute field goal
                    let testFgLeft = val.sFieldGoalsPercentage.toString();

                    let finalFgLeft;

                    if (testFgLeft === '1') {
                        finalFgLeft = '100';
                    } else if (testFgLeft === '0') {
                        finalFgLeft = '0';
                    } else {
                        let resFgLeft = testFgLeft.substring(2, 4);
                        if (resFgLeft.length > 1) {
                            finalFgLeft = resFgLeft;
                        } else {
                            finalFgLeft = resFgLeft + '0';
                        }
                    }

                    //compute 3pt%
                    let testThreePointLeft = val.sThreePointersPercentage.toString();

                    let finalThreePointLeft;

                    if (testThreePointLeft === '1') {
                        finalThreePointLeft = '100';
                    } else if (testThreePointLeft === '0') {
                        finalThreePointLeft = '0';
                    } else {
                        let resThreePointLeft = testThreePointLeft.substring(2, 4);
                        if (resThreePointLeft.length > 1) {
                            finalThreePointLeft = resThreePointLeft;
                        } else {
                            finalThreePointLeft = resThreePointLeft + '0';
                        }
                    }

                    //compute ft%
                    let testFtLeft = val.sFreeThrowsPercentage.toString();

                    let finalFtLeft;

                    if (testFtLeft === '1') {
                        finalFtLeft = '100';
                    } else if (testFtLeft === '0') {
                        finalFtLeft = '0';
                    } else {
                        let resFtLeft = testFtLeft.substring(2, 4);
                        if (resFtLeft.length > 1) {
                            finalFtLeft = resFtLeft;
                        } else {
                            finalFtLeft = resFtLeft + '0';
                        }
                    }


                    return <View style={{ flexDirection: 'row', alignItems: 'center', height: 60, backgroundColor: key % 2 === 0 ? '#e8e8e8' : 'white' }}>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        {/* <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.TVName}</Text>
                        </View> */}
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{finalMinutes}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sPoints}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sAssists}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sReboundsTotal}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{finalFgLeft}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sBlocks}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sSteals}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{finalThreePointLeft}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{finalFtLeft}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sTurnovers}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sFoulsPersonal}</Text>
                        </View>
                    </View>

                });

                //box score bench left team 
                boxBenchRight = benchRight.map((val, key) => {
                    return <View key={key} style={{ backgroundColor: key % 2 === 0 ? '#e8e8e8' : 'white', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderRightWidth: 1, borderRightColor: '#797979' }}>
                        <View style={styles.columnViewStatsImage}>
                            <Image
                                style={styles.playerPic}
                                source={{
                                    uri: val.images.photo.T1.url
                                }}
                            />
                        </View>
                        <View style={styles.columnViewStatsPlayerName}>
                            <Text style={styles.recordsLabelStatsPlayerName}>{val.firstName}</Text>
                            <Text style={styles.recordsLabelStatsPlayerName}>{val.familyName}</Text>
                        </View>
                    </View>

                });

                //box score bench left team stats
                boxBenchStatsRight = benchRight.map((val, key) => {

                    let testMinutes = val.sMinutes.toString();
                    let finalMinutes = testMinutes.split('.')[0];

                    //compute field goal
                    let testFgLeft = val.sFieldGoalsPercentage.toString();

                    let finalFgLeft;

                    if (testFgLeft === '1') {
                        finalFgLeft = '100';
                    } else if (testFgLeft === '0') {
                        finalFgLeft = '0';
                    } else {
                        let resFgLeft = testFgLeft.substring(2, 4);
                        if (resFgLeft.length > 1) {
                            finalFgLeft = resFgLeft;
                        } else {
                            finalFgLeft = resFgLeft + '0';
                        }
                    }

                    //compute 3pt%
                    let testThreePointLeft = val.sThreePointersPercentage.toString();

                    let finalThreePointLeft;

                    if (testThreePointLeft === '1') {
                        finalThreePointLeft = '100';
                    } else if (testThreePointLeft === '0') {
                        finalThreePointLeft = '0';
                    } else {
                        let resThreePointLeft = testThreePointLeft.substring(2, 4);
                        if (resThreePointLeft.length > 1) {
                            finalThreePointLeft = resThreePointLeft;
                        } else {
                            finalThreePointLeft = resThreePointLeft + '0';
                        }
                    }

                    //compute ft%
                    let testFtLeft = val.sFreeThrowsPercentage.toString();

                    let finalFtLeft;

                    if (testFtLeft === '1') {
                        finalFtLeft = '100';
                    } else if (testFtLeft === '0') {
                        finalFtLeft = '0';
                    } else {
                        let resFtLeft = testFtLeft.substring(2, 4);
                        if (resFtLeft.length > 1) {
                            finalFtLeft = resFtLeft;
                        } else {
                            finalFtLeft = resFtLeft + '0';
                        }
                    }

                    return <View style={{ flexDirection: 'row', alignItems: 'center', height: 60, backgroundColor: key % 2 === 0 ? '#e8e8e8' : 'white' }}>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        {/* <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.TVName}</Text>
                        </View> */}
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{finalMinutes}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sPoints}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sAssists}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sReboundsTotal}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{finalFgLeft}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sBlocks}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sSteals}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{finalThreePointLeft}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{finalFtLeft}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sTurnovers}</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>{val.sFoulsPersonal}</Text>
                        </View>
                    </View>

                });


                //box score starter left team 
                boxStarterRight = starterRight.map((val, key) => {
                    return <View key={key} style={{ backgroundColor: key % 2 === 0 ? '#e8e8e8' : 'white', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderRightWidth: 1, borderRightColor: '#797979' }}>
                        <View style={styles.columnViewStatsImage}>
                            <Image
                                style={styles.playerPic}
                                source={{
                                    uri: val.images.photo.T1.url
                                }}
                            />
                        </View>
                        <View style={styles.columnViewStatsPlayerName}>
                            <Text style={styles.recordsLabelStatsPlayerName}>{val.firstName}</Text>
                            <Text style={styles.recordsLabelStatsPlayerName}>{val.familyName}</Text>
                        </View>
                    </View>

                });

                //playbyplay 
                playbyplay = newPlay.sort((a, b) => b.actionNumber - a.actionNumber).map((val, key) => {

                    let result;
                    let actionResult;
                    let periodResult;

                    if (val.actionType == 'game') {
                        if (val.subType == 'end') {
                            result = 'GAME END';
                        } else {
                            result = 'GAME START';
                        }

                        return <View style={styles.rowInfoEnd}>
                            <Text style={styles.labelQuartsz}>{result}</Text>
                        </View>
                    }

                    else if (val.actionType == 'period') {
                        if (val.subType == 'end') {
                            result = 'PERIOD END';
                        } else {
                            result = 'PERIOD START';
                        }

                        return <View style={styles.rowInfoEnd}>
                            <Text style={styles.labelQuartsz}>{result}</Text>
                        </View>

                    } else {

                        if (val.success == 1) {
                            actionResult = 'made';
                        }

                        if (val.success == 0) {
                            actionResult = 'missed';
                        }

                        if (val.periodType == 'REGULAR') {
                            periodResult = 'Q';
                        } else {
                            periodResult = 'OT';
                        }

                        let trimTime = val.clock.substring(0, 5);
                        let teamCodePlay;

                        if (val.teamId == 88269) {
                            teamCodePlay = 'HKE';
                        }
                        if (val.teamId == 124987) {
                            teamCodePlay = 'FUB';
                        }
                        if (val.teamId == 88263) {
                            teamCodePlay = 'FMD';
                        }
                        if (val.teamId == 102547) {
                            teamCodePlay = 'MBB';
                        }
                        if (val.teamId == 25496) {
                            teamCodePlay = 'MNV';
                        }
                        if (val.teamId == 100) {
                            teamCodePlay = 'SGH';
                        }
                        if (val.teamId == 88264) {
                            teamCodePlay = 'SAP';
                        }
                        if (val.teamId == 79) {
                            teamCodePlay = 'SGS';
                        }
                        if (val.teamId == 104) {
                            teamCodePlay = 'KLD';
                        }
                        if (val.teamId == 102548) {
                            teamCodePlay = 'MWW';
                        }

                        let isPlayer;

                        let playersLeft = this.state.playInfoLeft.map((ey, key) => {
                            if (val.personId == ey.personId) {
                                isPlayer = ey.images.photo.T1.url;
                            }
                        });

                        let playersRight = this.state.playInfoRight.map((ey, key) => {
                            if (val.personId == ey.personId) {
                                isPlayer = ey.images.photo.T1.url;
                            }
                        });

                        return <View key={key} style={styles.rowInfo}>
                            <View style={{ width: 25 + '%', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', borderRightWidth: 1, borderRightColor: '#e3e3e3' }}>
                                <Text style={styles.infoLabelScoresLabelPbp}>{periodResult}{val.period} {trimTime}</Text>
                                <Text style={styles.infoLabelScoresLabelPbpNo}>{teamCodePlay}</Text>
                                <Text style={styles.infoLabelScoresLabelPbpNo}>{val.score1}-{val.score2}</Text>
                            </View>
                            <View style={{ width: 75 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', paddingLeft: 5, flexWrap: "wrap" }}>
                                <Image
                                    style={styles.userPic}
                                    source={{
                                        uri: isPlayer
                                    }}
                                />
                                <Text style={styles.infoLabelScoresLabelPbpNoText}>{val.familyName} {val.subType} {val.actionType} {actionResult}</Text>
                            </View>
                        </View>
                    }
                });

            } else {

                playbyplay = <View></View>;

                resFtLeft = '-';
                resFtRight = '-';
                resThreeLeft = '-';
                resThreeRight = '-';
                resFieldLeft = '-';
                resFieldRight = '-';

                leftAssists = '-';
                RightAssists = '-';

                leftReboundsTotal = '-';
                RightReboundsTotal = '-';

                leftReboundsOffensive = '-';
                RightReboundsOffensive = '-';

                leftsReboundsDefensive = '-';
                RightsReboundsDefensive = '-';

                LeftSteals = '-';
                RightSteals = '-';

                LeftBlocks = '-';
                RightBlocks = '-';

                LeftTurnovers = '-';
                RightTurnovers = '-';

                LeftFoulsTotal = '-';
                RightFoulsTotal = '-';

                quarterOneLeft = '-';
                quarterOneLeft = '-';
                quarterTwoLeft = '-';
                quarterThreeLeft = '-';
                quarterFourLeft = '-';

                quarterOneRight = '-';
                quarterOneRight = '-';
                quarterTwoRight = '-';
                quarterThreeRight = '-';
                quarterFourRight = '-';

                totalLeft = '-';
                totalRight = '-';

                otOneLeft = <View></View>;
                otOneLeftLabel = <View></View>;
                otOneRight = <View></View>;
                
                boxStarterLeft = <View>
                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderRightWidth: 1, borderRightColor: '#797979' }}>
                        <View style={styles.columnViewStatsImage}>
                            <Image
                                style={styles.playerPic}
                                source={{
                                    uri: 'https://img.wh.sportingpulseinternational.com/2a44dd848a8faa2a626104ff5aacd4eaS1.png'
                                }}
                            />
                        </View>
                        <View style={styles.columnViewStatsPlayerName}>
                            <Text style={styles.recordsLabelStatsPlayerName}>-</Text>
                            <Text style={styles.recordsLabelStatsPlayerName}>-</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderRightWidth: 1, borderRightColor: '#797979' }}>
                        <View style={styles.columnViewStatsImage}>
                            <Image
                                style={styles.playerPic}
                                source={{
                                    uri: 'https://img.wh.sportingpulseinternational.com/2a44dd848a8faa2a626104ff5aacd4eaS1.png'
                                }}
                            />
                        </View>
                        <View style={styles.columnViewStatsPlayerName}>
                            <Text style={styles.recordsLabelStatsPlayerName}>-</Text>
                            <Text style={styles.recordsLabelStatsPlayerName}>-</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderRightWidth: 1, borderRightColor: '#797979' }}>
                        <View style={styles.columnViewStatsImage}>
                            <Image
                                style={styles.playerPic}
                                source={{
                                    uri: 'https://img.wh.sportingpulseinternational.com/2a44dd848a8faa2a626104ff5aacd4eaS1.png'
                                }}
                            />
                        </View>
                        <View style={styles.columnViewStatsPlayerName}>
                            <Text style={styles.recordsLabelStatsPlayerName}>-</Text>
                            <Text style={styles.recordsLabelStatsPlayerName}>-</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderRightWidth: 1, borderRightColor: '#797979' }}>
                        <View style={styles.columnViewStatsImage}>
                            <Image
                                style={styles.playerPic}
                                source={{
                                    uri: 'https://img.wh.sportingpulseinternational.com/2a44dd848a8faa2a626104ff5aacd4eaS1.png'
                                }}
                            />
                        </View>
                        <View style={styles.columnViewStatsPlayerName}>
                            <Text style={styles.recordsLabelStatsPlayerName}>-</Text>
                            <Text style={styles.recordsLabelStatsPlayerName}>-</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderRightWidth: 1, borderRightColor: '#797979' }}>
                        <View style={styles.columnViewStatsImage}>
                            <Image
                                style={styles.playerPic}
                                source={{
                                    uri: 'https://img.wh.sportingpulseinternational.com/2a44dd848a8faa2a626104ff5aacd4eaS1.png'
                                }}
                            />
                        </View>
                        <View style={styles.columnViewStatsPlayerName}>
                            <Text style={styles.recordsLabelStatsPlayerName}>-</Text>
                            <Text style={styles.recordsLabelStatsPlayerName}>-</Text>
                        </View>
                    </View>
                </View>;

                boxStarterStatsLeft = <View>
                    <View style={{ flexDirection: 'row', alignItems: 'center', height: 60 }}>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center', height: 60 }}>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center', height: 60 }}>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center', height: 60 }}>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center', height: 60 }}>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                    </View>
                </View>;

                boxStarterRight = <View>
                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderRightWidth: 1, borderRightColor: '#797979' }}>
                        <View style={styles.columnViewStatsImage}>
                            <Image
                                style={styles.playerPic}
                                source={{
                                    uri: 'https://img.wh.sportingpulseinternational.com/2a44dd848a8faa2a626104ff5aacd4eaS1.png'
                                }}
                            />
                        </View>
                        <View style={styles.columnViewStatsPlayerName}>
                            <Text style={styles.recordsLabelStatsPlayerName}>-</Text>
                            <Text style={styles.recordsLabelStatsPlayerName}>-</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderRightWidth: 1, borderRightColor: '#797979' }}>
                        <View style={styles.columnViewStatsImage}>
                            <Image
                                style={styles.playerPic}
                                source={{
                                    uri: 'https://img.wh.sportingpulseinternational.com/2a44dd848a8faa2a626104ff5aacd4eaS1.png'
                                }}
                            />
                        </View>
                        <View style={styles.columnViewStatsPlayerName}>
                            <Text style={styles.recordsLabelStatsPlayerName}>-</Text>
                            <Text style={styles.recordsLabelStatsPlayerName}>-</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderRightWidth: 1, borderRightColor: '#797979' }}>
                        <View style={styles.columnViewStatsImage}>
                            <Image
                                style={styles.playerPic}
                                source={{
                                    uri: 'https://img.wh.sportingpulseinternational.com/2a44dd848a8faa2a626104ff5aacd4eaS1.png'
                                }}
                            />
                        </View>
                        <View style={styles.columnViewStatsPlayerName}>
                            <Text style={styles.recordsLabelStatsPlayerName}>-</Text>
                            <Text style={styles.recordsLabelStatsPlayerName}>-</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderRightWidth: 1, borderRightColor: '#797979' }}>
                        <View style={styles.columnViewStatsImage}>
                            <Image
                                style={styles.playerPic}
                                source={{
                                    uri: 'https://img.wh.sportingpulseinternational.com/2a44dd848a8faa2a626104ff5aacd4eaS1.png'
                                }}
                            />
                        </View>
                        <View style={styles.columnViewStatsPlayerName}>
                            <Text style={styles.recordsLabelStatsPlayerName}>-</Text>
                            <Text style={styles.recordsLabelStatsPlayerName}>-</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderRightWidth: 1, borderRightColor: '#797979' }}>
                        <View style={styles.columnViewStatsImage}>
                            <Image
                                style={styles.playerPic}
                                source={{
                                    uri: 'https://img.wh.sportingpulseinternational.com/2a44dd848a8faa2a626104ff5aacd4eaS1.png'
                                }}
                            />
                        </View>
                        <View style={styles.columnViewStatsPlayerName}>
                            <Text style={styles.recordsLabelStatsPlayerName}>-</Text>
                            <Text style={styles.recordsLabelStatsPlayerName}>-</Text>
                        </View>
                    </View>
                </View>;

                boxStarterStatsRight = <View>
                    <View style={{ flexDirection: 'row', alignItems: 'center', height: 60 }}>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center', height: 60 }}>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center', height: 60 }}>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center', height: 60 }}>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center', height: 60 }}>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}> </Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                        <View style={styles.columnViewStats}>
                            <Text style={styles.recordsLabelStats}>-</Text>
                        </View>
                    </View>
                </View>;
                boxBenchRight = <View></View>;
            }

            teamLeft = this.state.dataSource[0].teamId;
            teamRight = this.state.dataSource[1].teamId;

            if (teamLeft == '88269') {
                teamCodeLeft = 'HKE';
                teamNameLeft = 'EASTERN';
            }
            if (teamLeft == '88262') {
                teamCodeLeft = 'CLS';
                teamNameLeft = 'KNIGHTS';
            }
            if (teamLeft == '88263') {
                teamCodeLeft = 'FMD';
                teamNameLeft = 'DREAMERS';
            }
            if (teamLeft == '102547') {
                teamCodeLeft = 'MBB';
                teamNameLeft = 'BEARS';
            }
            if (teamLeft == '25496') {
                teamCodeLeft = 'MNV';
                teamNameLeft = 'VAMPIRE';
            }
            if (teamLeft == '100') {
                teamCodeLeft = 'SGH';
                teamNameLeft = 'HEAT';
            }
            if (teamLeft == '88264') {
                teamCodeLeft = 'SAP';
                teamNameLeft = 'ALAB';
            }
            if (teamLeft == '79') {
                teamCodeLeft = 'SGS';
                teamNameLeft = 'SLINGERS';
            }
            if (teamLeft == '104') {
                teamCodeLeft = 'KLD';
                teamNameLeft = 'DRAGONS';
            }
            if (teamLeft == '102548') {
                teamCodeLeft = 'MWW';
                teamNameLeft = 'WARRIORS';
            }
            if (teamRight == '88269') {
                teamCodeRight = 'HKE';
                teamNameRight = 'EASTERN';
            }
            if (teamRight == '124987') {
                teamCodeRight = 'FUB';
                teamNameRight = 'BRAVES';
            }
            if (teamRight == '88263') {
                teamCodeRight = 'FMD';
                teamNameRight = 'DREAMERS';
            }
            if (teamRight == '102547') {
                teamCodeRight = 'MBB';
                teamNameRight = 'BEARS';
            }
            if (teamRight == '25496') {
                teamCodeRight = 'MNV';
                teamNameRight = 'VAMPIRE';
            }
            if (teamRight == '100') {
                teamCodeRight = 'SGH';
                teamNameRight = 'HEAT';
            }
            if (teamRight == '88264') {
                teamCodeRight = 'SAP';
                teamNameRight = 'ALAB';
            }
            if (teamRight == '79') {
                teamCodeRight = 'SGS';
                teamNameRight = 'SLINGERS';
            }
            if (teamRight == '104') {
                teamCodeRight = 'KLD';
                teamNameRight = 'DRAGONS';
            }
            if (teamRight == '102548') {
                teamCodeRight = 'MWW';
                teamNameRight = 'WARRIORS';
            }

            if (this.state.activeIndex == 0) {

                //points group
                let lastNamePointsLeft;
                let leftPointsLeader;
                let leftPointsLeaderLastName;
                let leftPointsLeaderFirstName;
                let leftPointsLeaderImage;

                let lastNamePointsRight;
                let rightPointsLeader;
                let rightPointsLeaderLastName;
                let rightPointsLeaderFirstName;
                let rightPointsLeaderImage;

                //rebounds group
                let lastNameReboundsLeft;
                let leftReboundsLeader;
                let leftReboundsLeaderLastName;
                let leftReboundsLeaderFirstName;
                let leftReboundsLeaderImage;

                let lastNameReboundsRight;
                let rightReboundsLeader;
                let rightReboundsLeaderLastName;
                let rightReboundsLeaderFirstName;
                let rightReboundsLeaderImage;

                //assists group
                let lastNameAssistsLeft;
                let leftAssistsLeader;
                let leftAssistsLeaderLastName;
                let leftAssistsLeaderFirstName;
                let leftAssistsLeaderImage;

                let lastNameAssistsRight;
                let rightAssistsLeader;
                let rightAssistsLeaderLastName;
                let rightAssistsLeaderFirstName;
                let rightAssistsLeaderImage;
                let leftPointsLeaderLastNameFinal;
                let rightPointsLeaderLastNameFinal;
                let leftReboundsLeaderLastNameFinal;
                let rightReboundsLeaderLastNameFinal;
                let leftAssistsLeaderLastNameFinal;
                let rightAssistsLeaderLastNameFinal;
                //points group
                this.state.leadersLeft.sort((a, b) => a.sPoints - b.sPoints).map((val, key) => {


                    lastNamePointsLeft = val.familyName.toLowerCase();
                    leftPointsLeaderLastName = lastNamePointsLeft.charAt(0).toUpperCase() + lastNamePointsLeft.slice(1);

                    leftPointsLeaderFirstName = val.firstName.charAt(0);
                    leftPointsLeader = val.sPoints;
                    leftPointsLeaderImage = val.images.photo.S1.url;
                })

                this.state.leadersRight.sort((a, b) => a.sPoints - b.sPoints).map((val, key) => {
                    
                    lastNamePointsRight = val.familyName.toLowerCase();
                    
                    rightPointsLeaderLastName = lastNamePointsRight.charAt(0).toUpperCase() + lastNamePointsRight.slice(1);
              
                    rightPointsLeaderFirstName = val.firstName.charAt(0);
                    rightPointsLeader = val.sPoints;
                    rightPointsLeaderImage = val.images.photo.S1.url;
                })

                //rebounds group
                this.state.leadersLeft.sort((a, b) => a.sReboundsTotal - b.sReboundsTotal).map((val, key) => {
                    
                    lastNameReboundsLeft = val.familyName.toLowerCase();
                   
                    leftReboundsLeaderLastName = lastNameReboundsLeft.charAt(0).toUpperCase() + lastNameReboundsLeft.slice(1);
                
                    leftReboundsLeaderFirstName = val.firstName.charAt(0);
                    leftReboundsLeader = val.sReboundsTotal;
                    leftReboundsLeaderImage = val.images.photo.S1.url;
                })

                this.state.leadersRight.sort((a, b) => a.sReboundsTotal - b.sReboundsTotal).map((val, key) => {
                    
                    lastNameReboundsRight = val.familyName.toLowerCase();
                   
                    rightReboundsLeaderLastName = lastNameReboundsRight.charAt(0).toUpperCase() + lastNameReboundsRight.slice(1);
                   
                    rightReboundsLeaderFirstName = val.firstName.charAt(0);
                    rightReboundsLeader = val.sReboundsTotal;
                    rightReboundsLeaderImage = val.images.photo.S1.url;
                })

                //assists group
                this.state.leadersLeft.sort((a, b) => a.sAssists - b.sAssists).map((val, key) => {
                    
                    lastNameAssistsLeft = val.familyName.toLowerCase();
                    
                    leftAssistsLeaderLastName = lastNameAssistsLeft.charAt(0).toUpperCase() + lastNameAssistsLeft.slice(1);
              
                    leftAssistsLeaderFirstName = val.firstName.charAt(0);
                    leftAssistsLeader = val.sAssists;
                    leftAssistsLeaderImage = val.images.photo.S1.url;
                })

                this.state.leadersRight.sort((a, b) => a.sAssists - b.sAssists).map((val, key) => {
                    
                    lastNameAssistsRight = val.familyName.toLowerCase();
                   
                    rightAssistsLeaderLastName = lastNameAssistsRight.charAt(0).toUpperCase() + lastNameAssistsRight.slice(1);
                 
                    rightAssistsLeaderFirstName = val.firstName.charAt(0);
                    rightAssistsLeader = val.sAssists;
                    rightAssistsLeaderImage = val.images.photo.S1.url;
                })

                return (
                    <View>
                        <View style={styles.group}>
                            <View style={styles.rowLabel}>
                                <Text style={styles.label}>SCORES</Text>
                            </View>
                            <View style={styles.rowView}>
                                <View style={{ flexDirection: 'column', position: 'absolute', backgroundColor: 'white', zIndex: 10 }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={styles.columnViewStatsFix}>
                                            <Text style={styles.recordsLabelStatsFix}></Text>
                                        </View>
                                        <View style={styles.columnViewStatsFix}>
                                            <Text style={styles.recordsLabelStatsFix}> </Text>
                                        </View>
                                    </View>
                                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderRightWidth: 1, borderRightColor: '#797979' }}>
                                        <View style={styles.columnViewStatsTeamName}>
                                            <Text style={styles.recordsLabelStatsTeamName}>{teamCodeLeft}</Text>
                                        </View>
                                    </View>
                                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderRightWidth: 1, borderRightColor: '#797979' }}>
                                        <View style={styles.columnViewStatsTeamName}>
                                            <Text style={styles.recordsLabelStatsTeamName}>{teamCodeRight}</Text>
                                        </View>
                                    </View>
                                </View>

                                <ScrollView horizontal={true}>
                                    <View style={{ flexDirection: 'column' }}>
                                        <View style={{ flexDirection: 'row' }}>
                                            <View style={styles.columnViewStats}>
                                                <Text style={styles.recordsLabelStatsTeam}></Text>
                                            </View>
                                            <View style={styles.columnViewStats}>
                                                <Text style={styles.recordsLabelStatsTeam}></Text>
                                            </View>
                                            <View style={styles.columnViewStats}>
                                                <Text style={styles.recordsLabelStatsTeam}>Q1</Text>
                                            </View>
                                            <View style={styles.columnViewStats}>
                                                <Text style={styles.recordsLabelStatsTeam}>Q2</Text>
                                            </View>
                                            <View style={styles.columnViewStats}>
                                                <Text style={styles.recordsLabelStatsTeam}>Q3</Text>
                                            </View>
                                            <View style={styles.columnViewStats}>
                                                <Text style={styles.recordsLabelStatsTeam}>Q4</Text>
                                            </View>
                                            {otOneLeftLabel}                                                                                                                                                                                             
                                            <View style={styles.columnViewStats}>
                                                <Text style={styles.recordsLabelStatsTeam}>TOT</Text>
                                            </View>
                                        </View>
                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                            <View style={styles.columnViewStats}>
                                                <Text style={styles.recordsLabelStatsTeamScoreOne}></Text>
                                            </View>
                                            <View style={styles.columnViewStats}>
                                                <Text style={styles.recordsLabelStatsTeamScoreOne}></Text>
                                            </View>
                                            <View style={styles.columnViewStats}>
                                                <Text style={styles.recordsLabelStatsTeamScoreOne}>{quarterOneLeft}</Text>
                                            </View>
                                            <View style={styles.columnViewStats}>
                                                <Text style={styles.recordsLabelStatsTeamScoreOne}>{quarterTwoLeft}</Text>
                                            </View>
                                            <View style={styles.columnViewStats}>
                                                <Text style={styles.recordsLabelStatsTeamScoreOne}>{quarterThreeLeft}</Text>
                                            </View>
                                            <View style={styles.columnViewStats}>
                                                <Text style={styles.recordsLabelStatsTeamScoreOne}>{quarterFourLeft}</Text>
                                            </View>
                                            {otOneLeft}
                                            {otTwoLeft}
                                            <View style={styles.columnViewStats}>
                                                <Text style={styles.recordsLabelStatsTeamScoreOne}>{totalLeft}</Text>
                                            </View>
                                        </View>
                                        <View style={{ flexDirection: 'row', alignItems: 'center', height: 60 }}>
                                            <View style={styles.columnViewStats}>
                                                <Text style={styles.recordsLabelStatsTeamScoreTwo}></Text>
                                            </View>
                                            <View style={styles.columnViewStats}>
                                                <Text style={styles.recordsLabelStatsTeamScoreTwo}></Text>
                                            </View>
                                            <View style={styles.columnViewStats}>
                                                <Text style={styles.recordsLabelStatsTeamScoreTwo}>{quarterOneRight}</Text>
                                            </View>
                                            <View style={styles.columnViewStats}>
                                                <Text style={styles.recordsLabelStatsTeamScoreTwo}>{quarterTwoRight}</Text>
                                            </View>
                                            <View style={styles.columnViewStats}>
                                                <Text style={styles.recordsLabelStatsTeamScoreTwo}>{quarterThreeRight}</Text>
                                            </View>
                                            <View style={styles.columnViewStats}>
                                                <Text style={styles.recordsLabelStatsTeamScoreTwo}>{quarterFourRight}</Text>
                                            </View>
                                            {otOneRight}
                                            {otTwoRight}
                                            <View style={styles.columnViewStats}>
                                                <Text style={styles.recordsLabelStatsTeamScoreTwo}>{totalRight}</Text>
                                            </View>
                                        </View>
                                    </View>
                                </ScrollView>
                            </View>
                        </View>

                        <View style={styles.group}>
                            <View style={styles.rowLabel}>
                                <Text style={styles.label}>GAME LEADERS</Text>
                            </View>
                            <View style={styles.rowInfo}>
                                <View style={{ width: 50 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                    <Text style={styles.infoLabelScoresLabelComparisonLabel}>{teamCodeLeft}</Text>
                                </View>
                                <View style={{ width: 50 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                                    <Text style={styles.infoLabelScoresLabelComparisonLabel}>{teamCodeRight}</Text>
                                </View>
                            </View>
                            <View style={styles.rowInfoComparisonBar}>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                    <Image
                                        style={styles.playerPic}
                                        source={{
                                            uri: leftPointsLeaderImage
                                        }}
                                    />
                                    <Text style={styles.comparisonValueLeft}>{leftPointsLeader}</Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={styles.comparisonLabel}>POINTS</Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                                    <Text style={styles.comparisonValueRight}>{rightPointsLeader}</Text>
                                    <Image
                                        style={styles.playerPic}
                                        source={{
                                            uri: rightPointsLeaderImage
                                        }}
                                    />
                                </View>
                            </View>
                            <View style={styles.rowInfoComparisonName}>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                    <Text style={styles.comparisonValueNameLeft}>{leftPointsLeaderFirstName}. {leftPointsLeaderLastName}</Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={styles.comparisonLabel}></Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                                    <Text style={styles.comparisonValueNameRight}>{rightPointsLeaderFirstName}. {rightPointsLeaderLastName}</Text>
                                </View>
                            </View>

                            <View style={styles.rowInfoComparisonBar}>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                    <Image
                                        style={styles.playerPic}
                                        source={{
                                            uri: leftReboundsLeaderImage
                                        }}
                                    />
                                    <Text style={styles.comparisonValueLeft}>{leftReboundsLeader}</Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={styles.comparisonLabel}>REBOUNDS</Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                                    <Text style={styles.comparisonValueRight}>{rightReboundsLeader}</Text>
                                    <Image
                                        style={styles.playerPic}
                                        source={{
                                            uri: rightReboundsLeaderImage
                                        }}
                                    />
                                </View>
                            </View>
                            <View style={styles.rowInfoComparisonName}>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                    <Text style={styles.comparisonValueNameLeft}>{leftReboundsLeaderFirstName}. {leftReboundsLeaderLastName}</Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={styles.comparisonLabel}></Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                                    <Text style={styles.comparisonValueNameRight}>{rightReboundsLeaderFirstName}. {rightReboundsLeaderLastName}</Text>
                                </View>
                            </View>

                            <View style={styles.rowInfoComparisonBar}>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                    <Image
                                        style={styles.playerPic}
                                        source={{
                                            uri: leftAssistsLeaderImage
                                        }}
                                    />
                                    <Text style={styles.comparisonValueLeft}>{leftAssistsLeader}</Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={styles.comparisonLabel}>ASSISTS</Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                                    <Text style={styles.comparisonValueRight}>{rightAssistsLeader}</Text>
                                    <Image
                                        style={styles.playerPic}
                                        source={{
                                            uri: rightAssistsLeaderImage
                                        }}
                                    />
                                </View>
                            </View>
                            <View style={styles.rowInfoComparisonName}>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                    <Text style={styles.comparisonValueNameLeft}>{leftAssistsLeaderFirstName}. {leftAssistsLeaderLastName}</Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}> 
                                    <Text style={styles.comparisonLabel}></Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                                    <Text style={styles.comparisonValueNameRight}>{rightAssistsLeaderFirstName}. {rightAssistsLeaderLastName}</Text>
                                </View>
                            </View>
                        </View>
                        <View style={styles.group}>
                            <View style={styles.rowLabel}>
                                <Text style={styles.label}>TEAM COMPARISON</Text>
                            </View>
                            <View style={styles.rowInfo}>
                                <View style={{ width: 50 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                    <Text style={styles.infoLabelScoresLabelComparisonLabel}>{teamCodeLeft}</Text>
                                </View>
                                <View style={{ width: 50 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                                    <Text style={styles.infoLabelScoresLabelComparisonLabel}>{teamCodeRight}</Text>
                                </View>
                            </View>
                            <View style={styles.rowInfoComparison}>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                    <Text style={styles.comparisonValue}>{resFieldLeft}</Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={styles.comparisonLabel}>FG%</Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                                    <Text style={styles.comparisonValue}>{resFieldRight}</Text>
                                </View>
                            </View>
                            <View style={styles.rowInfoComparison}>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                    <Text style={styles.comparisonValue}>{resThreeLeftFinal}</Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={styles.comparisonLabel}>3P%</Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                                    <Text style={styles.comparisonValue}>{resThreeRightFinal}</Text>
                                </View>
                            </View>
                            <View style={styles.rowInfoComparison}>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                    <Text style={styles.comparisonValue}>{resFtLeft}</Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={styles.comparisonLabel}>FT%</Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                                    <Text style={styles.comparisonValue}>{resFtRight}</Text>
                                </View>
                            </View>
                            <View style={styles.rowInfoComparison}>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                    <Text style={styles.comparisonValue}>{leftAssists}</Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={styles.comparisonLabel}>Assists</Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                                    <Text style={styles.comparisonValue}>{RightAssists}</Text>
                                </View>
                            </View>
                            <View style={styles.rowInfoComparison}>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                    <Text style={styles.comparisonValue}>{leftReboundsTotal}</Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={styles.comparisonLabel}>Total Rebounds</Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                                    <Text style={styles.comparisonValue}>{RightReboundsTotal}</Text>
                                </View>
                            </View>
                            <View style={styles.rowInfoComparison}>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                    <Text style={styles.comparisonValue}>{leftReboundsOffensive}</Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={styles.comparisonLabel}>Offensive Rebounds</Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                                    <Text style={styles.comparisonValue}>{RightReboundsOffensive}</Text>
                                </View>
                            </View>
                            <View style={styles.rowInfoComparison}>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                    <Text style={styles.comparisonValue}>{leftsReboundsDefensive}</Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={styles.comparisonLabel}>Defensive Rebounds</Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                                    <Text style={styles.comparisonValue}>{RightsReboundsDefensive}</Text>
                                </View>
                            </View>
                            <View style={styles.rowInfoComparison}>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                    <Text style={styles.comparisonValue}>{LeftSteals}</Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={styles.comparisonLabel}>Steals</Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                                    <Text style={styles.comparisonValue}>{RightSteals}</Text>
                                </View>
                            </View>
                            <View style={styles.rowInfoComparison}>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                    <Text style={styles.comparisonValue}>{LeftBlocks}</Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={styles.comparisonLabel}>Blocks</Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                                    <Text style={styles.comparisonValue}>{RightBlocks}</Text>
                                </View>
                            </View>
                            <View style={styles.rowInfoComparison}>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                    <Text style={styles.comparisonValue}>{LeftTurnovers}</Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={styles.comparisonLabel}>Turnovers</Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                                    <Text style={styles.comparisonValue}>{RightTurnovers}</Text>
                                </View>
                            </View>
                            <View style={styles.rowInfoComparison}>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                    <Text style={styles.comparisonValue}>{LeftFoulsTotal}</Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={styles.comparisonLabel}>Fouls</Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                                    <Text style={styles.comparisonValue}>{RightFoulsTotal}</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                )
            }

            if (this.state.activeIndex == 1) {

                let rest;
                let ui;

                if (this.state.boxInfoLeft.length !== 0) {
                    rest = <View></View>;
                } else {
                    rest = <View>
                        {
                            this.state.showMe ?
                                <ActivityIndicator style={{ marginTop: 50 }} size="large" color="#000" />
                                :
                                <View></View>
                        }
                    </View>;
                    this.getboxscore();
                }
                return (
                    <View style={{ flexDirection: 'row', width: 100 + '%' }}>
                        <View style={styles.group}>
                            {rest}
                            {this.renderBox()}
                        </View>
                    </View>
                )
            }

            if (this.state.activeIndex == 2) {

                let rest;
                let ui;

                if (this.state.playInfotest.length !== 0) {
                    rest = <View></View>;
                } else {
                    rest = <View>
                        <ActivityIndicator style={{ marginTop: 50 }} size="large" color="#000" />
                    </View>;
                    this.getplaybyplay();
                }
                return (
                    <View>
                        <View style={styles.group}>
                            {rest}
                            {this.renderEvented()}
                        </View>
                    </View>
                )
            } // end active index 2

        }
    }

    hideSpinner() {
        this.setState({
            visible: false,
        });
    }

    _onRefreshPage = () => {
        this.setState({ showErrorPage: false }, this.getwow);
    }

    render() {

        if (this.state.showErrorPage) {
            return (
                <View style={{ flex: 1, justifyContent: "center", alignItems: "center", paddingHorizontal: 25, backgroundColor: 'white' }}>
                    <Image
                        style={{ width: 80, height: 80, marginBottom: 15, }}
                        source={require('./src/antenna.png')}
                    />
                    <Text style={styles.errorTitle}>No Internet Connection</Text>
                    <Text style={styles.errorText}>An error has occurred. Try checking your network settings</Text>
                    <Button
                        onPress={this._onRefreshPage.bind(this)}
                        title="Try Again"
                        color="#CE1A19"
                    />
                </View>
            )
        }

        if (this.state.isLoading) {
            return (
                <View style={styles.containerLoading}>
                    <ActivityIndicator size="large" color="#fff" />
                </View>
            )
        } else {

            if (this.state.matchstatus == "IN_PROGRESS") {
                return (
                    <View style={styles.container}>
                        <WebView
                            originWhitelist={['*']}
                            source={{ uri: 'http://www.fibalivestats.com/u/abl/' + this.state.matchid }}
                            style={{ marginTop: -30, backgroundColor: '#000000', }}
                            onLoad={() => this.hideSpinner()}
                        // renderLoading={ ()=> {return(<ActivityIndicator size="large" color="#000" style = {{position: 'absolute',left: 0,right: 0, top: 0,bottom: 0,alignItems: 'center',justifyContent: 'center'}}/>)} }
                        // startInLoadingState
                        />
                        {this.state.visible && (
                            <ActivityIndicator
                                style={{ position: "absolute", top: 50 + '%', left: 45 + '%', alignItems: 'center', justifyContent: 'center' }}
                                size="large"
                                color="#fff"
                            />
                        )}
                    </View>
                )
            }

            if (this.state.matchstatus == "COMPLETE" || this.state.matchstatus == "FINISHED" || this.state.matchstatus == "SCHEDULED") {

                let teamLeft = this.state.dataSource[0].teamId;
                let teamRight = this.state.dataSource[1].teamId;

                let teamCodeLeft;
                let teamCodeRight;
                let standingLeft;
                let standingRight;

                if (teamLeft == '88269') {
                    teamCodeLeft = 'HKE';
                }
                if (teamLeft == '124987') {
                    teamCodeLeft = 'FUB';
                }
                if (teamLeft == '88263') {
                    teamCodeLeft = 'FMD';
                }
                if (teamLeft == '102547') {
                    teamCodeLeft = 'MBB';
                }
                if (teamLeft == '25496') {
                    teamCodeLeft = 'MNV';
                }
                if (teamLeft == '100') {
                    teamCodeLeft = 'SGH';
                }
                if (teamLeft == '88264') {
                    teamCodeLeft = 'SAP';
                }
                if (teamLeft == '79') {
                    teamCodeLeft = 'SGS';
                }
                if (teamLeft == '104') {
                    teamCodeLeft = 'KLD';
                }
                if (teamLeft == '102548') {
                    teamCodeLeft = 'MWW';
                }
                if (teamRight == '88269') {
                    teamCodeRight = 'HKE';
                }
                if (teamRight == '124987') {
                    teamCodeRight = 'FUB';
                }
                if (teamRight == '88263') {
                    teamCodeRight = 'FMD';
                }
                if (teamRight == '102547') {
                    teamCodeRight = 'MBB';
                }
                if (teamRight == '25496') {
                    teamCodeRight = 'MNV';
                }
                if (teamRight == '100') {
                    teamCodeRight = 'SGH';
                }
                if (teamRight == '88264') {
                    teamCodeRight = 'SAP';
                }
                if (teamRight == '79') {
                    teamCodeRight = 'SGS';
                }
                if (teamRight == '104') {
                    teamCodeRight = 'KLD';
                }
                if (teamRight == '102548') {
                    teamCodeRight = 'MWW';
                }

                let teamImageLeft;
                let teamImageRight;

                let leftEw = this.state.dataSource[0].teamId;
                let RightEw = this.state.dataSource[1].teamId;

                let standings = this.state.standingInfo.map((ey, key) => {
                    if (leftEw == ey.competitorId) {
                        standingLeft = ey.won + '-' + ey.lost;
                        teamImageLeft = ey.images.logo.T1.url;
                    }
                    if (RightEw == ey.competitorId) {
                        standingRight = ey.won + '-' + ey.lost;
                        teamImageRight = ey.images.logo.T1.url;
                    }
                });

                let status;
                let timeOrNah;

                let pointsLeft;
                let pointsRight;

                if (this.state.dataSource[0].completionStatus == 'COMPLETE' || this.state.dataSource[0].completionStatus == 'FINISHED') {
                    status = 'FINAL';
                    timeOrNah = moment(this.state.matchtime).format('MM/DD/YY');
                    pointsLeft = this.state.dataSource[0].scoreString;
                    pointsRight = this.state.dataSource[1].scoreString;
                }

                if (this.state.dataSource[0].completionStatus == 'SCHEDULED' || this.state.dataSource[0].completionStatus == 'NOT_COMPLETE') {
                    status = '8:30 PM';
                    timeOrNah = moment(this.state.matchtime).format('MM/DD/YY');
                    pointsLeft = '-';
                    pointsRight = '-';
                }

                let videoHa;
                let op;
                let head;

                if (this.state.matchstatus == "COMPLETE") {
                    this.getVideo();
                    videoHa = <TouchableOpacity onPress={this._openVideo.bind(this)}>
                        <View style={{ width: 100 + '%', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', backgroundColor: '#000000' }}>
                            <Image style={{ width: 100 + '%', height: 250 }}
                                source={{ uri: 'http://aseanbasketballleague.com/wp-content/uploads/ablapp-recap/' + this.state.matchid + '.jpg' }}
                            />
                            <Image style={{ width: 64, height: 64, position: 'absolute' }}
                                source={require('./src/play-rounded-button.png')}
                            />
                        </View>
                    </TouchableOpacity>
                    // op = <View><Text>complete</Text></View>;
                    op = <View style={styles.group}>
                        <View style={styles.rowInfoTabs}>
                            <View style={{ width: 33 + '%', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', paddingRight: 5, borderRightWidth: 1, borderRightColor: '#e3e3e3' }}>
                                <TouchableOpacity onPress={() => this.segmentClicked(0)} active={this.state.activeIndex == 0}>
                                    <Text style={{ fontWeight: '700', paddingBottom: 10, paddingTop: 10, color: '#1f1f1f', }}>MATCH UP</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ width: 33 + '%', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', paddingLeft: 5 }}>
                                <TouchableOpacity onPress={() => this.segmentClicked(1)} active={this.state.activeIndex == 1}>
                                    <Text style={styles.recordsLabel}>BOX SCORE</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ width: 33 + '%', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', paddingLeft: 5, borderLeftWidth: 1, borderLeftColor: '#e3e3e3' }}>
                                <TouchableOpacity onPress={() => this.segmentClicked(2)} active={this.state.activeIndex == 2}>
                                    <Text style={styles.recordsLabel}>PLAY BY PLAY</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        {this.renderSection()}
                    </View>;
                    head = <View></View>;
                }
                if (this.state.matchstatus == "SCHEDULED" || this.state.matchstatus == "NOT_COMPLETE") {
                    this.getVideo();
                    videoHa = <TouchableOpacity onPress={this._openVideo.bind(this)}>
                        <View style={{ width: 100 + '%', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', backgroundColor: '#000000' }}>
                            <Image style={{ width: 100 + '%', height: 250 }}
                                source={{ uri: 'http://aseanbasketballleague.com/wp-content/uploads/ablapp-preview/' + this.state.matchid + '.jpg' }}
                            />
                            <Image style={{ width: 64, height: 64, position: 'absolute' }}
                                source={require('./src/play-rounded-button.png')}
                            />
                        </View>
                    </TouchableOpacity>;

                    if (this.state.seasonleaderpointsleftInfo.length > 0 && this.state.seasonleaderpointsrightInfo.length > 0) {

                        let lastNamePointsLeftseasonLeaders = this.state.seasonleaderpointsleftInfo[0].familyName.toLowerCase();

                        let leftPointsLeaderLastNameseasonLeaders = lastNamePointsLeftseasonLeaders.charAt(0).toUpperCase() + lastNamePointsLeftseasonLeaders.slice(1);

                        let lastNamePointsRightseasonLeaders = this.state.seasonleaderpointsrightInfo[0].familyName.toLowerCase();
                       
                        let rightPointsLeaderLastNameseasonLeaders = lastNamePointsRightseasonLeaders.charAt(0).toUpperCase() + lastNamePointsRightseasonLeaders.slice(1);
                       

                        let lastNameReboundsLeftseasonLeaders = this.state.seasonleaderreboundsleftInfo[0].familyName.toLowerCase();
                      
                        let leftReboundsLeaderLastNameseasonLeaders = lastNameReboundsLeftseasonLeaders.charAt(0).toUpperCase() + lastNameReboundsLeftseasonLeaders.slice(1);

                        let lastNameReboundsRightseasonLeaders = this.state.seasonleaderreboundsrightInfo[0].familyName.toLowerCase();
                       
                        let rightReboundsLeaderLastNameseasonLeaders = lastNameReboundsRightseasonLeaders.charAt(0).toUpperCase() + lastNameReboundsRightseasonLeaders.slice(1);

                        let lastNameAssistsLeftseasonLeaders = this.state.seasonleaderassistsleftInfo[0].familyName.toLowerCase();
                        
                        let leftAssistsLeaderLastNameseasonLeaders = lastNameAssistsLeftseasonLeaders.charAt(0).toUpperCase() + lastNameAssistsLeftseasonLeaders.slice(1);

                        let lastNameAssistsRightseasonLeaders = this.state.seasonleaderassistsrightInfo[0].familyName.toLowerCase();
                       
                        let rightAssistsLeaderLastNameseasonLeaders = lastNameAssistsRightseasonLeaders.charAt(0).toUpperCase() + lastNameAssistsRightseasonLeaders.slice(1);

                        op = <View style={styles.group}>
                            <View style={styles.rowLabel}>
                                <Text style={styles.label}>SEASON LEADERS</Text>
                            </View>
                            <View style={styles.rowInfo}>
                                <View style={{ width: 50 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                    <Text style={styles.infoLabelScoresLabelComparisonLabel}>{teamCodeLeft}</Text>
                                </View>
                                <View style={{ width: 50 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                                    <Text style={styles.infoLabelScoresLabelComparisonLabel}>{teamCodeRight}</Text>
                                </View>
                            </View>
                            <View style={styles.rowInfoComparisonBar}>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                    <Image
                                        style={styles.playerPic}
                                        source={{
                                            uri: this.state.seasonleaderpointsleftInfo[0].images.photo.S1.url
                                        }}
                                    />
                                    <Text style={styles.comparisonValueLeft}>{this.state.seasonleaderpointsleftInfo[0].sPointsAverage}</Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={styles.comparisonLabel}>POINTS</Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                                    <Text style={styles.comparisonValueRight}>{this.state.seasonleaderpointsrightInfo[0].sPointsAverage}</Text>
                                    <Image
                                        style={styles.playerPic}
                                        source={{
                                            uri: this.state.seasonleaderpointsrightInfo[0].images.photo.S1.url
                                        }}
                                    />
                                </View>
                            </View>
                            <View style={styles.rowInfoComparisonName}>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                    <Text style={styles.comparisonValueNameLeft}>{this.state.seasonleaderpointsleftInfo[0].firstName.charAt(0)}. {leftPointsLeaderLastNameseasonLeaders}</Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={styles.comparisonLabel}></Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                                    <Text style={styles.comparisonValueNameRight}>{this.state.seasonleaderpointsrightInfo[0].firstName.charAt(0)}. {rightPointsLeaderLastNameseasonLeaders}</Text>
                                </View>
                            </View>

                            <View style={styles.rowInfoComparisonBar}>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                    <Image
                                        style={styles.playerPic}
                                        source={{
                                            uri: this.state.seasonleaderreboundsleftInfo[0].images.photo.S1.url
                                        }}
                                    />
                                    <Text style={styles.comparisonValueLeft}>{this.state.seasonleaderreboundsleftInfo[0].sReboundsTotalAverage}</Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={styles.comparisonLabel}>REBOUNDS</Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                                    <Text style={styles.comparisonValueRight}>{this.state.seasonleaderreboundsrightInfo[0].sReboundsTotalAverage}</Text>
                                    <Image
                                        style={styles.playerPic}
                                        source={{
                                            uri: this.state.seasonleaderreboundsrightInfo[0].images.photo.S1.url
                                        }}
                                    />
                                </View>
                            </View>
                            <View style={styles.rowInfoComparisonName}>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                    <Text style={styles.comparisonValueNameLeft}>{this.state.seasonleaderreboundsleftInfo[0].firstName.charAt(0)}. {leftReboundsLeaderLastNameseasonLeaders}</Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={styles.comparisonLabel}></Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                                    <Text style={styles.comparisonValueNameRight}>{this.state.seasonleaderreboundsleftInfo[0].firstName.charAt(0)}. {rightReboundsLeaderLastNameseasonLeaders}</Text>
                                </View>
                            </View>

                            <View style={styles.rowInfoComparisonBar}>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                    <Image
                                        style={styles.playerPic}
                                        source={{
                                            uri: this.state.seasonleaderassistsleftInfo[0].images.photo.S1.url
                                        }}
                                    />
                                    <Text style={styles.comparisonValueLeft}>{this.state.seasonleaderassistsleftInfo[0].sAssistsAverage}</Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={styles.comparisonLabel}>ASSISTS</Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                                    <Text style={styles.comparisonValueRight}>{this.state.seasonleaderassistsrightInfo[0].sAssistsAverage}</Text>
                                    <Image
                                        style={styles.playerPic}
                                        source={{
                                            uri: this.state.seasonleaderassistsrightInfo[0].images.photo.S1.url
                                        }}
                                    />
                                </View>
                            </View>
                            <View style={styles.rowInfoComparisonName}>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                    <Text style={styles.comparisonValueNameLeft}>{this.state.seasonleaderassistsleftInfo[0].firstName.charAt(0)}. {leftAssistsLeaderLastNameseasonLeaders}</Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={styles.comparisonLabel}></Text>
                                </View>
                                <View style={{ width: 33 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                                    <Text style={styles.comparisonValueNameRight}>{this.state.seasonleaderassistsrightInfo[0].firstName.charAt(0)}. {rightAssistsLeaderLastNameseasonLeaders}</Text>
                                </View>
                            </View>
                        </View>;
                        head = <View style={{ paddingBottom: 100 }}>
                            {this.renderHeadtoHead()}
                        </View>;
                    } else {
                        op = <View style={{ paddingBottom: 500 }}></View>;
                        head = <View></View>;
                    }


                }
                if (this.state.matchstatus == "IN_PROGRESS") {
                    this.getVideo();
                    videoHa = <TouchableOpacity onPress={this._openVideo.bind(this)}>
                        <View style={{ width: 100 + '%', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', backgroundColor: '#000000' }}>
                            <Image style={{ width: 100 + '%', height: 250 }}
                                source={require('./src/if-youtube-live.jpg')}
                            />
                        </View>
                    </TouchableOpacity>;
                    op = <View></View>;
                    head = <View></View>;
                }

                return (
                    <View style={styles.container}>
                        <ScrollView>
                            <View style={[styles.row, { flexDirection: 'row', justifyContent: 'center', backgroundColor: '#ffffff' }]}>
                                <View style={{ width: 15 + '%', backgroundColor: 'white', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', }}>
                                    <Image
                                        style={styles.teamPic}
                                        source={{
                                            uri: teamImageLeft
                                        }}
                                    />
                                </View>
                                <View style={{ width: 18 + '%', backgroundColor: 'white', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', }}>
                                    <Text style={styles.teamCode}>{teamCodeLeft}</Text>
                                    <Text style={styles.teamStanding}>{standingLeft}</Text>
                                </View>
                                <View style={{ width: 12 + '%', backgroundColor: 'white', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', }}>
                                    <Text style={styles.teamScore}>{pointsLeft}</Text>
                                </View>
                                <View style={{ width: 18 + '%', backgroundColor: 'white  ', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', }}>
                                    <Text style={styles.teamTime}>{status}</Text>
                                    <Text style={styles.teamStanding}>{timeOrNah}</Text>
                                    {/* <Text style={ styles.teamStanding }>OT1: 10:00</Text> */}
                                </View>
                                <View style={{ width: 12 + '%', backgroundColor: 'white', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', }}>
                                    <Text style={styles.teamScore}>{pointsRight}</Text>
                                </View>
                                <View style={{ width: 18 + '%', backgroundColor: 'white', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', }}>
                                    <Text style={styles.teamCode}>{teamCodeRight}</Text>
                                    <Text style={styles.teamStanding}>{standingRight}</Text>
                                </View>
                                <View style={{ width: 15 + '%', backgroundColor: 'white ', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', }}>
                                    <Image
                                        style={styles.teamPic}
                                        source={{
                                            uri: teamImageRight
                                        }}
                                    />
                                </View>
                            </View>
                        </ScrollView>

                        <ScrollView>
                            <View style={{ width: 100 + '%', }}>
                                {videoHa}
                            </View>
                            {op}
                            {head}
                        </ScrollView>
                    </View>
                )
            }// end if match status complete or scheduled
        } // end else isloading

    }
}
const styles = StyleSheet.create({
    card: {
        borderBottomWidth: 1,
        borderBottomColor: '#1f1f1f',
    },
    date: {
        fontWeight: '700',
        paddingTop: 5,
        paddingBottom: 5,
        color: '#1f1f1f',
        paddingLeft: 15,
        paddingRight: 15,
    },
    final: {
        color: '#1f1f1f',
        fontWeight: '700',
        fontSize: 13,
    },
    errorTitle: {
        fontWeight: '700',
        fontSize: 20,
        textAlign: 'center',
        color: '#626671',
    },
    errorText: {
        textAlign: 'center',
        paddingBottom: 10,
        color: '#626671',
    },
    containerLoading: {
        flex: 1,
        backgroundColor: '#000000',
        justifyContent: 'center',
    },
    infoLabelScoresLabelPbpNoText: {
        color: '#000',
        flex: 1,
        flexWrap: 'wrap',
    },
    teamName: {
        color: '#1f1f1f',
        fontWeight: '700'
    },
    userPic: {
        height: 40,
        width: 40,
        marginRight: 10,
        borderRadius: 20,
    },
    infoLabelScoresLabelPbp: {
        color: '#000',
        fontWeight: '700',
    },
    infoLabelScoresLabelPbpNo: {
        color: '#000',
    },
    columnViewStatsImage: {
        flexDirection: 'column',
        paddingLeft: 15,
        paddingRight: 15,
    },
    recordsLabelStatsPlayerName: {
        color: '#ee3026',
        fontWeight: '700',
        textAlign: 'left',
        fontSize: 10,
    },
    recordsLabelStatsTeamName: {
        color: '#ee3026',
        fontWeight: '700',
        textAlign: 'right',
        fontSize: 15,
    },
    columnViewStatsPlayerName: {
        flexDirection: 'column',
        paddingLeft: 15,
        paddingRight: 15,
        width: 100,
        height: 60,
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    columnViewStatsTeamName: {
        flexDirection: 'column',
        paddingLeft: 15,
        paddingRight: 15,
        width: 100,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
    },
    columnViewStatsFix: {
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 20,
        paddingBottom: 10,
    },
    recordsLabelStatsFix: {
        color: '#1f1f1f',
        fontWeight: '700',
        textAlign: 'center',
        // width: 100 + '%',
        // paddingTop: 20,
        // paddingBottom: 20,
    },
    comparisonLabel: {
        fontSize: 12,
        color: '#999',
    },
    comparisonValue: {
        fontSize: 18,
        color: '#000000',
    },
    comparisonValueNameLeft: {
        fontSize: 12,
        color: '#000000',
    },
    comparisonValueNameRight: {
        fontSize: 12,
        color: '#000000',
    },
    comparisonValueLeft: {
        fontSize: 20,
        color: '#000000',
        fontWeight: '700',
        paddingLeft: 10,
    },
    comparisonValueRight: {
        fontSize: 20,
        color: '#000000',
        fontWeight: '700',
        paddingRight: 10,
    },
    teamTime: {
        fontSize: 15,
        fontWeight: '700',
        color: '#1f1f1f',
    },
    teamScore: {
        fontSize: 20,
        fontWeight: '700',
        color: '#1f1f1f',
    },
    teamStanding: {
        fontSize: 10,
        fontWeight: '700',
    },
    teamCode: {
        fontSize: 15,
        fontWeight: '700',
        color: '#1f1f1f',
    },
    rowView: {
        flexDirection: 'row',
        paddingLeft: 15,
        paddingRight: 15,
        paddingBottom: 10,
        paddingTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#e3e3e3',
    },
    columnView: {
        flexDirection: 'column',
    },
    columnViewStats: {
        flexDirection: 'column',
        width: 50,
    },
    columnViewStatsTeam: {
        flexDirection: 'column',
        paddingLeft: 15,
        paddingRight: 15,
        borderRightColor: '#e3e3e3',
        borderRightWidth: 1,
    },
    mainStatsValue: {
        fontSize: 22,
        fontWeight: '700',
        color: '#ee3026'
    },
    playerPic: {
        width: 40,
        height: 40,
        borderRadius: 20,

    },
    leadersScore: {
        fontSize: 18,
        color: '#1f1f1f'
    },
    subLabel: {
        fontSize: 15,
        fontWeight: '700',
        color: '#999'
    },
    recordsScore: {
        color: '#1f1f1f'
    },
    recordsScoreStats: {
        color: '#1f1f1f',
        paddingBottom: 10,
        paddingTop: 10,
        textAlign: 'center',
    },
    recordsScoreStatsCar: {
        color: '#1f1f1f',
        paddingBottom: 10,
        paddingTop: 10,
        textAlign: 'center',
        fontWeight: '700',
    },
    recordsScoreStatsDiv: {
        color: '#1f1f1f',
        paddingBottom: 10,
        paddingTop: 10,
        textAlign: 'center',
    },
    playerNameShit: {
        color: '#1f1f1f',
        textAlign: 'center',
    },
    recordsLabel: {
        color: '#1f1f1f',
        fontWeight: '700',
        paddingBottom: 10,
        paddingTop: 10,
    },
    recordsLabelMatch: {
        color: '#ee3026',
        fontWeight: '700',
        paddingBottom: 10,
        paddingTop: 10,
    },
    recordsLabelOne: {
        color: '#1f1f1f',
        fontWeight: '700',
        paddingBottom: 10,
        paddingTop: 10,
    },
    recordsLabelStats: {
        color: '#1f1f1f',
        fontWeight: '700',
        paddingBottom: 10,
        paddingTop: 10,
        textAlign: 'center',
    },
    recordsLabelStatsTeam: {
        color: '#1f1f1f',
        fontWeight: '700',
        textAlign: 'center',
        paddingBottom: 10,
        paddingTop: 10,
    },
    recordsLabelStatsTeamScoreOne: {
        color: '#1f1f1f',
        fontWeight: '700',
        textAlign: 'center',
        paddingTop: 7,

    },
    recordsLabelStatsTeamScoreTwo: {
        color: '#1f1f1f',
        fontWeight: '700',
        textAlign: 'center',

    },

    recordsLabelStatsDiv: {
        color: '#1f1f1f',
        fontWeight: '700',
        paddingBottom: 10,
        paddingTop: 10,
        textAlign: 'center',

    },
    recordsLabelStatsOne: {
        color: '#1f1f1f',
        fontWeight: '700',
        paddingBottom: 10,
        paddingTop: 10,
    },
    rowLabel: {
        flexDirection: 'row',
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 10,
    },
    rowLabelheadtohead: {
        flexDirection: 'row',
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 10,
        paddingBottom: 10,
    },
    rowInfo: {
        flexDirection: 'row',
        paddingLeft: 15,
        paddingRight: 15,
        paddingBottom: 10,
        paddingTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#e3e3e3'
    },
    rowInfoEnd: {
        flexDirection: 'row',
        paddingLeft: 15,
        paddingRight: 15,
        paddingBottom: 10,
        paddingTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#e3e3e3',
        textAlign: 'center',
        alignItems: 'center',
        textAlignVertical: "center",
        justifyContent: 'center',
    },
    rowInfoComparison: {
        flexDirection: 'row',
        paddingLeft: 15,
        paddingRight: 15,
        paddingBottom: 10,
        paddingTop: 10,
    },
    rowInfoComparisonName: {
        flexDirection: 'row',
        paddingLeft: 15,
        paddingRight: 15,
        paddingBottom: 10,
    },
    rowInfoComparisonBar: {
        flexDirection: 'row',
        paddingLeft: 15,
        paddingRight: 15,
        paddingBottom: 5,
        paddingTop: 10,
    },
    rowInfoTabs: {
        flexDirection: 'row',
        paddingLeft: 15,
        paddingRight: 15,
        paddingBottom: 5,
        paddingTop: 5,
        borderBottomWidth: 1,
        borderBottomColor: '#e3e3e3'
    },
    value: {
        color: '#000'
    },
    infoLabel: {
        color: '#000',
        fontWeight: '700'
    },
    infoLabelScores: {
        color: '#000',
        paddingBottom: 5,
    },
    infoLabelScoresLabel: {
        color: '#000',
        fontWeight: '700',
        paddingBottom: 5,
    },
    infoLabelScoresLabelComparisonLabel: {
        color: '#ee3026',
        fontWeight: '700',
        paddingBottom: 5,
    },
    group: {
        backgroundColor: "#fff",
        //   elevation:2,
        //   shadowOffset: { width: 2, height: 2 },
        //   shadowColor: "grey",
        //   shadowOpacity: 0.5,
        //   shadowRadius: 2,
        marginBottom: 10,
        width: 100 + '%',
        flex: 1,
    },
    groupheadtohead: {
        backgroundColor: "#fff",
        //   elevation:2,
        //   shadowOffset: { width: 2, height: 2 },
        //   shadowColor: "grey",
        //   shadowOpacity: 0.5,
        //   shadowRadius: 2,
        marginBottom: 10,
        width: 100 + '%',
    },
    labelQuartsz: {
        fontWeight: '700',
        color: '#1f1f1f',
        fontSize: 18,
        textAlign: 'center',
        textAlignVertical: "center",
    },
    label: {
        fontWeight: '700',
        color: '#1f1f1f',
        fontSize: 18
    },
    standing: {
        color: '#fff'
    },
    teamName: {
        color: '#ffffff',
        fontSize: 18,
        fontWeight: '700'
    },
    row: {
        flexDirection: 'row',
        padding: 15,
    },
    container: {
        backgroundColor: '#e3e3e3',
        flex: 1,
    },
    linearGradient: {
        backgroundColor: "transparent",
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0
    },
    teamPic: {
        width: 50,
        height: 50
    },
    scoreHeadtohead: {
        color: '#1f1f1f',
        fontSize: 18,
        fontWeight: '700',
    },
});