import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, ImageBackground, ScrollView, Image, StatusBar, Button, WebView, ActivityIndicator, TouchableOpacity, Linking, FlatList, RefreshControl } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import moment from 'moment';

export default class videos extends Component {

  static navigationOptions = ({ navigation }) => ({
    headerBackground: (
      <Image
        style={{ width: '100%', height: '100%' }}
        source={require('./src/bg-header.jpg')}
      />
    ),
    headerStyle: {
      backgroundColor: '#000000',
      ...Platform.select({
        ios: {

        },
        android: {
          // height: 80,
          // paddingTop: 20
        },
      }),
    },
    headerTintColor: '#fff',
    title: 'VIDEOS',
    headerTitleStyle: {
      fontWeight: 'bold',
      flex: 1,
      ...Platform.select({
        ios: {

        },
        android: {
          // marginTop: 200,

        },
      }),
    },
  });

  constructor(props) {
    super(props);
    this.state = {
      isRefreshing: false,
      isLoading: true,
      showErrorPage: false,
      dataSource: [],
    }
  }

  componentDidMount() {
    this.getData();
  }

  getData = async () => {

    let youtubekey = "AIzaSyBU7865vDB9p9APyxKtJC2bjrpf7XqiVbo";
    let youtubeChannel = "UCjFguEjaNj6L0xbRqEpFqYg";

    return fetch('https://www.googleapis.com/youtube/v3/search?key=' + youtubekey + '&channelId=' + youtubeChannel + '&part=snippet,id&order=date&maxResults=50')
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          isLoading: false,
          isRefreshing: false,
          dataSource: responseJson.items,
        })
      })
      .catch((error) => {
        console.log(error);
        this.setState({
          isLoading: false,
          isRefreshing: false,
          showErrorPage: true,
        });
      });
  }

  _open(item) {
    if (!this.state.opening) {
      this.props.navigation.push('Youtube', { video: item.id.videoId })
      this.setState({ opening: true })
      setTimeout(() => {
        this.setState({ opening: false })
      }, 1000)
    }
  }

  renderRow = ({ item, index }) => {

    var i;
    let newArray = [];
    for (i = 1; i < this.state.dataSource.length; i++) {
      newArray[i] = this.state.dataSource[i];
    }



    if (index === 0) {
      return <TouchableOpacity onPress={this._open.bind(this, item)}> 
        <View style={styles.row}>
          <View style={{ justifyContent: 'center', alignItems: 'center', }}>
            <Image style={{ width: 100 + '%', height: 200, backgroundColor: '#000000' }}
              source={{ uri: this.state.dataSource[0].snippet.thumbnails.high.url }}
            />
            <Image style={{ width: 52, height: 52, position: 'absolute' }}
              source={require('./src/play-rounded-button.png')}
            />
          </View>
          <View style={styles.title}>
            <Text style={styles.time}>{moment(this.state.dataSource[0].snippet.publishedAt).format('ll')}</Text>
            <Text style={styles.tit}>{this.state.dataSource[0].snippet.title}</Text>
          </View>
        </View>
      </TouchableOpacity>
    }
    return <TouchableOpacity onPress={this._open.bind(this, item)}>
      <View style={[styles.row, { flex: 1, flexDirection: 'row' }]}>
        <View style={{ width: 60 + '%', height: 120, padding: 15, }}>
          <Text style={styles.time}>{moment(item.snippet.publishedAt).format('ll')}</Text>
          <Text style={styles.titSub}>{item.snippet.title}</Text>
        </View>
        <View style={{ width: 40 + '%', height: 120, padding: 15, paddingTop: 20, justifyContent: 'center', alignItems: 'center', }}>
          <Image style={{ width: 100 + '%', height: 80, backgroundColor: '#000000' }}
            source={{ uri: item.snippet.thumbnails.medium.url }}
          />
          <Image style={{ width: 52, height: 52, position: 'absolute' }}
            source={require('./src/play-rounded-button.png')}
          />
        </View>
      </View>
    </TouchableOpacity>

  }

  _onRefresh = () => {
    this.setState({ isRefreshing: true, }, this.getData);
  }

  _onRefreshPage = () => {
    this.setState({ isRefreshing: true, showErrorPage: false }, this.getData);
  }

  render() {

    if (this.state.showErrorPage) {
      return (
        <View style={{ flex: 1, justifyContent: "center", alignItems: "center", paddingHorizontal: 25, backgroundColor: 'white' }}>
          <Image
            style={{ width: 80, height: 80, marginBottom: 15, }}
            source={require('./src/antenna.png')}
          />
          <Text style={styles.errorTitle}>No Internet Connection</Text>
          <Text style={styles.errorText}>An error has occurred. Try checking your network settings</Text>
          <Button
            onPress={this._onRefreshPage.bind(this)}
            title="Try Again"
            color="#CE1A19"
          />
        </View>
      )
    }

    const shadowStyle = {
      shadowOffset: { width: 10, height: 10, },
      shadowColor: 'black',
      shadowOpacity: 1.0,
    }

    if (this.state.isLoading) {
      return (
        <View style={styles.containerLoading}>
          <ActivityIndicator size="large" color="#fff" />
          <View>
          </View>
        </View>
      )
    } else {



      return (
        <View style={styles.container}>
          <FlatList
            style={styles.container}
            data={this.state.dataSource}
            renderItem={this.renderRow}
            keyExtractor={(item, index) => index.toString()}
            refreshControl={
              <RefreshControl
                refreshing={this.state.isRefreshing}
                onRefresh={this._onRefresh}
              />
            }
          />
        </View>
      )

    }// end else
  }// end render
}

const styles = StyleSheet.create({
  errorTitle: {
    fontWeight: '700',
    fontSize: 20,
    textAlign: 'center',
    color: '#626671',
  },
  errorText: {
    textAlign: 'center',
    paddingBottom: 10,
    color: '#626671',
  },
  item: {
    flex: 1
  },
  container: {
    flex: 1,
    backgroundColor: '#e3e3e3',
  },
  containerLoading: {
    flex: 1,
    backgroundColor: '#000000',
    justifyContent: 'center',
  },
  headerText: {
    ...Platform.select({
      ios: {
        paddingBottom: 15,
        paddingTop: 30,
      },
      android: {
        padding: 17,
        paddingTop: 30,
      },
    }),
    fontSize: 18,
    color: 'white',
    fontWeight: '700',
    textAlign: 'center',
  },
  header: {
    backgroundColor: '#1f1f1f',
    alignItems: 'center',
    justifyContent: 'center',
  },
  userPic: {
    height: 40,
    width: 40,
    borderRadius: 20,
    marginRight: 10,
  },
  row: {
    backgroundColor: '#fff',
    marginBottom: 7,
    elevation: 2,
    shadowOffset: { width: 2, height: 2 },
    shadowColor: "grey",
    shadowOpacity: 0.5,
    shadowRadius: 2,
  },
  author: {
    flexDirection: "row",
    alignItems: "center",
    padding: 15,
  },
  authorName: {
    ...Platform.select({
      ios: {
        fontWeight: '700',
      },
      android: {
        fontWeight: '700',
      },
    }),
    color: '#1f1f1f'
  },
  title: {
    flexDirection: 'column',
    flex: 1,
    flexWrap: 'wrap',
    padding: 15,
    backgroundColor: '#fff',
  },
  tit: {
    fontSize: 18,
    color: '#1f1f1f',
    fontWeight: '700'
  },
  titSub: {
    fontSize: 14,
    color: '#1f1f1f',
    fontWeight: '700'
  },
  time: {
    fontSize: 10,
    paddingTop: 5,
    color: '#999',
  }
});