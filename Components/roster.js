import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, ImageBackground, ScrollView, Image, StatusBar, Button, WebView, Dimensions, ActivityIndicator, TouchableOpacity, FlatList} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Flag from 'react-native-flags';

export default class roster extends Component{

  static navigationOptions = ({ navigation }) => ( {
    headerBackground: (
      <Image
        style={[StyleSheet.absoluteFillObject]}
        source={{ uri: 'http://aseanbasketballleague.com/wp-content/uploads/abl-app/bg-header-'+ navigation.state.params.teamid +'.jpg' }}
      />
    ),
    headerStyle: {
      backgroundColor: '#000000',
      ...Platform.select({
        ios: {     
        
        },
        android: {
          // height: 80,
          // paddingTop: 20
        },
      }),
    },
    headerTintColor: '#fff',
    title: navigation.state.params.teamnickname + ' ROSTER',
    headerTitleStyle: {
      fontWeight: 'bold',
      flex: 1,
      ...Platform.select({
        ios: {     
        
        },
        android: {
         // marginTop: 200,
        
        },
      }),
    }, 
  });

  constructor(props) {
    super(props)
    this.state = {
      isLoading: true,
      teamid: this.props.navigation.state.params.teamid,
      teamnickname: this.props.navigation.state.params.teamnickname,
      rosterInfo: [],
      opening: false,
    }; 
  }

  componentDidMount(){
    this.getData();
  }


  getData = async () => {

   let comid = 26702;

   var teamRoster = 'https://api.wh.geniussports.com/v1/basketball/competitions/'+ comid +'/teams/'+ this.state.teamid +'/persons?ak=eebd8ae256142ac3fd24bd2003d28782&isCurrent=1&fields=images,personId,primaryTeamId,familyName,firstName,defaultShirtNumber,defaultPlayingPosition&limit=100';

   return fetch(teamRoster).then((response) => response.json()).then((responseJson)  => {
        this.setState({
          isLoading: false,
          rosterInfo: responseJson.response.data,
        });
    }).catch((error) => {
        console.error(error);
    });
  }

  _open (item) {
    if (!this.state.opening) {
      this.props.navigation.push('PlayerInfo', {playerid: item.personId, teamid: item.primaryTeamId})
      this.setState({ opening: true })
      setTimeout(() => {
        this.setState({ opening: false })
      }, 1000)
    }
  }


  renderRow = ({item, index}) => { 

    let convertedLastNameFinal;
    let lastNameTest = item.familyName.toLowerCase();
    let convertedLastName = lastNameTest.charAt(0).toUpperCase() + lastNameTest.slice(1);
   
    let firstLetterFirstName = item.firstName.charAt(0);

    if(index === 0) {

      return  <View>
                <View style={ styles.rowLabel }>
                  <Text style={ styles.label }>{this.state.teamnickname}</Text>
                </View>
                <View style={ styles.rowInfo }>
                  <View style={{width: 50 + '%',flexDirection: 'row', justifyContent: 'flex-start'}}>
                    <Text style={ styles.recordsLabel }>Name</Text>
                  </View>
                  <View style={{width: 50 + '%',flexDirection: 'row', justifyContent: 'flex-end'}}>
                    <Text style={ styles.recordsLabel }>Jersey #</Text>
                  </View>
                </View>          
              <TouchableOpacity onPress={this._open.bind(this, item)}>
              <View style={ styles.group }>
                  <View style={ styles.rowInfo }>
                    <View style={ styles.author }>
                      <Image 
                        style={ styles.userPic }
                        source={{ 
                          uri: item.images.photo.S1.url }}
                      />
                      <Text style={ styles.rosterName }>{firstLetterFirstName}. {convertedLastName} | {item.defaultPlayingPosition}</Text> 
                    </View>
                    <View style={{width: 50 + '%',flexDirection: 'row', justifyContent: 'flex-end', alignItems: "center",}}>
                      <Text style={ styles.recordsScore }>{item.defaultShirtNumber}</Text>
                    </View>
                  </View>
              </View>
            </TouchableOpacity>
            </View>
    }


   
      return <TouchableOpacity onPress={this._open.bind(this, item)}>
              <View style={ styles.group }>
                  <View style={ styles.rowInfo }>
                    <View style={ styles.author }>
                      <Image 
                        style={ styles.userPic }
                        source={{ 
                          uri: item.images.photo.T1.url }}
                      />
                      <Text style={ styles.rosterName }>{firstLetterFirstName}. {convertedLastName} | {item.defaultPlayingPosition}</Text>            
                    </View>
                    <View style={{width: 50 + '%',flexDirection: 'row', justifyContent: 'flex-end', alignItems: "center",}}>
                      <Text style={ styles.recordsScore }>{item.defaultShirtNumber}</Text>
                    </View>
                  </View>
              </View>
            </TouchableOpacity>

  }
 
    render() {
      if(this.state.isLoading) {
        return (
          <View style={styles.containerLoading}>
            <ActivityIndicator size="large" color="#fff" />
          </View>
        )
      } else {

      return(
              <FlatList
                style={styles.container}
                data={this.state.rosterInfo}
                renderItem={this.renderRow}
                keyExtractor={(item, index) => index.toString()}
              />
      )
    }//end else
    }//end render
}
const styles = StyleSheet.create({
    containerLoading: {
      flex: 1,
      backgroundColor: '#000000',
      justifyContent: 'center',
    },
    rosterName: {
      color: '#000',
    },
    author: {
      flexDirection: "row", 
      alignItems: "center",
      width: 50 + '%',
    },
    userPic: {
      height: 40,
      width: 40,
      borderRadius: 20,
      marginRight: 10,
    },
    playerPic: {
      width: 40,
      height: 40,
      borderRadius: 20,
    },
    leadersScore: {
      fontSize: 18,
      color: '#1f1f1f'
    },
    subLabel: {
      fontSize: 15,
      fontWeight: '700',
      color: '#999'
    },
    recordsScore: {
      color: '#1f1f1f'
    },
    recordsLabel: {
      color: '#1f1f1f',
      fontWeight: '700'
    },
    rowLabel: {
      flexDirection: 'row',
      paddingLeft: 15,
      paddingRight: 15,
      paddingTop: 10,
      backgroundColor: 'white',
    },
    rowInfo: {
      flexDirection: 'row',
      paddingLeft: 15,
      paddingRight: 15,
      paddingBottom: 10,
      paddingTop: 10,
      borderBottomWidth: 1, 
      borderBottomColor: '#e3e3e3',
      backgroundColor: 'white',
    },
    value: {
      color: '#999'
    },
    infoLabel: {
      color: '#999',
      fontWeight: '700'
    },
    group: {
      backgroundColor: "#fff",
      elevation:2,
      shadowOffset: { width: 2, height: 2 },
      shadowColor: "grey",
      shadowOpacity: 0.5,
      shadowRadius: 2,
      marginBottom: 0,
    },
    label: {
      fontWeight: '700',
      color: '#1f1f1f',
      fontSize: 18
    },
    standing: {
      color: '#fff'
    },
    teamName: {
      color: '#ffffff',
      fontSize: 18,
      fontWeight: '700'
    },
    row: {
      flexDirection: 'row',
      padding: 15,
    },
    container: {
      flex: 1,
      backgroundColor: '#e3e3e3',
    },
    linearGradient: {
      backgroundColor: "transparent",
      position: "absolute",
      top: 0,
      bottom: 0,
      left: 0,
      right: 0
    },
    teamPic: {
      width: 80,
      height: 80
    }
});