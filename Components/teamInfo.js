import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, ImageBackground, ScrollView, Image, StatusBar, Button, WebView, Dimensions, ActivityIndicator, Linking, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Flag from 'react-native-flags';
import moment from 'moment';

export default class teaminfo extends Component {

  static navigationOptions = ({ navigation }) => ({
    headerBackground: (
      <Image
        style={[StyleSheet.absoluteFillObject]}
        source={{ uri: 'http://aseanbasketballleague.com/wp-content/uploads/abl-app/bg-header-' + navigation.state.params.teamid + '.jpg' }}
      />
    ),
    headerStyle: {
      backgroundColor: '#000000',
      ...Platform.select({
        ios: {

        },
        android: {
          // height: 80,
          // paddingTop: 20
        },
      }),
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
      textAlign: 'left',
      flex: 1,
      ...Platform.select({
        ios: {

        },
        android: {
          // marginTop: 200,

        },
      }),
    },
  });

  constructor(props) {
    super(props)

    this.state = {
      isLoading: true,
      teamid: this.props.navigation.state.params.teamid,
      clubid: this.props.navigation.state.params.clubid,
      standingInfo: [],
      teamDetails: [],
      clubInfo: [],
      pointsInfo: [],
      assistsInfo: [],
      reboundsInfo: [],
      scheduleInfo: [],
      opening: false,
      openingPoints: false,
      openingAssists: false,
      openingRebounds: false,
      openingSchedule: false,
      openingRoster: false,
    }
  }

  componentDidMount() {
    this.getData();
  }

  getData = async () => {

    let comid = 26702;

    let dateeh;
    let newdate;
    let timeOrNah;

    let d = new Date();
    d.setDate(d.getDate() - 5);

    timeOrNah = moment(d.toString()).format('YYYY-MM-DD');

    var teamSolo = 'https://api.wh.geniussports.com/v1/basketball/teams/' + this.state.teamid + '?ak=eebd8ae256142ac3fd24bd2003d28782&fields=images,teamId,teamName,address1,ticketURL';
    var clubSolo = 'https://api.wh.geniussports.com/v1/basketball/clubs/' + this.state.clubid + '?ak=eebd8ae256142ac3fd24bd2003d28782&fields=clubNickname,address1';
    var teamStandings = 'https://api.wh.geniussports.com/v1/basketball/competitions/' + comid + '/standings?ak=eebd8ae256142ac3fd24bd2003d28782&fields=competitorId,won,lost,homeWins,homeLosses,awayWins,awayLosses,last5,streak,teamNickname&limit=100';
    var teamSchedule = 'https://api.wh.geniussports.com/v1/basketball/competitions/' + comid + '/matcheslive?ak=eebd8ae256142ac3fd24bd2003d28782&fromDate=' + timeOrNah + '&teamId=' + this.state.teamid + '&fields=matchNumber,matchTime,competitors.teamId,matchStatus,competitors.scoreString,competitors.teamNickname,competitors.isHomeCompetitor,matchId&limit=100';
    var teamPointsLeader = 'https://api.wh.geniussports.com/v1/basketball/competitions/' + comid + '/leaders/sPointsAverage?ak=eebd8ae256142ac3fd24bd2003d28782&teamId=' + this.state.teamid + '&fields=images,familyName,firstName,sPointsAverage,personId,teamId&limit=1';
    var teamAssistsLeader = 'https://api.wh.geniussports.com/v1/basketball/competitions/' + comid + '/leaders/sAssistsAverage?ak=eebd8ae256142ac3fd24bd2003d28782&teamId=' + this.state.teamid + '&fields=images,familyName,firstName,sAssistsAverage,personId,teamId&limit=1';
    var teamReboundsLeader = 'https://api.wh.geniussports.com/v1/basketball/competitions/' + comid + '/leaders/sReboundsTotalAverage?ak=eebd8ae256142ac3fd24bd2003d28782&teamId=' + this.state.teamid + '&fields=images,familyName,firstName,sReboundsTotalAverage,personId,teamId&limit=1';

    return fetch(teamSolo).then((response) => response.json()).then((responseJson) => {
      this.setState({
        teamDetails: responseJson.response.data,
      });
    }).then(() => {
      return fetch(clubSolo).then((response) => response.json()).then((responseJson) => {
        this.setState({
          clubInfo: responseJson.response.data,
        });
      }).catch((error) => {
        console.error(error);
      });
    }).then(() => {
      return fetch(teamStandings).then((response) => response.json()).then((responseJson) => {
        this.setState({
          standingInfo: responseJson.response.data,
        });
      }).catch((error) => {
        console.error(error);
      });
    }).then(() => {
      return fetch(teamSchedule).then((response) => response.json()).then((responseJson) => {
        this.setState({
          scheduleInfo: responseJson.response.data,
        });
      }).catch((error) => {
        console.error(error);
      });
    }).then(() => {
      return fetch(teamPointsLeader).then((response) => response.json()).then((responseJson) => {
        this.setState({
          pointsInfo: responseJson.response.data,
        });
      }).catch((error) => {
        console.error(error);
      });
    }).then(() => {
      return fetch(teamAssistsLeader).then((response) => response.json()).then((responseJson) => {
        this.setState({
          assistsInfo: responseJson.response.data,
        });
      }).catch((error) => {
        console.error(error);
      });
    }).then(() => {
      return fetch(teamReboundsLeader).then((response) => response.json()).then((responseJson) => {
        this.setState({
          isLoading: false,
          reboundsInfo: responseJson.response.data,
        });
      }).catch((error) => {
        console.error(error);
      });
    }).catch((error) => {
      console.error(error);
    });
  }

  _open(val) {
    if (!this.state.opening) {
      this.props.navigation.push('GameInfo', {matchstatus: val.matchStatus, matchid: val.matchId,matchtime: val.matchTime,lefteam: val.competitors[0].teamId,rightteam: val.competitors[1].teamId})
      this.setState({ opening: true })
      setTimeout(() => {
        this.setState({ opening: false })
      }, 1000)
    }
  }

  _openPoints(val) {
    if (!this.state.openingPoints) {
      this.props.navigation.push('PlayerInfo', { playerid: val.personId, teamid: val.teamId })
      this.setState({ openingPoints: true })
      setTimeout(() => {
        this.setState({ openingPoints: false })
      }, 1000)
    }
  }

  _openAssists(val) {
    if (!this.state.openingAssists) {
      this.props.navigation.push('PlayerInfo', { playerid: val.personId, teamid: val.teamId })
      this.setState({ openingAssists: true })
      setTimeout(() => {
        this.setState({ openingAssists: false })
      }, 1000)
    }
  }

  _openRebounds(val) {
    if (!this.state.openingRebounds) {
      this.props.navigation.push('PlayerInfo', { playerid: val.personId, teamid: val.teamId })
      this.setState({ openingRebounds: true })
      setTimeout(() => {
        this.setState({ openingRebounds: false })
      }, 1000)
    }
  }

  _openSchedule(nickname) {
    if (!this.state.openingSchedule) {
      this.props.navigation.push('Schedule', { teamid: this.state.teamid, teamnickname: nickname.toUpperCase() })
      this.setState({ openingSchedule: true })
      setTimeout(() => {
        this.setState({ openingSchedule: false })
      }, 1000)
    }
  }

  _openRoster(nickname) {
    if (!this.state.openingRoster) {
      this.props.navigation.push('Roster', { teamid: this.state.teamid, teamnickname: nickname.toUpperCase() })
      this.setState({ openingRoster: true })
      setTimeout(() => {
        this.setState({ openingRoster: false })
      }, 1000)
    }
  }



  render() {

    const { navigate } = this.props.navigation

    if (this.state.isLoading) {
      return (
        <View style={styles.containerLoading}>
          <ActivityIndicator size="large" color="#fff" />
        </View>
      )
    } else {

      let stand;
      let home;
      let away;
      let lastFiveWin;
      let lastFiveLoss;
      let streak;
      let lastChar;

      let testTicket = this.state.teamDetails.ticketURL.split("-");

      let finalCountryCode = testTicket[1];
      let finalArena = testTicket[2];
      let finalHead = testTicket[3];
      let coverPhoto = testTicket[4];

      let testSocial = this.state.clubInfo.clubNickname.split("-");
      let testFb = this.state.clubInfo.address1.split("-");

      let finalFacebookIos = testFb[0];
      let finalFacebookAndroid = testSocial[0];
      let finalTwitter = testSocial[1];
      let finalInstagram = testFb[1];
      let nickname;

      let standings = this.state.standingInfo.map((ey, key) => {
        if (this.state.teamDetails.teamId == ey.competitorId) {
          stand = ey.won + '-' + ey.lost;
          home = ey.homeWins + '-' + ey.homeLosses;
          away = ey.awayWins + '-' + ey.awayLosses;
          lastFiveWin = ey.last5.split("W").length - 1;
          lastFiveLoss = ey.last5.split("L").length - 1;
          lastChar = ey.last5[ey.last5.length - 1];
          streak = lastChar + ey.streak;
          nickname = ey.teamNickname;
        }
      });

      let count = 0;
      let max = 5;

      let newSchedule = [];

      let teamGames = this.state.scheduleInfo.sort((a, b) => a.matchNumber - b.matchNumber).map((val, key) => {

        if (count < 5) {
          newSchedule[count] = val;
          count++;
        }
      });

      let teamScheduleNew = newSchedule.sort((a, b) => a.matchNumber - b.matchNumber).map((val, key) => {

        let timeOrResult;
        let gameDate = moment(val.matchTime).format('MM/DD');
        let scoreLeft;
        let scoreRight;
        let fighResult;
        let opponent;
        let homeAway;

        if (val.matchStatus == 'COMPLETE' || val.matchStatus == 'FINISHED') {
          if (val.competitors[0].teamId == this.state.teamid) {
            scoreLeft = val.competitors[0].scoreString;
            scoreRight = val.competitors[1].scoreString;
            let xLeft = Number(scoreLeft);
            let xRight = Number(scoreRight);
            if (xLeft > xRight) {
              fighResult = <Text style={{color: 'green'}}>W</Text>;
            } else {
              fighResult = <Text style={{color: 'red'}}>L</Text>;
            }
            timeOrResult = <Text style={styles.recordsScore}>{fighResult} {scoreLeft + '-' + scoreRight}</Text>;
          }
          if (val.competitors[1].teamId == this.state.teamid) {
            scoreLeft = val.competitors[1].scoreString;
            scoreRight = val.competitors[0].scoreString;
            let xLeft = Number(scoreLeft);
            let xRight = Number(scoreRight);
            if (xLeft > xRight) {
              fighResult = <Text style={{color: 'green'}}>W</Text>;
            } else {
              fighResult = <Text style={{color: 'red'}}>L</Text>;
            }
            timeOrResult = <Text style={styles.recordsScore}>{fighResult} {scoreLeft + '-' + scoreRight}</Text>;
          }
        }
        if (val.matchStatus == 'SCHEDULED') {
          timeOrResult = <Text style={styles.recordsScore}>{moment(val.matchTime).format('LT')}</Text>;
        }
        if (val.matchStatus == 'IN_PROGRESS') {
          timeOrResult = <Text style={styles.recordsScoreLive}>LIVE</Text>;
        }

        if (val.competitors[0].teamId == this.state.teamid) {
          opponent = val.competitors[1].teamNickname;
          if (val.competitors[0].isHomeCompetitor == 1) {
            homeAway = 'vs';
          } else {
            homeAway = '@';
          }
        } else {
          opponent = val.competitors[0].teamNickname;
          if (val.competitors[1].isHomeCompetitor == 1) {
            homeAway = 'vs';
          } else {
            homeAway = '@';
          }
        }
        if (key === newSchedule.length - 1) {

          return <TouchableOpacity onPress={this._open.bind(this, val)}>
            <View key={key} style={styles.rowInfoBox}>
              <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-start' }}>
                <Text style={styles.recordsLabel}>{gameDate}</Text>
              </View>
              <View style={{ width: 40 + '%', flexDirection: 'row', justifyContent: 'flex-start' }}>
                <Text style={styles.recordsScore}>{homeAway} {opponent}</Text>
              </View>
              <View style={{ width: 40 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                {timeOrResult}
                <Icon name="chevron-right" size={25} color="#999" />
              </View>
            </View>
          </TouchableOpacity>
        } else {
          return <TouchableOpacity onPress={this._open.bind(this, val)}>
            <View key={key} style={styles.rowInfo}>
              <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-start' }}>
                <Text style={styles.recordsLabel}>{gameDate}</Text>
              </View>
              <View style={{ width: 40 + '%', flexDirection: 'row', justifyContent: 'flex-start' }}>
                <Text style={styles.recordsScore}>{homeAway} {opponent}</Text>
              </View>
              <View style={{ width: 40 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                {timeOrResult}
                <Icon name="chevron-right" size={25} color="#999" />
              </View>
            </View>
          </TouchableOpacity>
        }

      });

      let pointsLead = this.state.pointsInfo.map((val, key) => {
        
        let convertedLastNameFinal;
        let lastNameTest = val.familyName.toLowerCase();

        let convertedLastName = lastNameTest.charAt(0).toUpperCase() + lastNameTest.slice(1);

        let firstLetterFirstName = val.firstName.charAt(0);
        let valueTest = val.sPointsAverage.toString()
        let convertedValue = valueTest.substring(0, 4);
        return <TouchableOpacity onPress={this._openPoints.bind(this, val)}>
          <View key={key} style={styles.rowInfo}>
            <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-start' }}>
              <Image
                style={styles.playerPic}
                source={{
                  uri: val.images.photo.T1.url
                }}
              />
            </View>
            <View style={{ width: 40 + '%', height: 40, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', }}>
              <Text style={styles.recordsScore}>{firstLetterFirstName}. {convertedLastName}</Text>
            </View>
            <View style={{ width: 40 + '%', height: 40, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', }}>
              <Text style={styles.leadersScore}>{convertedValue}</Text>
            </View>
          </View>
        </TouchableOpacity>
      });

      let assistsLead = this.state.assistsInfo.map((val, key) => {

        let convertedLastNameFinal;
        let lastNameTest = val.familyName.toLowerCase();

        let convertedLastName = lastNameTest.charAt(0).toUpperCase() + lastNameTest.slice(1);
   
        let firstLetterFirstName = val.firstName.charAt(0);
        let valueTest = val.sAssistsAverage.toString()
        let convertedValue = valueTest.substring(0, 4);

        return <TouchableOpacity onPress={this._openAssists.bind(this, val)}>
          <View key={key} style={styles.rowInfo}>
            <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-start' }}>
              <Image
                style={styles.playerPic}
                source={{
                  uri: val.images.photo.T1.url
                }}
              />
            </View>
            <View style={{ width: 40 + '%', height: 40, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', }}>
              <Text style={styles.recordsScore}>{firstLetterFirstName}. {convertedLastName}</Text>
            </View>
            <View style={{ width: 40 + '%', height: 40, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', }}>
              <Text style={styles.leadersScore}>{convertedValue}</Text>
            </View>
          </View>
        </TouchableOpacity>
      });

      let reboundsLead = this.state.reboundsInfo.map((val, key) => {

        let convertedLastNameFinal;
        let lastNameTest = val.familyName.toLowerCase();
       
        let convertedLastName = lastNameTest.charAt(0).toUpperCase() + lastNameTest.slice(1);

        let firstLetterFirstName = val.firstName.charAt(0);
        let valueTest = val.sReboundsTotalAverage.toString()
        let convertedValue = valueTest.substring(0, 4);

        if (key === this.state.reboundsInfo.length - 1) {
          return <TouchableOpacity onPress={this._openRebounds.bind(this, val)}>
            <View key={key} style={styles.rowInfoBox}>
              <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-start' }}>
                <Image
                  style={styles.playerPic}
                  source={{
                    uri: val.images.photo.T1.url
                  }}
                />
              </View>
              <View style={{ width: 40 + '%', height: 40, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', }}>
                <Text style={styles.recordsScore}>{firstLetterFirstName}. {convertedLastName}</Text>
              </View>
              <View style={{ width: 40 + '%', height: 40, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', }}>
                <Text style={styles.leadersScore}>{convertedValue}</Text>
              </View>
            </View>
          </TouchableOpacity>
        } else {
          return <TouchableOpacity onPress={this._openRebounds.bind(this, val)}>
            <View key={key} style={styles.rowInfo}>
              <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-start' }}>
                <Image
                  style={styles.playerPic}
                  source={{
                    uri: val.images.photo.T1.url
                  }}
                />
              </View>
              <View style={{ width: 40 + '%', height: 40, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', }}>
                <Text style={styles.recordsScore}>{firstLetterFirstName}. {convertedLastName}</Text>
              </View>
              <View style={{ width: 40 + '%', height: 40, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', }}>
                <Text style={styles.leadersScore}>{convertedValue}</Text>
              </View>
            </View>
          </TouchableOpacity>
        }


      });

      let fbLink;
      let instagramlink;

      if (Platform.OS === 'ios') {
        fbLink = <Icon onPress={() => Linking.openURL('fb://profile/' + finalFacebookIos)} name="facebook" size={25} color="#000" />
        instagramlink = <Icon onPress={() => Linking.openURL('instagram://user?username=' + finalInstagram)} name="instagram" size={25} color="#000" />
      } else {
        fbLink = <Icon onPress={() => Linking.openURL('http://www.facebook.com/n/?' + finalFacebookAndroid)} name="facebook" size={25} color="#000" />
        instagramlink = <Icon onPress={() => Linking.openURL('http://instagram.com/_u/' + finalInstagram)} name="instagram" size={25} color="#000" />
      }

      return (

        <ScrollView>
          <View style={styles.container}>
            <ImageBackground source={{ uri: coverPhoto }} style={{ width: '100%', height: 'auto', resizeMode: 'cover', backgroundColor: '#000000' }}>
              <View style={styles.row}>
                <View style={{ width: 50 + '%', height: 150, flexDirection: 'column', justifyContent: 'center', }}>
                  <Text style={styles.teamName}>{this.state.teamDetails.teamName.toUpperCase()}</Text>
                  <Text style={styles.standing}>{stand}</Text>
                </View>
                <View style={{ width: 50 + '%', height: 150, flexDirection: 'column', justifyContent: 'center', alignItems: 'flex-end' }}>
                  <Image
                    style={styles.teamPic}
                    source={{
                      uri: this.state.teamDetails.images.logo.S1.url
                    }}
                  />
                </View>
              </View>
            </ImageBackground>

            <View style={styles.group}>
              <View style={styles.rowLabel}>
                <Text style={styles.label}>INFO</Text>
              </View>
              <View style={styles.rowInfo}>
                <View style={{ width: 30 + '%', flexDirection: 'column', justifyContent: 'center', }}>
                  <Text style={styles.infoLabel}>Head Coach: </Text>
                </View>
                <View style={{ width: 70 + '%', flexDirection: 'column', justifyContent: 'center', }}>
                  <Text style={styles.value}> {finalHead}</Text>
                </View>
              </View>
              <View style={styles.rowInfo}>
                <View style={{ width: 30 + '%', flexDirection: 'column', justifyContent: 'center', }}>
                  <Text style={styles.infoLabel}>Arena: </Text>
                </View>
                <View style={{ width: 70 + '%', flexDirection: 'column', justifyContent: 'center', }}>
                  <Text style={styles.value}> {finalArena}</Text>
                </View>
              </View>
              <View style={styles.rowInfo}>
                <View style={{ width: 30 + '%', flexDirection: 'column', justifyContent: 'center', }}>
                  <Text style={styles.infoLabel}>Country: </Text>
                </View>
                <View style={{ width: 70 + '%', flexDirection: 'column', justifyContent: 'center' }}>
                  <Text style={[styles.value, { textAlignVertical: "center" }]}> <Flag code={finalCountryCode} size={16} type="flat" /> {this.state.teamDetails.address1}</Text>
                </View>
              </View>
              <View style={styles.rowInfoBox}>
                <View style={{ width: 33.3 + '%', flexDirection: 'row', justifyContent: 'center', }}>
                  {fbLink}
                </View>
                <View style={{ width: 33.3 + '%', flexDirection: 'row', justifyContent: 'center', }}>
                  <Icon onPress={() => Linking.openURL('https://twitter.com/' + finalTwitter)} name="twitter" size={25} color="#000" />
                </View>
                <View style={{ width: 33.3 + '%', flexDirection: 'row', justifyContent: 'center', }}>
                  {instagramlink}
                </View>
              </View>
            </View>
            {/* {team} */}
            <View style={{ flexDirection: 'row', paddingBottom: 20, paddingTop: 10 }}>
              <View style={{ width: 50 + '%', flexDirection: 'row', justifyContent: 'flex-end', paddingRight: 5 }}>
                <TouchableOpacity onPress={this._openSchedule.bind(this, nickname)}>
                  <Text style={{ backgroundColor: '#EE3026', color: '#fff', padding: 10, fontWeight: '700', borderRadius: 2 }}>FULL SCHEDULE</Text>
                </TouchableOpacity>
              </View>
              <View style={{ width: 50 + '%', flexDirection: 'row', justifyContent: 'flex-start', paddingLeft: 5 }}>
                <TouchableOpacity onPress={this._openRoster.bind(this, nickname)}>
                  <Text style={{ backgroundColor: '#EE3026', color: '#fff', padding: 10, fontWeight: '700', borderRadius: 2 }}>TEAM ROSTER</Text>
                </TouchableOpacity>
              </View>
            </View>

            <View style={styles.group}>
              <View style={styles.rowLabel}>
                <Text style={styles.label}>RECORDS</Text>
              </View>
              <View style={styles.rowInfo}>
                <View style={{ width: 50 + '%', flexDirection: 'row', justifyContent: 'space-between', paddingRight: 5, borderRightWidth: 1, borderRightColor: '#e3e3e3' }}>
                  <Text style={styles.recordsLabel}>Home: </Text>
                  <Text style={styles.recordsScore}>{home}</Text>
                </View>
                <View style={{ width: 50 + '%', flexDirection: 'row', justifyContent: 'space-between', paddingLeft: 5 }}>
                  <Text style={styles.recordsLabel}>Last5: </Text>
                  <Text style={styles.recordsScore}>{lastFiveWin}-{lastFiveLoss}</Text>
                </View>
              </View>
              <View style={styles.rowInfoBox}>
                <View style={{ width: 50 + '%', flexDirection: 'row', justifyContent: 'space-between', paddingRight: 5, borderRightWidth: 1, borderRightColor: '#e3e3e3' }}>
                  <Text style={styles.recordsLabel}>Away: </Text>
                  <Text style={styles.recordsScore}>{away}</Text>
                </View>
                <View style={{ width: 50 + '%', flexDirection: 'row', justifyContent: 'space-between', paddingLeft: 5 }}>
                  <Text style={styles.recordsLabel}>Streak: </Text>
                  <Text style={styles.recordsScore}>{streak}</Text>
                </View>
              </View>
            </View>
            <View style={styles.group}>
              <View style={styles.rowLabel}>
                <Text style={styles.label}>RECENT SCHEDULE</Text>
              </View>
              <View style={styles.rowInfo}>
                <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-start' }}>
                  <Text style={styles.recordsLabel}>Date</Text>
                </View>
                <View style={{ width: 40 + '%', flexDirection: 'row', justifyContent: 'flex-start' }}>
                  <Text style={styles.recordsLabel}>Game</Text>
                </View>
                <View style={{ width: 40 + '%', flexDirection: 'row', justifyContent: 'flex-end' }}>
                  <Text style={styles.recordsLabel}>Time/Result</Text>
                </View>
                {/* <View style={{width: 20 + '%',flexDirection: 'row', justifyContent: 'flex-end'}}>
               
              </View> */}
              </View>
              {teamScheduleNew}
            </View>
            <View style={styles.group}>
              <View style={styles.rowLabel}>
                <Text style={styles.label}>LEADERS</Text>
              </View>
              <View style={styles.rowLabel}>
                <Text style={styles.subLabel}>Points</Text>
              </View>
              <View style={styles.rowInfo}>
                <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-start' }}>
                  <Text style={styles.recordsLabel}>Player</Text>
                </View>
                <View style={{ width: 40 + '%', flexDirection: 'row', justifyContent: 'flex-start' }}>
                  <Text style={styles.recordsLabel}>Name</Text>
                </View>
                <View style={{ width: 40 + '%', flexDirection: 'row', justifyContent: 'flex-end' }}>
                  <Text style={styles.recordsLabel}>Points</Text>
                </View>
              </View>
              {pointsLead}
              <View style={styles.rowLabel}>
                <Text style={styles.subLabel}>Assists</Text>
              </View>
              <View style={styles.rowInfo}>
                <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-start' }}>
                  <Text style={styles.recordsLabel}>Player</Text>
                </View>
                <View style={{ width: 40 + '%', flexDirection: 'row', justifyContent: 'flex-start' }}>
                  <Text style={styles.recordsLabel}>Name</Text>
                </View>
                <View style={{ width: 40 + '%', flexDirection: 'row', justifyContent: 'flex-end' }}>
                  <Text style={styles.recordsLabel}>Assists</Text>
                </View>
              </View>
              {assistsLead}
              <View style={styles.rowLabel}>
                <Text style={styles.subLabel}>Rebounds</Text>
              </View>
              <View style={styles.rowInfo}>
                <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-start' }}>
                  <Text style={styles.recordsLabel}>Player</Text>
                </View>
                <View style={{ width: 40 + '%', flexDirection: 'row', justifyContent: 'flex-start' }}>
                  <Text style={styles.recordsLabel}>Name</Text>
                </View>
                <View style={{ width: 40 + '%', flexDirection: 'row', justifyContent: 'flex-end' }}>
                  <Text style={styles.recordsLabel}>Rebounds</Text>
                </View>
              </View>
              {reboundsLead}
            </View>
          </View>
        </ScrollView>

      )
    }// end else 
  } // end render
}
const styles = StyleSheet.create({
  containerLoading: {
    flex: 1,
    backgroundColor: '#000000',
    justifyContent: 'center',
  },
  playerPic: {
    width: 40,
    height: 40,
    borderRadius: 20,
  },
  leadersScore: {
    fontSize: 18,
    color: '#1f1f1f'
  },
  subLabel: {
    fontSize: 15,
    fontWeight: '700',
    color: '#999'
  },
  recordsScore: {
    color: '#1f1f1f',
    textTransform: 'capitalize'
  },
  recordsScoreLive: {
    color: '#EE3026'
  },
  recordsLabel: {
    color: '#1f1f1f',
    fontWeight: '700'
  },
  rowLabel: {
    flexDirection: 'row',
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 10,
  },
  rowInfo: {
    flexDirection: 'row',
    paddingLeft: 15,
    paddingRight: 15,
    paddingBottom: 10,
    paddingTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#e3e3e3'
  },
  rowInfoBox: {
    flexDirection: 'row',
    paddingLeft: 15,
    paddingRight: 15,
    paddingBottom: 10,
    paddingTop: 10,
  },
  value: {
    color: '#000'
  },
  infoLabel: {
    color: '#000',
    fontWeight: '700'
  },
  group: {
    backgroundColor: "#fff",
    elevation: 2,
    shadowOffset: { width: 2, height: 2 },
    shadowColor: "grey",
    shadowOpacity: 0.5,
    shadowRadius: 2,
    marginBottom: 10,
  },
  label: {
    fontWeight: '700',
    color: '#1f1f1f',
    fontSize: 18
  },
  standing: {
    color: '#fff'
  },
  teamName: {
    color: '#ffffff',
    fontSize: 18,
    fontWeight: '700'
  },
  row: {
    flexDirection: 'row',
    padding: 15,
  },
  container: {
    flex: 1,
    backgroundColor: '#e3e3e3',
  },
  linearGradient: {
    backgroundColor: "transparent",
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0
  },
  teamPic: {
    width: 80,
    height: 80
  }
});