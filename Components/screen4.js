import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,ImageBackground, ScrollView, Image, StatusBar, Button, ActivityIndicator,TouchableOpacity, FlatList, RefreshControl} from 'react-native';
import axios from 'axios';

export default class screen4 extends Component{

  static navigationOptions = ({ navigation }) => ( {
    headerBackground: (
      <Image
        style={{width: '100%', height: '100%'}}
        source={ require('./src/bg-header.jpg') }
      />
    ),
    headerStyle: {
      backgroundColor: 'transparent',
      ...Platform.select({
        ios: {     
        
        },
        android: {
          // height: 80,
          // paddingTop: 20
        },
      }),
    },
    headerTintColor: '#fff',
    title: 'STANDINGS',
    headerTitleStyle: {
      fontWeight: 'bold',
      fontSize: 18,
      textAlign: 'center',
      flex: 1,
      ...Platform.select({
        ios: {     
        
        },
        android: {
         // marginTop: 200,
        },
      }),
    }, 
  });

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      dataSource: [],
      opening: false,
      isRefreshing: false,
      showErrorPage: false,
    }
  }

  componentDidMount() {
    this.getData();
  }

  getData = async () => {

    let comid = 26702;

    var request_1_url = 'https://api.wh.geniussports.com/v1/basketball/competitions/'+ comid +'/standings?ak=eebd8ae256142ac3fd24bd2003d28782&fields=images,teamId,teamClubId,percentageWon,teamCode,last5,played,won,lost,teamNickname';

    fetch(request_1_url).then((response) => response.json())
    .then((responseJson) => {
        let teamStandings;
        function sortByKey(array, key) {
          return array.sort(function(a, b) {
              var x = a[key]; var y = b[key];
              return ((y < x) ? -1 : ((y > x) ? 1 : 0));
          });
        }

        teamStandings = sortByKey(responseJson.response.data, 'percentageWon');
        this.setState({
            isLoading: false,
            isRefreshing: false,
            dataSource: teamStandings,
        });
    })
    .catch((error) => {
      console.log(error);
      this.setState({
        isLoading: false,
        isRefreshing: false,
        showErrorPage: true,
      });
    });
  } 

  _open (item) {
    if (!this.state.opening) {
      this.props.navigation.push('TeamInfo', {teamid: item.teamId,clubid: item.teamClubId})
      this.setState({ opening: true })
      setTimeout(() => {
        this.setState({ opening: false })
      }, 1000)
    }
  }

  renderRow = ({item}) => {

     //function sort array api to team name
    //  function sortByKey(array, key) {
    //   return array.sort(function(a, b) {
    //       var x = a[key]; var y = b[key];
    //       return ((y < x) ? -1 : ((y > x) ? 1 : 0));
    //   });
    // }

    // teamStandings = sortByKey(this.state.dataSource, 'percentageWon');

    let country;
    let lFiveWin;
    let lFiveLost;

    if(item.teamCode == 'SGH') {
      country =  'Saigon'; 
    }
    if(item.teamCode == 'SGS') {
      country =  'Singapore'; 
    }
    if(item.teamCode == 'KLD') {
      country =  'Malaysia'; 
    }
    if(item.teamCode == 'MNV') {
      country =  'Thailand'; 
    }
    if(item.teamCode == 'FUB') {
      country =  'Chinese Taipei'; 
    }
    if(item.teamCode == 'FMD') {
      country =  'Chinese Taipei'; 
    }
    if(item.teamCode == 'HKE') {
      country =  'Hong Kong'; 
    }
    if(item.teamCode == 'SAP') {
      country =  'Philippines'; 
    }
    if(item.teamCode == 'MBB') {
      country =  'Macau'; 
    }
    if(item.teamCode == 'MWW') {
      country =  'Macau'; 
    }

    //function for specific character count
    function countOccurence(char) {
      var count = 0;
        for(var i=0; i<item.last5.length;i++) {
          if(item.last5[i] === char) {
            count++;
          }
        }
      return count;
    }

    lFiveWin = countOccurence('W');
    lFiveLost = countOccurence('L');

    //compute win percentage
    let winPercentage;

    if(item.played > 0) {
      if(item.won === 0) {
        winPercentage = '.000';
      } else {
        let totalHa = item.won/item.played;
        let totalHaTest = totalHa.toString().length;
          if(totalHaTest === 1) {
            winPercentage = '100';
          } else {
            let remove = totalHa.toString().substring(1, 5);
            let yeah = remove.length;
              if(yeah === 2) {
                winPercentage = remove + '00';
              }
              if(yeah === 3) {
                winPercentage = remove + '0';
              }
              if(yeah === 4) {
                winPercentage = remove;
              }
          }
      }
    }


    return <TouchableOpacity onPress={this._open.bind(this, item)}>
            <View style={ styles.row }>                                                                                                                               
              <View style={ styles.author }>
                <Image 
                  style={ styles.userPic }
                  source={{ 
                    uri: item.images.logo.S1.url }}
                />
                <View  style={{ width: 40 + '%', flexDirection: 'column', justifyContent: 'center' }}>
                  <Text style={ styles.country }>{country}</Text> 
                  <Text style={ styles.teamName }>{item.teamNickname}</Text>   
                </View> 
                <View  style={{  width: 25 + '%', paddingRight: 10, flexDirection: 'column', justifyContent: 'center', alignItems: 'flex-end',}}>
                  <Text style={ styles.country }>L5 {lFiveWin}-{lFiveLost}</Text> 
                  <Text style={ styles.teamName }>{item.won}-{item.lost}</Text>   
                </View>  
                <View  style={{  width: 20 + '%', borderLeftWidth: 1, borderLeftColor: '#e3e3e3' ,paddingLeft: 10, flexDirection: 'column', justifyContent: 'center', alignItems: 'flex-end', }}>
                  <Text style={ styles.country }>WIN%</Text> 
                  <Text style={ styles.teamName }>{winPercentage}</Text>   
                </View> 
              </View>
            </View>
          </TouchableOpacity>

  }

  _onRefresh = () => {
    this.setState({isRefreshing: true,}, this.getData);
  }

  _onRefreshPage = () => {
    this.setState({isRefreshing: true, showErrorPage: false}, this.getData);
  }

  render() {

    if(this.state.showErrorPage) {
      return (
        <View style={{ flex: 1, justifyContent: "center", alignItems: "center", paddingHorizontal: 25, backgroundColor: 'white' }}>
          <Image
            style={{width: 80, height: 80, marginBottom: 15,}}
            source={ require('./src/antenna.png') }
          />
          <Text style={styles.errorTitle}>No Internet Connection</Text>
          <Text style={styles.errorText}>An error has occurred. Try checking your network settings</Text>
          <Button
              onPress={this._onRefreshPage.bind(this)}
              title="Try Again"
              color="#CE1A19"
          />
        </View>
      )
    }

    if(this.state.isLoading) {
      return (
        <View style={styles.containerLoading}>
          <ActivityIndicator size="large" color="#fff" />
        </View>
      )
    } else { 

 
      return(
      <View style={styles.container}>
          <FlatList
            style={styles.container}
            data={this.state.dataSource}
            renderItem={this.renderRow}
            keyExtractor={(item, index) => index.toString()}
            refreshControl={
              <RefreshControl 
                refreshing={this.state.isRefreshing}
                onRefresh={this._onRefresh}
              />
            }
          />
        </View>
      )
    }// end else is loading
  }
  
}

const styles = StyleSheet.create({
  errorTitle: {
    fontWeight: '700',
    fontSize: 20,
    textAlign: 'center',
    color: '#626671',
  },
  errorText: {
    textAlign: 'center',
    paddingBottom: 10,
    color: '#626671',
  },
  containerLoading: {
    flex: 1,
    backgroundColor: '#000000',
    justifyContent: 'center',
  },
  container: {
    flex: 1,
    backgroundColor: '#e3e3e3',
  },
  headerText: {
    ...Platform.select({
      ios: {     
        paddingBottom: 15,
        paddingTop: 30,
      },
      android: {
        padding: 17,
        paddingTop: 30,
      },
    }),
    fontSize: 18,
    color: 'white',
    fontWeight: '700',
    textAlign: 'center', 
  },
  header:  {
    backgroundColor: '#1f1f1f',
    alignItems: 'center',
    justifyContent: 'center',
  },
  row: {
    padding: 15,
    backgroundColor: 'white',
    marginBottom: 5,
    elevation:2,
    shadowOffset: { width: 2, height: 2 },
    shadowColor: "grey",
    shadowOpacity: 0.5,
    shadowRadius: 2,
  },
  author: {
    flexDirection: "row", 
    alignItems: "center",
    justifyContent: 'flex-start',
  },
  userPic: {
    height: 40,
    width: 40,
    marginRight: 10,
  },
  teamName: {
    color: '#1f1f1f',
    fontWeight: '700',
    fontSize: 18
  },
  country: {
    fontSize: 12,
    color: '#999'
  }, 
  lastFive: {

  },
  wl: {

  }
});
