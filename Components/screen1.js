import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, StatusBar, ImageBackground, ScrollView, TouchableOpacity, ActivityIndicator, FlatList, Button, RefreshControl } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { createStackNavigator } from 'react-navigation'; // Version can be specified in package.json
import { Card, CardTitle, CardContent, CardAction, CardButton, CardImage } from 'react-native-cards';
import moment from 'moment';
// import { Button } from 'native-base';
import HTML from 'react-native-render-html';
import OneSignal from 'react-native-onesignal'; // Import package from node modules

export default class screen1 extends Component {

  static navigationOptions = ({ navigation }) => ({
    headerBackground: (
      <Image
        style={{ width: '100%', height: '100%' }}
        source={require('./src/bg-header.jpg')}
      />
    ),
    headerStyle: {
      backgroundColor: 'transparent',
      ...Platform.select({
        ios: {

        },
        android: {
          // height: 80,
          // paddingTop: 20
        },
      }),
    },
    headerTintColor: '#fff',
    title: 'LATEST',
    headerTitleStyle: {
      fontWeight: 'bold',
      fontSize: 18,
      textAlign: 'center',
      flex: 1,
      ...Platform.select({
        ios: {

        },
        android: {
          // marginTop: 200,
        },
      }),
    },
  });



  constructor(props) {

    super(props);

    // OneSignal.init("979cf3b7-e768-4445-8cd1-d7e730eec3ee");
    OneSignal.init("bad676d5-a814-4be3-bf20-5c931f643afe");
    OneSignal.promptLocation();
    OneSignal.setSubscription(true);
    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened.bind(this));
    OneSignal.addEventListener('ids', this.onIds);

    this.state = {
      isRefreshing: false,
      isLoading: true,
      data: [],
      page: 1,
      opening: false,
      dataMatch: [],
      show: false,
      showErrorPage: false,
      dataSourceLive: [],
    }
  }

  componentDidMount() {

    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('ids', this.onIds);
    this.getData();
    this.props.navigation.setParams({
      scrollToTop: this.goToTop.bind(this),
    });
  }

  onReceived(notification) {
    console.log("Notification received: ", notification);
  }

  onOpened(openResult) {
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);

    if (openResult.notification.payload.additionalData != null) {
      if (openResult.notification.payload.additionalData.blog != null) {
        let data = openResult.notification.payload.additionalData.blog;
        this.props.navigation.push('ScreenNews', { slug: data, });
      }
      if (openResult.notification.payload.additionalData.matchid != null) {

        let data = openResult.notification.payload.additionalData.matchid;
        var request_1_url = 'https://api.wh.geniussports.com/v1/basketball/matches/' + data + '?ak=eebd8ae256142ac3fd24bd2003d28782&fields=matchStatus,matchId,matchTime,competitors.competitorId';

        fetch(request_1_url).then((response) => response.json()).then((responseJson) => {
          let teamStandings;
          teamStandings = responseJson.response.data;
          this.props.navigation.push('GameInfo', { matchstatus: teamStandings.matchStatus, matchid: teamStandings.matchId, matchtime: teamStandings.matchTime, lefteam: teamStandings.competitors[0].competitorId, rightteam: teamStandings.competitors[1].competitorId })
        });
      }
      if (openResult.notification.payload.additionalData.video != null) {
        let videoUrl = openResult.notification.payload.additionalData.video;
        this.props.navigation.push('Youtube', { video: videoUrl });
      }
    }
  }


  onIds(device) {
    console.log('Device info: ', device);
  }

  getData = async () => {

    let comid = 26702;
    //https://aseanbasketballleague.com/wp-json/wp/v2/youtubeticket
    const url = 'http://aseanbasketballleague.com/wp-json/wp/v2/posts?_embed&per_page=10&page=' + this.state.page;
    const request_url_game = 'https://api.wh.geniussports.com/v1/basketball/competitions/' + comid + '/matcheslive?ak=eebd8ae256142ac3fd24bd2003d28782&fields=matchStatus,competitors.images,competitors.teamNickname,matchId,matchTime,competitors.competitorId&limit=150';

    await fetch(url).then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          isLoading: false,
          isRefreshing: false,
          show: true,
          showErrorPage: false,
          data: this.state.data.concat(responseJson),
        })
      }).then(() => {
        return fetch(request_url_game).then((response) => response.json()).then((responseJson) => {
          this.setState({
            isLoading: false,
            dataSourceLive: responseJson.response.data,
          });
        }).catch((error) => {
          console.error(error);
          this.setState({
            isLoading: false,
            isRefreshing: false,
            showErrorPage: true,
          });
        });
      })
      .catch((error) => {
        console.log(error);
        this.setState({
          isLoading: false,
          isRefreshing: false,
          showErrorPage: true,
        });
      });
  }

  renderRecommended = (item, index) => {
    return (
      <View>
        <FlatList
          //horizontal
          pagingEnabled
          scrollEnabled
          showsHorizontalScrollIndicator={false}
          scrollEventThrottle={16}
          snapToAlignment="center"
          style={{ overflow: 'visible' }}
          data={this.state.dataSourceLive}
          keyExtractor={(item, index) => `${item.id}`}
          renderItem={({ item, index }) => this.renderRecommendation(item, index)}
        />
      </View>
    )
  }

  renderRecommendation = (item, index) => {

    if (item.matchStatus == 'IN_PROGRESS') {
      return (
        <View style={{ paddingHorizontal: 25, width: 100 + '%', backgroundColor: '#1f1f1f', marginBottom: 5, marginTop: 5, }}>
          <TouchableOpacity onPress={this._openLive.bind(this, item)}>
            <View style={{ flexDirection: 'row', }}>
              <View style={{ width: 50 + '%', backgroundColor: '#1f1f1f', }}>
                <View style={styles.authorLive}>
                  <Image
                    style={styles.userPicLive}
                    source={{
                      uri: item.competitors[0].images.logo.S1.url
                    }}
                  />
                  <Text style={styles.teamNameLive}>{item.competitors[0].teamNickname.toUpperCase()}</Text>
                </View>
                <View style={styles.authorLive}>
                  <Image
                    style={styles.userPicLive}
                    source={{
                      uri: item.competitors[1].images.logo.S1.url
                    }}
                  />
                  <Text style={styles.teamNameLive}>{item.competitors[1].teamNickname.toUpperCase()}</Text>
                </View>
              </View>
              <View style={{ width: 50 + '%', backgroundColor: '#1f1f1f', alignItems: 'flex-start', justifyContent: 'center', }}>
                <Text style={styles.watchLive}>WATCH LIVE</Text>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      )
    } else {
      <View></View>
    }
  }

  _open(item) {
    if (!this.state.opening) {
      this.props.navigation.push('ScreenNews', { slug: item.slug })
      this.setState({ opening: true })
      setTimeout(() => {
        this.setState({ opening: false })
      }, 1000)
    }
  }

  _openLive(item) {
    if (!this.state.opening) {
      this.props.navigation.push('GameInfo', { matchstatus: item.matchStatus, matchid: item.matchId, matchtime: item.matchTime, lefteam: item.competitors[0].competitorId, rightteam: item.competitors[1].competitorId })
      this.setState({ opening: true })
      setTimeout(() => {
        this.setState({ opening: false })
      }, 1000)
    }
  }

  renderRow = ({ item, index }) => {

    let dateFinal;

    let dateTo = moment().format('YYYY-MM-DD');
    let dateFrom = moment().subtract(6, 'd').format('YYYY-MM-DD');
    let yesterday = moment().add(-1, 'days');

    dateTest = moment(item.date).startOf('hour').fromNow();

    let blogDate = moment(item.date).format('YYYY-MM-DD');
    if (blogDate > dateFrom) {
      // date is future
      if (dateTest == "a day ago") {
        dateFinal = "1 day ago";
      } else {
        dateFinal = moment(item.date).startOf('hour').fromNow();
      }

    } else {
      // date is past
      if (dateFrom > blogDate) {
        dateFinal = moment(item.date).format('D MMMM');
      }
    }

    let image;
    typeof large //"undefined"

    image = item._embedded['wp:featuredmedia'][0].source_url;

    const htmlContent = '<font style="fontWeight: 700;color: black;fontSize: 18">' + item.title.rendered + '</font>';

    return (
      <TouchableOpacity onPress={this._open.bind(this, item)}>
        <View style={styles.row}>
          <View style={{ width: 100 + '%', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', backgroundColor: '#000000' }}>
            <Image style={{ width: 100 + '%', height: 360, backgroundColor: '#000000' }}
              source={{ uri: image }}
            />
          </View>
          <View style={styles.title}>
            <HTML html={htmlContent} />
            <Text style={styles.time}>{dateFinal}</Text>
          </View>
        </View>
      </TouchableOpacity>
    )
  }

  handleLoadMore = () => {
    this.setState(
      { page: this.state.page + 1 },
      this.getData,
    )
  }

  renderFooter = () => {
    return (
      <View style={styles.loader}>
        <ActivityIndicator animating={this.state.show} size="large" color="#000" />
      </View>
    )
  }

  goToTop = () => {
    this.flatListRef.scrollToOffset({ animated: true, offset: 0 });
  }

  _onRefresh = () => {
    this.setState({ isRefreshing: true, }, this.getData);
  }

  _onRefreshPage = () => {
    this.setState({ isRefreshing: true, showErrorPage: false }, this.getData);
  }

  render() {

    console.log(this.state.data, 'data');

    if (this.state.showErrorPage) {
      return (
        <View style={{ flex: 1, justifyContent: "center", alignItems: "center", paddingHorizontal: 25, backgroundColor: 'white' }}>
          <Image
            style={{ width: 80, height: 80, marginBottom: 15, }}
            source={require('./src/antenna.png')}
          />
          <Text style={styles.errorTitle}>No Internet Connection</Text>
          <Text style={styles.errorText}>An error has occurred. Try checking your network settings</Text>
          <Button
            onPress={this._onRefreshPage.bind(this)}
            title="Try Again"
            color="#CE1A19"
          />
        </View>
      )
    }

    const { navigate } = this.props.navigation

    const shadowStyle = {
      shadowOffset: { width: 10, height: 10, },
      shadowColor: 'black',
      shadowOpacity: 1.0,
    }

    if (this.state.isLoading) {
      return (
        <View style={styles.containerLoading}>
          <ActivityIndicator size="large" color="#fff" />
          <View>
          </View>
        </View>
      )
    } else {


      let browser;
      let wow;
      if (Platform.OS === 'ios') {
        browser = <StatusBar backgroundColor={'rgba(34, 34, 34, 0.80)'} translucent hidden={false} barStyle="light-content" />;
      } else {
        browser = <View></View>;
      }

      return (

        <View style={styles.container}>
          {browser}
          {this.renderRecommended()}
          <FlatList
            style={styles.container}
            data={this.state.data}
            renderItem={this.renderRow}
            keyExtractor={(item, index) => index.toString()}
            onEndReached={this.handleLoadMore}
            onEndReachedThreshold={1}
            ListFooterComponent={this.renderFooter}
            ref={(ref) => { this.flatListRef = ref; }}
            refreshControl={
              <RefreshControl
                refreshing={this.state.isRefreshing}
                onRefresh={this._onRefresh}
              />
            }
          />
        </View>
      )
    }

  }

}


const styles = StyleSheet.create({
  teamNameLive: {
    color: '#fff',
    fontWeight: '700',

  },
  watchLive: {
    borderLeftWidth: 1,
    borderLeftColor: '#666',
    color: 'red',
    fontWeight: '700',
    paddingVertical: 10,
    textAlign: 'right',
    // backgroundColor: 'blue',
    width: 100 + '%',
  },
  userPicLive: {
    height: 40,
    width: 40,
    marginRight: 10,
  },
  authorLive: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: 'flex-start',
    paddingTop: 1,
    paddingBottom: 1,
  },
  errorTitle: {
    fontWeight: '700',
    fontSize: 20,
    textAlign: 'center',
    color: '#626671',
  },
  errorText: {
    textAlign: 'center',
    paddingBottom: 10,
    color: '#626671',
  },
  loader: {
    // marginTop: 10,
    alignItems: 'center',
  },
  item: {
    flex: 1
  },
  container: {
    flex: 1,
    backgroundColor: '#e3e3e3',
  },
  containerLoading: {
    flex: 1,
    backgroundColor: '#000000',
    justifyContent: 'center',
  },
  headerText: {
    ...Platform.select({
      ios: {
        paddingBottom: 15,
        paddingTop: 30,
      },
      android: {
        padding: 17,
        paddingTop: 30,
      },
    }),
    fontSize: 18,
    color: 'white',
    fontWeight: '700',
    textAlign: 'center',
  },
  header: {
    backgroundColor: '#1f1f1f',
    alignItems: 'center',
    justifyContent: 'center',
  },
  userPic: {
    height: 40,
    width: 40,
    borderRadius: 20,
    marginRight: 10,
  },
  row: {
    backgroundColor: '#fff',
    marginBottom: 7,
    elevation: 2,
    shadowOffset: { width: 2, height: 2 },
    shadowColor: "grey",
    shadowOpacity: 0.5,
    shadowRadius: 2,
  },
  rowLive: {
    backgroundColor: '#1f1f1f',
    marginBottom: 10,
    marginTop: 10,
    paddingHorizontal: 20,
    flexDirection: 'row',
  },

  author: {
    flexDirection: "row",
    alignItems: "center",
    padding: 15,
  },
  authorName: {
    ...Platform.select({
      ios: {
        fontWeight: '700',
      },
      android: {
        fontWeight: '700',
      },
    }),
    color: '#1f1f1f'
  },
  title: {
    flexDirection: 'column',
    flex: 1,
    flexWrap: 'wrap',
    padding: 15,
    backgroundColor: '#fff',
  },
  tit: {
    fontSize: 18,
    color: '#1f1f1f',
    fontWeight: '700'
  },
  time: {
    fontSize: 10,
    paddingTop: 5,
    color: '#999',
  }
});
