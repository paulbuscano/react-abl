import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, ImageBackground, ScrollView, Image, TouchableHighlight, StatusBar, TouchableOpacity, ActivityIndicator, FlatList, RefreshControl, Button} from 'react-native';


export default class screen3 extends Component{

  static navigationOptions = ({ navigation }) => ( {
    headerBackground: (
      <Image
        style={{width: '100%', height: '100%'}}
        source={ require('./src/bg-header.jpg') }
      />
    ),
    headerStyle: {
      backgroundColor: 'transparent',
      ...Platform.select({
        ios: {     
        
        },
        android: {
          // height: 80,
          // paddingTop: 20
        },
      }),
    },
    headerTintColor: '#fff',
    title: 'TEAMS',
    headerTitleStyle: {
      fontWeight: 'bold',
      fontSize: 18,
      textAlign: 'center',
      flex: 1,
      ...Platform.select({
        ios: {     
        
        },
        android: {
         // marginTop: 200,
        },
      }),
    },  
  });

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      dataSource: [],
      opening: false,
      isRefreshing: false,
      showErrorPage: false,
    }
  }

  componentDidMount() {
    this.getData();
  } 
  
  getData = async () => {

    let comid = 26702;

    var request_1_url = 'https://api.wh.geniussports.com/v1/basketball/competitions/'+ comid +'/teams?ak=eebd8ae256142ac3fd24bd2003d28782&fields=images,teamId,clubId,teamName&limit=20';

    fetch(request_1_url).then((response) => response.json())
    .then((responseJson) => {
        let people;
          function sortByKey(array, key) {
            return array.sort(function(a, b) {
                var x = a[key]; var y = b[key];
                return ((x < y) ? -1 : ((x > y) ? 1 : 0));
            });
        }
        people = sortByKey(responseJson.response.data, 'teamName');
        this.setState({
            isLoading: false,
            isRefreshing: false,
            dataSource: people,
        });    
    })
    .catch((error) => {
      console.log(error);
      this.setState({
        isLoading: false,
        isRefreshing: false,
        showErrorPage: true,
      });
    });
  }

  _open (item) {
    if (!this.state.opening) {
      this.props.navigation.push('TeamInfo', {teamid: item.teamId,clubid: item.clubId})
      this.setState({ opening: true })
      setTimeout(() => {
        this.setState({ opening: false })
      }, 1000)
    }
  }

  renderRow = ({item}) => {
  
      return <TouchableOpacity onPress={this._open.bind(this, item)}>
              <View style={ styles.row }>
                <View style={ styles.author }>
                  <Image 
                    style={ styles.userPic }
                    source={{ 
                      uri: item.images.logo.T1.url }}
                  />
                  <Text style={ styles.teamName }>{item.teamName}</Text>            
                </View>
              </View>
            </TouchableOpacity>
  }

  _onRefresh = () => {
    this.setState({isRefreshing: true,}, this.getData);
  }

  _onRefreshPage = () => {
    this.setState({isRefreshing: true, showErrorPage: false}, this.getData);
  }

  render() {

    if(this.state.showErrorPage) {
      return (
        <View style={{ flex: 1, justifyContent: "center", alignItems: "center", paddingHorizontal: 25, backgroundColor: 'white' }}>
          <Image
            style={{width: 80, height: 80, marginBottom: 15,}}
            source={ require('./src/antenna.png') }
          />
          <Text style={styles.errorTitle}>No Internet Connection</Text>
          <Text style={styles.errorText}>An error has occurred. Try checking your network settings</Text>
          <Button
              onPress={this._onRefreshPage.bind(this)}
              title="Try Again"
              color="#CE1A19"
          />
        </View>
      )
    }

    if(this.state.isLoading) {
      return (
        <View style={styles.containerLoading}>
          <ActivityIndicator size="large" color="#fff" />
        </View>
      )
    } else {   

  
      return(
        <View style={styles.container}>
          <FlatList
            style={styles.container}
            data={this.state.dataSource}
            renderItem={this.renderRow}
            keyExtractor={(item, index) => index.toString()}
            refreshControl={
              <RefreshControl 
                refreshing={this.state.isRefreshing}
                onRefresh={this._onRefresh}
              />
            }
          />
        </View>
      )
    }// end else is loading
  }
}

const styles = StyleSheet.create({
  errorTitle: {
    fontWeight: '700',
    fontSize: 20,
    textAlign: 'center',
    color: '#626671',
  },
  errorText: {
    textAlign: 'center',
    paddingBottom: 10,
    color: '#626671',
  },
  containerLoading: {
    flex: 1,
    backgroundColor: '#000000',
    justifyContent: 'center',
  },
  container: {
    flex: 1,
    backgroundColor: '#e3e3e3',
  },
  headerText: {
    ...Platform.select({
      ios: {            
        paddingBottom: 15,
        paddingTop: 30,
      },
      android: {
        padding: 17,
        paddingTop: 30,
      },
    }),
    fontSize: 18,
    color: 'white',
    fontWeight: '700',
    textAlign: 'center', 
  },
  header:  {
    backgroundColor: '#1f1f1f',
    alignItems: 'center',
    justifyContent: 'center',
  },
  row: {
    padding: 15,
    backgroundColor: 'white',
    borderBottomWidth: 1, 
    borderBottomColor: '#e3e3e3'
  },
  author: {
    flexDirection: "row", 
    alignItems: "center",
    justifyContent: 'flex-start',
  },
  userPic: {
    height: 40,
    width: 40,
    marginRight: 10,
  },
  teamName: {
    color: '#1f1f1f',
    fontWeight: '700'
  }

});
