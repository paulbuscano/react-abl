import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, ImageBackground, ScrollView, Image, StatusBar, Button, WebView, Dimensions, ActivityIndicator, TouchableOpacity, FlatList} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Flag from 'react-native-flags';
import moment from 'moment';

export default class schedule extends Component{

  static navigationOptions = ({ navigation }) => ( {
    headerBackground: (
      <Image
        style={[StyleSheet.absoluteFillObject]}
        source={{ uri: 'http://aseanbasketballleague.com/wp-content/uploads/abl-app/bg-header-'+ navigation.state.params.teamid +'.jpg' }}
      />
    ),
    headerStyle: {
      backgroundColor: '#000000',
      ...Platform.select({
        ios: {     
        
        },
        android: {
          // height: 80,
          // paddingTop: 20
        },
      }),
    },
    headerTintColor: '#fff',
    title: navigation.state.params.teamnickname + ' SCHEDULE',
    headerTitleStyle: {
      fontWeight: 'bold',
      flex: 1,
      ...Platform.select({
        ios: {     
        
        },
        android: {
         // marginTop: 200,
        },
      }),
    }, 
  });

  constructor(props) {
    super(props)
    this.state = {
      isLoading: true,
      teamid: this.props.navigation.state.params.teamid,
      teamnickname: this.props.navigation.state.params.teamnickname,
      teamInfo: [],
      opening: false,
    }; 
  }

  componentDidMount(){
    this.getData();
  }

  getData = async () => {

    let comid = 26702;

    var teamSchedule = 'https://api.wh.geniussports.com/v1/basketball/competitions/'+ comid +'/matcheslive?ak=eebd8ae256142ac3fd24bd2003d28782&fromDate=2019-11-16&teamId='+ this.state.teamid +'&fields=matchNumber,matchTime,competitors.teamId,competitors.scoreString,matchStatus,competitors.teamNickname,competitors.isHomeCompetitor,matchId,competitors.competitorId&limit=100';

   return fetch(teamSchedule).then((response) => response.json()).then((responseJson)  => {

        let people;
          function sortByKey(array, key) {
            return array.sort(function(a, b) {
                var x = a[key]; var y = b[key];
                return ((x < y) ? -1 : ((x > y) ? 1 : 0));
            });
        }
        people = sortByKey(responseJson.response.data, 'matchNumber');

        this.setState({
          isLoading: false,
          teamInfo: people,
        });
    }).catch((error) => {
        console.error(error);
    });
  }

  _open (item) {
    if (!this.state.opening) {
      this.props.navigation.push('GameInfo', {matchstatus: item.matchStatus, matchid: item.matchId,matchtime: item.matchTime,lefteam: item.competitors[0].competitorId,rightteam: item.competitors[1].competitorId})
      this.setState({ opening: true })
      setTimeout(() => {
        this.setState({ opening: false })
      }, 1000)
    }
  }

  renderRow = ({item, index}) => { 

    let timeOrResult;
    let gameDate = moment(item.matchTime).format('MM/DD');
    let scoreLeft;
    let scoreRight;
    let fighResult;
    let opponent;
    let homeAway;

    if(item.matchStatus == 'COMPLETE' || item.matchStatus == 'FINISHED') {
        if(item.competitors[0].teamId == this.state.teamid) {
            scoreLeft = item.competitors[0].scoreString;
            scoreRight = item.competitors[1].scoreString;
            let xLeft = Number(scoreLeft);
            let xRight = Number(scoreRight);
              if(xLeft > xRight) {
                fighResult = <Text style={{color: 'green'}}>W</Text>;
              } else {
                fighResult = <Text style={{color: 'red'}}>L</Text>;
              }
              timeOrResult = <Text style={styles.recordsScore}>{fighResult} {scoreLeft + '-' + scoreRight}</Text>;
        }
        if(item.competitors[1].teamId == this.state.teamid) {
          scoreLeft = item.competitors[1].scoreString;
          scoreRight = item.competitors[0].scoreString;
          let xLeft = Number(scoreLeft);
          let xRight = Number(scoreRight);
            if(xLeft > xRight) {
              fighResult = <Text style={{color: 'green'}}>W</Text>;
            } else {
              fighResult = <Text style={{color: 'red'}}>L</Text>;
            }
            timeOrResult = <Text style={styles.recordsScore}>{fighResult} {scoreLeft + '-' + scoreRight}</Text>;
        } 
    }
    if(item.matchStatus == 'SCHEDULED') {
      timeOrResult = <Text style={ styles.recordsScore }>{moment(item.matchTime).format('LT')}</Text>;
    }
    if(item.matchStatus == 'IN_PROGRESS') {
      timeOrResult = <Text style={ styles.recordsScoreLive }>LIVE</Text>;
    }

    if(item.competitors[0].teamId == this.state.teamid) {
       opponent = item.competitors[1].teamNickname;
        if(item.competitors[0].isHomeCompetitor == 1) {
          homeAway = 'vs';
        } else {
          homeAway = '@';
        }
    } else {
      opponent = item.competitors[0].teamNickname;
      if(item.competitors[1].isHomeCompetitor == 1) {
        homeAway = 'vs';
      } else {
        homeAway = '@';
      }
    }

    if(index === 0) {
      return  <View>
                <View style={ styles.rowLabel }>
                  <Text style={ styles.label }>REGULAR SEASON</Text>
                </View>

                  <View style={ styles.rowInfo }>
                    <View style={{width: 20 + '%',flexDirection: 'row', justifyContent: 'flex-start'}}>
                      <Text style={ styles.recordsLabel }>Date</Text>
                    </View>
                    <View style={{width: 40 + '%',flexDirection: 'row', justifyContent: 'flex-start'}}>
                      <Text style={ styles.recordsLabel }>Game</Text>
                    </View>
                    <View style={{width: 40 + '%',flexDirection: 'row', justifyContent: 'flex-end'}}>
                      <Text style={ styles.recordsLabel }>Time/Result</Text>
                    </View>
                  </View>
                  <TouchableOpacity onPress={this._open.bind(this, item)}>
                    <View style={ styles.group }>
                      <View style={ styles.rowInfo }>
                        <View style={{width: 20 + '%',flexDirection: 'row', justifyContent: 'flex-start'}}>
                          <Text style={ styles.recordsLabel }>{gameDate}</Text>
                        </View>
                        <View style={{width: 40 + '%',flexDirection: 'row', justifyContent: 'flex-start'}}>
                          <Text style={ styles.recordsScore }>{homeAway} {opponent}</Text>
                        </View>
                        <View style={{width: 40 + '%',flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center'}}>
                          {timeOrResult}
                          <Icon name="chevron-right" size={25} color="#999"/>
                        </View>
                      </View>
                    </View>
                  </TouchableOpacity>
              </View>
    }

      return <TouchableOpacity onPress={this._open.bind(this, item)}>
              <View style={ styles.group }>
                <View style={ styles.rowInfo }>
                  <View style={{width: 20 + '%',flexDirection: 'row', justifyContent: 'flex-start'}}>
                    <Text style={ styles.recordsLabel }>{gameDate}</Text>
                  </View>
                  <View style={{width: 40 + '%',flexDirection: 'row', justifyContent: 'flex-start'}}>
                    <Text style={ styles.recordsScore }>{homeAway} {opponent}</Text>
                  </View>
                  <View style={{width: 40 + '%',flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center'}}>
                    {timeOrResult}
                    <Icon name="chevron-right" size={25} color="#999"/>
                  </View>
                </View>
              </View>
            </TouchableOpacity>

  }
 
    render() {
      if(this.state.isLoading) {
        return (
          <View style={styles.containerLoading}>
            <ActivityIndicator size="large" color="#fff" />
          </View>
        )
      } else {

      return(
          <FlatList
          style={styles.container}
          data={this.state.teamInfo}
          renderItem={this.renderRow}
          keyExtractor={(item, index) => index.toString()}
        />
      )
    }// end else
    }// end render
}
const styles = StyleSheet.create({
    containerLoading: {
      flex: 1,
      backgroundColor: '#000000',
      justifyContent: 'center',
    },
    playerPic: {
      width: 40,
      height: 40,
      borderRadius: 20,
    },
    leadersScore: {
      fontSize: 18,
      color: '#1f1f1f'
    },
    subLabel: {
      fontSize: 15,
      fontWeight: '700',
      color: '#999'
    },
    recordsScore: {
      color: '#1f1f1f'
    },
    recordsScoreLive: {
      color: '#EE3026'
    },
    recordsLabel: {
      color: '#1f1f1f',
      fontWeight: '700'
    },
    rowLabel: {
      flexDirection: 'row',
      paddingLeft: 15,
      paddingRight: 15,
      paddingTop: 10,
      paddingBottom: 10,
      backgroundColor: 'white',
    },
    rowInfo: {
      flexDirection: 'row',
      paddingLeft: 15,
      paddingRight: 15,
      paddingBottom: 10,
      paddingTop: 10,
      borderBottomWidth: 1, 
      borderBottomColor: '#e3e3e3',
      backgroundColor: 'white',
    },
    value: {
      color: '#999'
    },
    infoLabel: {
      color: '#999',
      fontWeight: '700'
    },
    group: {
      backgroundColor: "#fff",
      elevation:2,
      shadowOffset: { width: 2, height: 2 },
      shadowColor: "grey",
      shadowOpacity: 0.5,
      shadowRadius: 2,
      marginBottom: 0,
    },
    label: {
      fontWeight: '700',
      color: '#1f1f1f',
      fontSize: 18
    },
    standing: {
      color: '#fff'
    },
    teamName: {
      color: '#ffffff',
      fontSize: 18,
      fontWeight: '700'
    },
    row: {
      flexDirection: 'row',
      padding: 15,
    },
    container: {
      flex: 1,
      backgroundColor: '#e3e3e3',
    },
    linearGradient: {
      backgroundColor: "transparent",
      position: "absolute",
      top: 0,
      bottom: 0,
      left: 0,
      right: 0
    },
    teamPic: {
      width: 80,
      height: 80
    }
});