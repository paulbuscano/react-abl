import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, ImageBackground, ScrollView, Image, StatusBar, Button, WebView, ActivityIndicator} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class about extends Component{
    static navigationOptions = ({ navigation }) => ( {
        headerBackground: (
          <Image
            style={[StyleSheet.absoluteFillObject]}
            source={{ uri: 'http://aseanbasketballleague.com/wp-content/uploads/abl-app/header-bg.jpg' }}
          />
        ),
        headerStyle: {
          backgroundColor: '#000000',
          ...Platform.select({
            ios: {     
            
            },
            android: {
              // height: 80,
              // paddingTop: 20
            },
          }),
        },
        headerTintColor: '#fff',
        title: 'ABOUT ABL', 
        headerTitleStyle: {
        fontWeight: 'bold',
        flex: 1,
          ...Platform.select({
            ios: {     
            
            },
            android: {
             // marginTop: 200,
            
            },
          }),
        }, 
      });

    constructor(props) {
      super(props)
      this.state = {
        visible: true,
      }; 
    } 
    
    hideSpinner() {
      this.setState({
        visible: false,
      });
    }

    render() {
      return(
        <View style={styles.container}>
          <WebView  
          originWhitelist={['*']}
          source={{uri: 'http://aseanbasketballleague.com/about-us/'}}
          style={{marginTop: 0, backgroundColor: '#000000'}}
          onLoad={() => this.hideSpinner()}       
          // renderLoading={ ()=> {return(<ActivityIndicator size="large" color="#000" style = {{position: 'absolute',left: 0,right: 0, top: 0,bottom: 0,alignItems: 'center',justifyContent: 'center'}}/>)} }
          // startInLoadingState
          />
          {this.state.visible && (
            <ActivityIndicator
              style={{ position: "absolute", top: 50 + '%', left: 45 + '%', alignItems: 'center', justifyContent: 'center' }}
              size="large"
              color="#fff"
            />
          )}
        </View>
      )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#e3e3e3',
      overflow:'hidden',
    },
});