import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, ImageBackground, ScrollView, Image, StatusBar, Button, WebView, Dimensions, ActivityIndicator, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Flag from 'react-native-flags';

export default class leaders extends Component {
    static navigationOptions = ({ navigation }) => ({
        headerBackground: (
            <Image
                style={{ width: '100%', height: '100%' }}
                source={require('./src/bg-header.jpg')}
            />
        ),
        headerStyle: {
            backgroundColor: '#000000',
            ...Platform.select({
                ios: {

                },
                android: {
                    //   height: 80,
                    //   paddingTop: 20
                },
            }),
        },
        headerTintColor: '#fff',
        title: 'LEAGUE LEADERS',
        headerTitleStyle: {
            fontWeight: 'bold',
            flex: 1,
            ...Platform.select({
                ios: {

                },
                android: {
                    // marginTop: 200,

                },
            }),
        },
    });

    constructor(props) {
        super(props)
        this.state = {
            activeIndex: 0,
            isLoading: true,
            dataPoints: [],
            dataAssists: [],
            dataRebounds: [],
            dataBlocks: [],
            dataSteals: [],
            dataFg: [],
            dataThreePercentage: [],
            dataThreePm: [],
            dataTeams: [],
            dataTeamStats: [],
            isMounted: false,
        };
    }

    componentDidMount() {
        this.setState({isMounted: true})
        this.getData();
    }

    componentWillUnmount(){
        this.state.isMounted = false
    }

    getData = async () => {

        let comid = 26702;

        var requestPoints = 'https://api.wh.geniussports.com/v1/basketball/competitions/' + comid + '/leaders/sPointsAverage?ak=eebd8ae256142ac3fd24bd2003d28782&fields=images,teamId,familyName,firstName,sPointsAverage';
        var requestAssists = 'https://api.wh.geniussports.com/v1/basketball/competitions/' + comid + '/leaders/sAssistsAverage?ak=eebd8ae256142ac3fd24bd2003d28782&fields=images,teamId,familyName,firstName,sAssistsAverage';
        var requestRebounds = 'https://api.wh.geniussports.com/v1/basketball/competitions/' + comid + '/leaders/sReboundsTotalAverage?ak=eebd8ae256142ac3fd24bd2003d28782&fields=images,teamId,familyName,firstName,sReboundsTotalAverage';
        var requestBlocks = 'https://api.wh.geniussports.com/v1/basketball/competitions/' + comid + '/leaders/sBlocksAverage?ak=eebd8ae256142ac3fd24bd2003d28782&fields=images,teamId,familyName,firstName,sBlocksAverage';
        var requestSteals = 'https://api.wh.geniussports.com/v1/basketball/competitions/' + comid + '/leaders/sStealsAverage?ak=eebd8ae256142ac3fd24bd2003d28782&fields=images,teamId,familyName,firstName,sStealsAverage';
        var requestFg = 'https://api.wh.geniussports.com/v1/basketball/competitions/' + comid + '/leaders/sFieldGoalsPercentage?ak=eebd8ae256142ac3fd24bd2003d28782&fields=images,teamId,familyName,firstName,sFieldGoalsPercentage';
        var requestThreePm = 'https://api.wh.geniussports.com/v1/basketball/competitions/' + comid + '/leaders/sThreePointersMade?ak=eebd8ae256142ac3fd24bd2003d28782&fields=images,teamId,familyName,firstName,sThreePointersMade';
        var requestThreePercentage = 'https://api.wh.geniussports.com/v1/basketball/competitions/' + comid + '/leaders/sThreePointersPercentage?ak=eebd8ae256142ac3fd24bd2003d28782&fields=images,teamId,familyName,firstName,sThreePointersPercentage';

        var requestTeamStats = 'https://api.wh.geniussports.com/v1/basketball/statistics/teamcompetition/competitions/' + comid + '?ak=eebd8ae256142ac3fd24bd2003d28782&periodNumber=0&fields=logo,teamName,sThreePointersMade,sPointsAverage,sAssistsAverage,sReboundsTotalAverage,sBlocksAverage,sStealsAverage,sFieldGoalsPercentage,sThreePointersMade,sThreePointersPercentage&limit=100';

        var requestTeams = 'https://api.wh.geniussports.com/v1/basketball/competitions/' + comid + '/teams?ak=eebd8ae256142ac3fd24bd2003d28782&fields=images,teamId,teamCode&limit=100';

        return fetch(requestPoints).then((response) => response.json()).then((responseJson) => {
            if (this.state.isMounted) {
            this.setState({
                dataPoints: responseJson.response.data,
            });
        }
        }).then(() => {
            return fetch(requestAssists).then((response) => response.json()).then((responseJson) => {
                if (this.state.isMounted) {
                this.setState({
                    dataAssists: responseJson.response.data,
                });
            }
            }).catch((error) => {
                console.error(error);
            });
        }).then(() => {
            return fetch(requestTeams).then((response) => response.json()).then((responseJson) => {
                if (this.state.isMounted) {
                this.setState({
                    dataTeams: responseJson.response.data,
                });
            }
            }).catch((error) => {
                console.error(error);
            });
        }).then(() => {
            return fetch(requestRebounds).then((response) => response.json()).then((responseJson) => {
                if (this.state.isMounted) {
                this.setState({
                    dataRebounds: responseJson.response.data,
                });
            }
            }).catch((error) => {
                console.error(error);
            });
        }).then(() => {
            return fetch(requestBlocks).then((response) => response.json()).then((responseJson) => {
                if (this.state.isMounted) {
                this.setState({
                    dataBlocks: responseJson.response.data,
                });
            }
            }).catch((error) => {
                console.error(error);
            });
        }).then(() => {
            return fetch(requestSteals).then((response) => response.json()).then((responseJson) => {
                if (this.state.isMounted) {
                this.setState({
                    dataSteals: responseJson.response.data,
                });
            }
            }).catch((error) => {
                console.error(error);
            });
        }).then(() => {
            return fetch(requestFg).then((response) => response.json()).then((responseJson) => {
                if (this.state.isMounted) {
                this.setState({
                    dataFg: responseJson.response.data,
                });
            }
            }).catch((error) => {
                console.error(error);
            });
        }).then(() => {
            return fetch(requestThreePm).then((response) => response.json()).then((responseJson) => {
                if (this.state.isMounted) {
                this.setState({
                    dataThreePm: responseJson.response.data,
                });
            }
            }).catch((error) => {
                console.error(error);
            });
        }).then(() => {
            return fetch(requestThreePercentage).then((response) => response.json()).then((responseJson) => {
                if (this.state.isMounted) {
                this.setState({
                    dataThreePercentage: responseJson.response.data,
                });
            }
            }).catch((error) => {
                console.error(error);
            });
        }).then(() => {
            return fetch(requestTeamStats).then((response) => response.json()).then((responseJson) => {
                if (this.state.isMounted) {
                this.setState({
                    isLoading: false,
                    dataTeamStats: responseJson.response.data,
                });
            }
            }).catch((error) => {
                console.error(error);
            });
        }).catch((error) => {
            console.error(error);
        });
    }

    segmentClicked = (index) => {
        this.setState({
            activeIndex: index
        })
    }

    renderSection = () => {
        if (this.state.activeIndex == 0) {

            if (this.state.dataPoints.length > 0) {


                let Newpoints;
                let Newassists;
                let Newrebounds;
                let Newblocks;
                let Newsteals;
                let Newfg;
                let Newthreepercentage;
                let Newthreepm;

                //function sort array api to 
                function sortByKey(array, key) {
                    return array.sort(function (a, b) {
                        var x = parseInt(a[key]); var y = parseInt(b[key]);
                        return ((y < x) ? -1 : ((y > x) ? 1 : 0));
                    });
                }

                Newpoints = sortByKey(this.state.dataPoints, 'sPointsAverage');
                Newassists = sortByKey(this.state.dataAssists, "sAssistsAverage");
                Newrebounds = sortByKey(this.state.dataRebounds, "sReboundsTotalAverage");
                Newblocks = sortByKey(this.state.dataBlocks, "sBlocksAverage");
                Newsteals = sortByKey(this.state.dataSteals, "sStealsAverage");
                Newfg = sortByKey(this.state.dataFg, "sFieldGoalsPercentage");
                Newthreepercentage = sortByKey(this.state.dataThreePercentage, "sThreePointersPercentage");
                Newthreepm = sortByKey(this.state.dataThreePm, "sThreePointersMade");

                let teamLogo;
                let teamCode;

                let teamLogoAssists;
                let teamCodeAsists;

                let teamLogoRebounds;
                let teamCodeRebounds;

                let teamLogoBlocks;
                let teamCodeBlocks;

                let teamLogoSteals;
                let teamCodeSteals;

                let teamLogoFg;
                let teamCodeFg;

                let teamLogoThreePercentage;
                let teamCodeThreePercentage;

                let teamLogoThreePm;
                let teamCodeThreePm;

                let standings = this.state.dataTeams.map((ey, key) => {
                    if (Newpoints[0].teamId == ey.teamId) {
                        teamLogo = ey.images.logo.T1.url;
                        teamCode = ey.teamCode;
                    }
                    if (Newassists[0].teamId == ey.teamId) {
                        teamLogoAssists = ey.images.logo.T1.url;
                        teamCodeAsists = ey.teamCode;
                    }
                    if (Newrebounds[0].teamId == ey.teamId) {
                        teamLogoRebounds = ey.images.logo.T1.url;
                        teamCodeRebounds = ey.teamCode;
                    }
                    if (Newblocks[0].teamId == ey.teamId) {
                        teamLogoBlocks = ey.images.logo.T1.url;
                        teamCodeBlocks = ey.teamCode;
                    }
                    if (Newsteals[0].teamId == ey.teamId) {
                        teamLogoSteals = ey.images.logo.T1.url;
                        teamCodeSteals = ey.teamCode;
                    }
                    if (Newfg[0].teamId == ey.teamId) {
                        teamLogoFg = ey.images.logo.T1.url;
                        teamCodeFg = ey.teamCode;
                    }
                    if (Newthreepercentage[0].teamId == ey.teamId) {
                        teamLogoThreePercentage = ey.images.logo.T1.url;
                        teamCodeThreePercentage = ey.teamCode;
                    }
                    if (Newthreepm[0].teamId == ey.teamId) {
                        teamLogoThreePm = ey.images.logo.T1.url;
                        teamCodeThreePm = ey.teamCode;
                    }
                });

                let shit = 1;

                let i;
                let j = 1;
                let newArrayPoints = [];

                for (i = 0; i < 4; i++) {
                    newArrayPoints[i] = Newpoints[j];
                    j++;
                }

                let lastNameTestPoints = Newpoints[0].familyName.toLowerCase();
                let convertedLastNamePoints = lastNameTestPoints.charAt(0).toUpperCase() + lastNameTestPoints.slice(1);

                let pointsLead = newArrayPoints.map((val, key) => {

                    let lastNameTest = val.familyName.toLowerCase();
                    let convertedLastName = lastNameTest.charAt(0).toUpperCase() + lastNameTest.slice(1);

                    let teamLogoFive;
                    let teamCodeFive;

                    let standingsFive = this.state.dataTeams.map((wo, key) => {
                        if (val.teamId == wo.teamId) {
                            teamLogoFive = wo.images.logo.T1.url;
                            teamCodeFive = wo.teamCode;
                        }
                    });

                    shit++;

                    let testPpgAll = val.sPointsAverage.toString();
                    let convertedPpgAll;

                    if (testPpgAll.length < 5) {
                        convertedPpgAll = testPpgAll.substring(0, 3);
                    } else {
                        convertedPpgAll = testPpgAll.substring(0, 4);
                    }

                    let testppg;
                    if (val.images.photo == null) {
                        testppg = 'https://img.wh.sportingpulseinternational.com/16d773bd4cae752437e856ce82c06102T1.png';
                    } else {
                        testppg = val.images.photo.T1.url;
                    }

                    return <View style={styles.rowInfo}>
                        <View style={styles.authorPlayers}>
                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                <Text style={[styles.number, { marginRight: 10, }]}>{shit}</Text>
                            </View>
                            <Image
                                style={styles.userPic}
                                source={{ uri: testppg }}
                            />
                            <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center' }}>
                                <View>
                                    <Text style={styles.rosterName}>{val.firstName} {convertedLastName}</Text>
                                </View>
                                <View>
                                    <Text style={styles.rosterPosition}>PPG: {convertedPpgAll} &#8226; {teamCodeFive}</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: "center" }}>
                            <Image
                                style={styles.userPicTeam}
                                source={{ uri: teamLogoFive }}
                            />
                        </View>
                    </View>

                });

                let shitAssists = 1;

                let iAssists;
                let jAssists = 1;
                let newArrayAssists = [];

                for (iAssists = 0; iAssists < 4; iAssists++) {
                    newArrayAssists[iAssists] = Newassists[jAssists];
                    jAssists++;
                }

                let lastNameTestAssists = Newassists[0].familyName.toLowerCase();

                let convertedLastNameAssists = lastNameTestAssists.charAt(0).toUpperCase() + lastNameTestAssists.slice(1);

                let assistsLead = newArrayAssists.map((val, key) => {

                    let lastNameTest = val.familyName.toLowerCase();
                    let convertedLastName = lastNameTest.charAt(0).toUpperCase() + lastNameTest.slice(1);

                    let teamLogoFive;
                    let teamCodeFive;

                    let standingsFive = this.state.dataTeams.map((wo, key) => {
                        if (val.teamId == wo.teamId) {
                            teamLogoFive = wo.images.logo.T1.url;
                            teamCodeFive = wo.teamCode;
                        }
                    });

                    shitAssists++;

                    let testApgAll = val.sAssistsAverage.toString();
                    let convertedApgAll;

                    if (testApgAll.length < 5) {
                        convertedApgAll = testApgAll.substring(0, 3);
                    } else {
                        convertedApgAll = testApgAll.substring(0, 4);
                    }

                    let testapg;
                    if (val.images.photo == null) {
                        testapg = 'https://img.wh.sportingpulseinternational.com/16d773bd4cae752437e856ce82c06102T1.png';
                    } else {
                        testapg = val.images.photo.T1.url;
                    }

                    return <View style={styles.rowInfo}>
                        <View style={styles.authorPlayers}>
                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                <Text style={[styles.number, { marginRight: 10, }]}>{shitAssists}</Text>
                            </View>
                            <Image
                                style={styles.userPic}
                                source={{ uri: testapg }}
                            />
                            <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center' }}>
                                <View>
                                    <Text style={styles.rosterName}>{val.firstName} {convertedLastName}</Text>
                                </View>
                                <View>
                                    <Text style={styles.rosterPosition}>APG: {convertedApgAll} &#8226; {teamCodeFive}</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: "center" }}>
                            <Image
                                style={styles.userPicTeam}
                                source={{ uri: teamLogoFive }}
                            />
                        </View>
                    </View>
                });

                let shitRebounds = 1;

                let iRebounds;
                let jRebounds = 1;
                let newArrayRebounds = [];

                for (iRebounds = 0; iRebounds < 4; iRebounds++) {
                    newArrayRebounds[iRebounds] = Newrebounds[jRebounds];
                    jRebounds++;
                }

                let lastNameTestRebounds = Newrebounds[0].familyName.toLowerCase();
                let convertedLastNameRebounds = lastNameTestRebounds.charAt(0).toUpperCase() + lastNameTestRebounds.slice(1);

                let reboundsLead = newArrayRebounds.map((val, key) => {

                    let lastNameTest = val.familyName.toLowerCase();
                    let convertedLastName = lastNameTest.charAt(0).toUpperCase() + lastNameTest.slice(1);

                    let teamLogoFive;
                    let teamCodeFive;

                    let standingsFive = this.state.dataTeams.map((wo, key) => {
                        if (val.teamId == wo.teamId) {
                            teamLogoFive = wo.images.logo.T1.url;
                            teamCodeFive = wo.teamCode;
                        }
                    });

                    shitRebounds++;

                    let testRpgAll = val.sReboundsTotalAverage.toString();
                    let convertedRpgAll;

                    if (testRpgAll.length < 5) {
                        convertedRpgAll = testRpgAll.substring(0, 3);
                    } else {
                        convertedRpgAll = testRpgAll.substring(0, 4);
                    }

                    let testrpg;
                    if (val.images.photo == null) {
                        testrpg = 'https://img.wh.sportingpulseinternational.com/16d773bd4cae752437e856ce82c06102T1.png';
                    } else {
                        testrpg = val.images.photo.T1.url;
                    }

                    return <View style={styles.rowInfo}>
                        <View style={styles.authorPlayers}>
                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                <Text style={[styles.number, { marginRight: 10, }]}>{shitRebounds}</Text>
                            </View>
                            <Image
                                style={styles.userPic}
                                source={{ uri: testrpg }}
                            />
                            <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center' }}>
                                <View>
                                    <Text style={styles.rosterName}>{val.firstName} {convertedLastName}</Text>
                                </View>
                                <View>
                                    <Text style={styles.rosterPosition}>RPG: {convertedRpgAll} &#8226; {teamCodeFive}</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: "center" }}>
                            <Image
                                style={styles.userPicTeam}
                                source={{ uri: teamLogoFive }}
                            />
                        </View>
                    </View>
                });

                let shitBlocks = 1;

                let iBlocks;
                let jBlocks = 1;
                let newArrayBlocks = [];

                for (iBlocks = 0; iBlocks < 4; iBlocks++) {
                    newArrayBlocks[iBlocks] = Newblocks[jBlocks];
                    jBlocks++;
                }

                let lastNameTestBlocks = Newblocks[0].familyName.toLowerCase();
                let convertedLastNameBlocks = lastNameTestBlocks.charAt(0).toUpperCase() + lastNameTestBlocks.slice(1);

                let blocksLead = newArrayBlocks.map((val, key) => {

                    let lastNameTest = val.familyName.toLowerCase();
                    let convertedLastName = lastNameTest.charAt(0).toUpperCase() + lastNameTest.slice(1);

                    let teamLogoFive;
                    let teamCodeFive;

                    let standingsFive = this.state.dataTeams.map((wo, key) => {
                        if (val.teamId == wo.teamId) {
                            teamLogoFive = wo.images.logo.T1.url;
                            teamCodeFive = wo.teamCode;
                        }
                    });

                    shitBlocks++;

                    let testBpgAll = val.sBlocksAverage.toString();
                    let convertedBpgAll;

                    if (testBpgAll.length < 5) {
                        convertedBpgAll = testBpgAll.substring(0, 3);
                    } else {
                        convertedBpgAll = testBpgAll.substring(0, 4);
                    }

                    let testbpg

                    if (val.images.photo == null) {
                        testbpg = 'https://img.wh.sportingpulseinternational.com/16d773bd4cae752437e856ce82c06102T1.png';
                    } else {
                        testbpg = val.images.photo.T1.url;
                    }

                    return <View style={styles.rowInfo}>
                        <View style={styles.authorPlayers}>
                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                <Text style={[styles.number, { marginRight: 10, }]}>{shitBlocks}</Text>
                            </View>
                            <Image
                                style={styles.userPic}
                                source={{ uri: testbpg }}
                            />
                            <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center' }}>
                                <View>
                                    <Text style={styles.rosterName}>{val.firstName} {convertedLastName}</Text>
                                </View>
                                <View>
                                    <Text style={styles.rosterPosition}>BPG: {convertedBpgAll} &#8226; {teamCodeFive}</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: "center" }}>
                            <Image
                                style={styles.userPicTeam}
                                source={{ uri: teamLogoFive }}
                            />
                        </View>
                    </View>
                });

                let shitSteals = 1;

                let iSteals;
                let jSteals = 1;
                let newArraySteals = [];

                for (iSteals = 0; iSteals < 4; iSteals++) {
                    newArraySteals[iSteals] = Newsteals[jSteals];
                    jSteals++;
                }

                let lastNameTestSteals = Newsteals[0].familyName.toLowerCase();
                let convertedLastNameSteals = lastNameTestSteals.charAt(0).toUpperCase() + lastNameTestSteals.slice(1);

                let stealsLead = newArraySteals.map((val, key) => {

                    let lastNameTest = val.familyName.toLowerCase();
                    let convertedLastName = lastNameTest.charAt(0).toUpperCase() + lastNameTest.slice(1);

                    let teamLogoFive;
                    let teamCodeFive;

                    let standingsFive = this.state.dataTeams.map((wo, key) => {
                        if (val.teamId == wo.teamId) {
                            teamLogoFive = wo.images.logo.T1.url;
                            teamCodeFive = wo.teamCode;
                        }
                    });

                    shitSteals++;

                    let testSpgAll = val.sStealsAverage.toString();
                    let convertedSpgAll;

                    if (testSpgAll.length < 5) {
                        convertedSpgAll = testSpgAll.substring(0, 3);
                    } else {
                        convertedSpgAll = testSpgAll.substring(0, 4);
                    }

                    let testspg;

                    if (val.images.photo == null) {
                        testspg = 'https://img.wh.sportingpulseinternational.com/16d773bd4cae752437e856ce82c06102T1.png';
                    } else {
                        testspg = val.images.photo.T1.url;
                    }

                    return <View style={styles.rowInfo}>
                        <View style={styles.authorPlayers}>
                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                <Text style={[styles.number, { marginRight: 10, }]}>{shitSteals}</Text>
                            </View>
                            <Image
                                style={styles.userPic}
                                source={{ uri: testspg }}
                            />
                            <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center' }}>
                                <View>
                                    <Text style={styles.rosterName}>{val.firstName} {convertedLastName}</Text>
                                </View>
                                <View>
                                    <Text style={styles.rosterPosition}>SPG: {convertedSpgAll} &#8226; {teamCodeFive}</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: "center" }}>
                            <Image
                                style={styles.userPicTeam}
                                source={{ uri: teamLogoFive }}
                            />
                        </View>
                    </View>
                });

                let shitFg = 1;

                let iFg;
                let jFg = 1;
                let newArrayFg = [];

                for (iFg = 0; iFg < 4; iFg++) {
                    newArrayFg[iFg] = Newfg[jFg];
                    jFg++;
                }

                let lastNameTestFg = Newfg[0].familyName.toLowerCase();
                let convertedLastNameFg = lastNameTestFg.charAt(0).toUpperCase() + lastNameTestFg.slice(1);

                let fgLead = newArrayFg.map((val, key) => {

                    let lastNameTest = val.familyName.toLowerCase();
                    let convertedLastName = lastNameTest.charAt(0).toUpperCase() + lastNameTest.slice(1);

                    let teamLogoFive;
                    let teamCodeFive;

                    let standingsFive = this.state.dataTeams.map((wo, key) => {
                        if (val.teamId == wo.teamId) {
                            teamLogoFive = wo.images.logo.T1.url;
                            teamCodeFive = wo.teamCode;
                        }
                    });

                    shitFg++;


                    let postFg = val.sFieldGoalsPercentage.toString();
                    let fgFinal = postFg.substring(2, 4);
                    let fgFinalNa;

                    if (fgFinal.length == 1) {
                        fgFinalNa = fgFinal + '0';
                    } else {
                        fgFinalNa = fgFinal;
                    }

                    let testfg;
                    if (val.images.photo == null) {
                        testfg = 'https://img.wh.sportingpulseinternational.com/16d773bd4cae752437e856ce82c06102T1.png';
                    } else {
                        testfg = val.images.photo.T1.url;
                    }

                    return <View style={styles.rowInfo}>
                        <View style={styles.authorPlayers}>
                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                <Text style={[styles.number, { marginRight: 10, }]}>{shitFg}</Text>
                            </View>
                            <Image
                                style={styles.userPic}
                                source={{ uri: testfg }}
                            />
                            <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center' }}>
                                <View>
                                    <Text style={styles.rosterName}>{val.firstName} {convertedLastName}</Text>
                                </View>
                                <View>
                                    <Text style={styles.rosterPosition}>FG: {fgFinalNa}% &#8226; {teamCodeFive}</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: "center" }}>
                            <Image
                                style={styles.userPicTeam}
                                source={{ uri: teamLogoFive }}
                            />
                        </View>
                    </View>
                });

                let threepercentageFinalNa;
                let threepercentageFinal;

                let postThreePercentage = Newthreepercentage[0].sThreePointersPercentage.toString();
                if (postThreePercentage.length == 1) {
                    threepercentageFinalNa = postThreePercentage + '00';
                } else {

                    threepercentageFinal = postThreePercentage.substring(2, 4);

                    if (threepercentageFinal.length == 1) {
                        threepercentageFinalNa = threepercentageFinal + '0';
                    } else {
                        threepercentageFinalNa = threepercentageFinal;
                    }

                }

                let shitThreePercentage = 1;

                let iThreePercentage;
                let jThreePercentage = 1;
                let newArrayThreePercentage = [];

                for (iThreePercentage = 0; iThreePercentage < 4; iThreePercentage++) {
                    newArrayThreePercentage[iThreePercentage] = Newthreepercentage[jThreePercentage];
                    jThreePercentage++;
                }

                let lastNameTestThreePercentage = Newthreepercentage[0].familyName.toLowerCase();
                let convertedLastNameThreePercentage = lastNameTestThreePercentage.charAt(0).toUpperCase() + lastNameTestThreePercentage.slice(1);

                let threepercentageLead = newArrayThreePercentage.map((val, key) => {

                    let lastNameTest = val.familyName.toLowerCase();
                    let convertedLastName = lastNameTest.charAt(0).toUpperCase() + lastNameTest.slice(1);

                    let teamLogoFive;
                    let teamCodeFive;

                    let standingsFive = this.state.dataTeams.map((wo, key) => {
                        if (val.teamId == wo.teamId) {
                            teamLogoFive = wo.images.logo.T1.url;
                            teamCodeFive = wo.teamCode;
                        }
                    });

                    shitThreePercentage++;

                    let threepercentageFinal;
                    let threepercentageFinalNa;
                    let postThreePercentage = val.sThreePointersPercentage.toString();
                    
                    if (postThreePercentage.length == 1) {
                        threepercentageFinalNa = postThreePercentage + '00';
                    } else {
    
                        threepercentageFinal = postThreePercentage.substring(2, 4);
    
                        if (threepercentageFinal.length == 1) {
                            threepercentageFinalNa = threepercentageFinal + '0';
                        } else {
                            threepercentageFinalNa = threepercentageFinal;
                        }
    
                    }

                    let testtpg;

                    if (val.images.photo == null) {
                        testtpg = 'https://img.wh.sportingpulseinternational.com/16d773bd4cae752437e856ce82c06102T1.png';
                    } else {
                        testtpg = val.images.photo.T1.url;
                    }

                    return <View style={styles.rowInfo}>
                        <View style={styles.authorPlayers}>
                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                <Text style={[styles.number, { marginRight: 10, }]}>{shitThreePercentage}</Text>
                            </View>
                            <Image
                                style={styles.userPic}
                                source={{ uri: testtpg }}
                            />
                            <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center' }}>
                                <View>
                                    <Text style={styles.rosterName}>{val.firstName} {convertedLastName}</Text>
                                </View>
                                <View>
                                    <Text style={styles.rosterPosition}>3P%: {threepercentageFinalNa}% &#8226; {teamCodeFive}</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: "center" }}>
                            <Image
                                style={styles.userPicTeam}
                                source={{ uri: teamLogoFive }}
                            />
                        </View>
                    </View>
                });

                let fgFinal;
                let fgFinalNa;
                let postFg = Newfg[0].sFieldGoalsPercentage.toString();

                if (postFg.length == 1) {
                    fgFinalNa = postFg + '00';
                } else {

                    fgFinal = postFg.substring(2, 4);

                    if (fgFinal.length == 1) {
                        fgFinalNa = fgFinal + '0';
                    } else {
                        fgFinalNa = fgFinal;
                    }
                }

                let shitThreePm = 1;

                let iThreePm;
                let jThreePm = 1;
                let newArrayThreePm = [];

                for (iThreePm = 0; iThreePm < 4; iThreePm++) {
                    newArrayThreePm[iThreePm] = Newthreepm[jThreePm];
                    jThreePm++;
                }

                let lastNameTestThreePm = Newthreepm[0].familyName.toLowerCase();
                let convertedLastNameThreePm = lastNameTestThreePm.charAt(0).toUpperCase() + lastNameTestThreePm.slice(1);

                let threepmLead = newArrayThreePm.map((val, key) => {

                    let lastNameTest = val.familyName.toLowerCase();
                    let convertedLastName = lastNameTest.charAt(0).toUpperCase() + lastNameTest.slice(1);

                    let teamLogoFive;
                    let teamCodeFive;

                    let standingsFive = this.state.dataTeams.map((wo, key) => {
                        if (val.teamId == wo.teamId) {
                            teamLogoFive = wo.images.logo.T1.url;
                            teamCodeFive = wo.teamCode;
                        }
                    });

                    shitThreePm++;

                    let testtpm;
                    if (val.images.photo == null) {
                        testtpm = 'https://img.wh.sportingpulseinternational.com/16d773bd4cae752437e856ce82c06102T1.png';
                    } else {
                        testtpm = val.images.photo.T1.url;
                    }

                    return <View style={styles.rowInfo}>
                        <View style={styles.authorPlayers}>
                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                <Text style={[styles.number, { marginRight: 10, }]}>{shitThreePm}</Text>
                            </View>
                            <Image
                                style={styles.userPic}
                                source={{ uri: testtpm }}
                            />
                            <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center' }}>
                                <View>
                                    <Text style={styles.rosterName}>{val.firstName} {convertedLastName}</Text>
                                </View>
                                <View>
                                    <Text style={styles.rosterPosition}>3PM: {val.sThreePointersMade} &#8226; {teamCodeFive}</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: "center" }}>
                            <Image
                                style={styles.userPicTeam}
                                source={{ uri: teamLogoFive }}
                            />
                        </View>
                    </View>
                });

                let testPpgOne = Newpoints[0].sPointsAverage.toString();
                let convertedPpgOne;

                if (testPpgOne.length < 5) {
                    convertedPpgOne = testPpgOne.substring(0, 3);
                } else {
                    convertedPpgOne = testPpgOne.substring(0, 4);
                }

                let testApgOne = Newassists[0].sAssistsAverage.toString();
                let convertedApgOne;

                if (testApgOne.length < 5) {
                    convertedApgOne = testApgOne.substring(0, 3);
                } else {
                    convertedApgOne = testApgOne.substring(0, 4);
                }

                let testRpgOne = Newrebounds[0].sReboundsTotalAverage.toString();
                let convertedRpgOne;

                if (testRpgOne.length < 5) {
                    convertedRpgOne = testRpgOne.substring(0, 3);
                } else {
                    convertedRpgOne = testRpgOne.substring(0, 4);
                }

                let testBpgOne = Newblocks[0].sBlocksAverage.toString();
                let convertedBpgOne;

                if (testBpgOne.length < 5) {
                    convertedBpgOne = testBpgOne.substring(0, 3);
                } else {
                    convertedBpgOne = testBpgOne.substring(0, 4);
                }

                let testSpgOne = Newsteals[0].sStealsAverage.toString();
                let convertedSpgOne;

                if (testSpgOne.length < 5) {
                    convertedSpgOne = testSpgOne.substring(0, 3);
                } else {
                    convertedSpgOne = testSpgOne.substring(0, 4);
                }

                return (
                    <View style={{ backgroundColor: '#e3e3e3', }}>
                        <View style={styles.group}>
                            <View style={styles.rowPer}>
                                <Text style={styles.monthPer}>POINTS PER GAME</Text>
                            </View>
                            <View style={styles.row}>
                                <View style={[styles.rowInfo, { borderBottomWidth: 1, borderBottomColor: '#e3e3e3' }]}>
                                    <View style={styles.authorPlayers}>
                                        <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                            <Text style={[styles.numberOne, { marginRight: 10, }]}>1</Text>
                                        </View>
                                        <Image
                                            style={styles.userPic}
                                            source={{ uri: Newpoints[0].images.photo.T1.url }}
                                        />
                                        <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', }}>
                                            <View>
                                                <Text style={styles.rosterName}>{Newpoints[0].firstName} {convertedLastNamePoints}</Text>
                                            </View>
                                            <View>
                                                <Text style={styles.rosterPositionFirst}>PPG: {convertedPpgOne} &#8226; {teamCode}</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: "center" }}>
                                        <Image
                                            style={styles.userPicTeam}
                                            source={{ uri: teamLogo }}
                                        />
                                    </View>
                                </View>
                                {pointsLead}
                            </View>
                            <View style={styles.rowPer}>
                                <Text style={styles.monthPer}>ASSISTS PER GAME</Text>
                            </View>
                            <View style={styles.row}>
                                <View style={[styles.rowInfo, { borderBottomWidth: 1, borderBottomColor: '#e3e3e3' }]}>
                                    <View style={styles.authorPlayers}>
                                        <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                            <Text style={[styles.numberOne, { marginRight: 10, }]}>1</Text>
                                        </View>
                                        <Image
                                            style={styles.userPic}
                                            source={{ uri: Newassists[0].images.photo.T1.url }}
                                        />
                                        <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', }}>
                                            <View>
                                                <Text style={styles.rosterName}>{Newassists[0].firstName} {convertedLastNameAssists}</Text>
                                            </View>
                                            <View>
                                                <Text style={styles.rosterPositionFirst}>APG: {convertedApgOne} &#8226; {teamCodeAsists}</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: "center" }}>
                                        <Image
                                            style={styles.userPicTeam}
                                            source={{ uri: teamLogoAssists }}
                                        />
                                    </View>
                                </View>
                                {assistsLead}
                            </View>
                            <View style={styles.rowPer}>
                                <Text style={styles.monthPer}>REBOUNDS PER GAME</Text>
                            </View>
                            <View style={styles.row}>
                                <View style={[styles.rowInfo, { borderBottomWidth: 1, borderBottomColor: '#e3e3e3' }]}>
                                    <View style={styles.authorPlayers}>
                                        <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                            <Text style={[styles.numberOne, { marginRight: 10, }]}>1</Text>
                                        </View>
                                        <Image
                                            style={styles.userPic}
                                            source={{ uri: Newrebounds[0].images.photo.T1.url }}
                                        />
                                        <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', }}>
                                            <View>
                                                <Text style={styles.rosterName}>{Newrebounds[0].firstName} {convertedLastNameRebounds}</Text>
                                            </View>
                                            <View>
                                                <Text style={styles.rosterPositionFirst}>RPG: {convertedRpgOne} &#8226; {teamCodeRebounds}</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: "center" }}>
                                        <Image
                                            style={styles.userPicTeam}
                                            source={{ uri: teamLogoRebounds }}
                                        />
                                    </View>
                                </View>
                                {reboundsLead}
                            </View>
                            <View style={styles.rowPer}>
                                <Text style={styles.monthPer}>BLOCKS PER GAME</Text>
                            </View>
                            <View style={styles.row}>
                                <View style={[styles.rowInfo, { borderBottomWidth: 1, borderBottomColor: '#e3e3e3' }]}>
                                    <View style={styles.authorPlayers}>
                                        <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                            <Text style={[styles.numberOne, { marginRight: 10, }]}>1</Text>
                                        </View>
                                        <Image
                                            style={styles.userPic}
                                            source={{ uri: Newblocks[0].images.photo.T1.url }}
                                        />
                                        <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', }}>
                                            <View>
                                                <Text style={styles.rosterName}>{Newblocks[0].firstName} {convertedLastNameBlocks}</Text>
                                            </View>
                                            <View>
                                                <Text style={styles.rosterPositionFirst}>BPG: {convertedBpgOne} &#8226; {teamCodeBlocks}</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: "center" }}>
                                        <Image
                                            style={styles.userPicTeam}
                                            source={{ uri: teamLogoBlocks }}
                                        />
                                    </View>
                                </View>
                                {blocksLead}
                            </View>
                            <View style={styles.rowPer}>
                                <Text style={styles.monthPer}>STEALS PER GAME</Text>
                            </View>
                            <View style={styles.row}>
                                <View style={[styles.rowInfo, { borderBottomWidth: 1, borderBottomColor: '#e3e3e3' }]}>
                                    <View style={styles.authorPlayers}>
                                        <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                            <Text style={[styles.numberOne, { marginRight: 10, }]}>1</Text>
                                        </View>
                                        <Image
                                            style={styles.userPic}
                                            source={{ uri: Newsteals[0].images.photo.T1.url }}
                                        />
                                        <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', }}>
                                            <View>
                                                <Text style={styles.rosterName}>{Newsteals[0].firstName} {convertedLastNameSteals}</Text>
                                            </View>
                                            <View>
                                                <Text style={styles.rosterPositionFirst}>SPG: {convertedSpgOne} &#8226; {teamCodeSteals}</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: "center" }}>
                                        <Image
                                            style={styles.userPicTeam}
                                            source={{ uri: teamLogoSteals }}
                                        />
                                    </View>
                                </View>
                                {stealsLead}
                            </View>
                            <View style={styles.rowPer}>
                                <Text style={styles.monthPer}>FIELD GOAL PERCENTAGE</Text>
                            </View>
                            <View style={styles.row}>
                                <View style={[styles.rowInfo, { borderBottomWidth: 1, borderBottomColor: '#e3e3e3' }]}>
                                    <View style={styles.authorPlayers}>
                                        <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                            <Text style={[styles.numberOne, { marginRight: 10, }]}>1</Text>
                                        </View>
                                        <Image
                                            style={styles.userPic}
                                            source={{ uri: Newfg[0].images.photo.T1.url }}
                                        />
                                        <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', }}>
                                            <View>
                                                <Text style={styles.rosterName}>{Newfg[0].firstName} {convertedLastNameFg}</Text>
                                            </View>
                                            <View>
                                                <Text style={styles.rosterPositionFirst}>FG: {fgFinalNa}% &#8226; {teamCodeFg}</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: "center" }}>
                                        <Image
                                            style={styles.userPicTeam}
                                            source={{ uri: teamLogoFg }}
                                        />
                                    </View>
                                </View>
                                {fgLead}
                            </View>
                            <View style={styles.rowPer}>
                                <Text style={styles.monthPer}>3 POINTS MADE</Text>
                            </View>

                            {/* <View style={ styles.row }>
                        <View style={[styles.rowInfo, {borderBottomWidth: 1, borderBottomColor: '#e3e3e3'}]}>
                            <View style={ styles.authorPlayers }>
                                <View style={{flexDirection: 'row',justifyContent: 'flex-start', alignItems: 'center'}}>  
                                    <Text style={[styles.numberOne, {marginRight: 10,}]}>1</Text>
                                 </View>
                                <Image 
                                style={ styles.userPic }
                                source={{ uri: Newthreepm[0].images.photo.T1.url }}
                                />
                                    <View style={{flex: 1,flexDirection: 'column',justifyContent: 'center',}}>        
                                            <View>
                                                <Text style={ styles.rosterName }>{Newthreepm[0].firstName} {convertedLastNameThreePm}</Text>            
                                            </View>
                                            <View>
                                                <Text style={ styles.rosterPositionFirst }>3PM: {Newthreepm[0].value} &#8226; {teamCodeThreePm}</Text>      
                                            </View>                                                                                                               
                                    </View>
                            </View>
                            <View style={{width: 30 + '%',flexDirection: 'row', justifyContent: 'flex-end', alignItems: "center"}}>
                                <Image 
                                    style={ styles.userPicTeam }
                                    source={{ uri: teamLogoThreePm }}
                                />
                            </View>
                        </View>
                        {threepmLead}
                    </View> */}

                            <View style={styles.row}>
                                <View style={[styles.rowInfo, { borderBottomWidth: 1, borderBottomColor: '#e3e3e3' }]}>
                                    <View style={styles.authorPlayers}>
                                        <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                            <Text style={[styles.numberOne, { marginRight: 10, }]}>1</Text>
                                        </View>
                                        <Image
                                            style={styles.userPic}
                                            source={{ uri: Newthreepm[0].images.photo.T1.url }}
                                        />
                                        <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', }}>
                                            <View>
                                                <Text style={styles.rosterName}>{Newthreepm[0].firstName} {convertedLastNameThreePm}</Text>
                                            </View>
                                            <View>
                                                <Text style={styles.rosterPositionFirst}>3PM: {Newthreepm[0].sThreePointersMade} &#8226; {teamCodeThreePm}</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: "center" }}>
                                        <Image
                                            style={styles.userPicTeam}
                                            source={{ uri: teamLogoThreePm }}
                                        />
                                    </View>
                                </View>
                                {threepmLead}
                            </View>

                            <View style={styles.rowPer}>
                                <Text style={styles.monthPer}>3 POINTS PERCENTAGE</Text>
                            </View>
                            <View style={styles.row}>
                                <View style={[styles.rowInfo, { borderBottomWidth: 1, borderBottomColor: '#e3e3e3' }]}>
                                    <View style={styles.authorPlayers}>
                                        <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                            <Text style={[styles.numberOne, { marginRight: 10, }]}>1</Text>
                                        </View>
                                        <Image
                                            style={styles.userPic}
                                            source={{ uri: Newthreepercentage[0].images.photo.T1.url }}
                                        />
                                        <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', }}>
                                            <View>
                                                <Text style={styles.rosterName}>{Newthreepercentage[0].firstName} {convertedLastNameThreePercentage}</Text>
                                            </View>
                                            <View>
                                                <Text style={styles.rosterPositionFirst}>3P%: {threepercentageFinalNa}% &#8226; {teamCodeThreePercentage}</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: "center" }}>
                                        <Image
                                            style={styles.userPicTeam}
                                            source={{ uri: teamLogoThreePercentage }}
                                        />
                                    </View>
                                </View>
                                {threepercentageLead}
                            </View>
                        </View>
                    </View>
                )
            }// end if length > 0
            else {
                <View></View>
            }
        }// end activeindex 0
        if (this.state.activeIndex == 1) {

            if (this.state.dataTeamStats.length > 0) {



                let NewpointsTeams;
                let NewassistsTeams;
                let NewreboundsTeams;
                let NewblocksTeams;
                let NewstealsTeams;
                let NewfgTeams;
                let NewthreepercentageTeams;
                let NewthreepmTeams;

                let newDataTeamsStatsPoints = [];
                let newDataTeamsStatsAssists = [];
                let newDataTeamsStatsRebounds = [];
                let newDataTeamsStatsBlocks = [];
                let newDataTeamsStatsSteals = [];
                let newDataTeamsStatsFg = [];
                let newDataTeamsStatsThreePercentage = [];
                let newDataTeamsStatsThreePm = [];

                for (var i = 0; i < this.state.dataTeamStats.length; i++) {
                    newDataTeamsStatsPoints[i] = this.state.dataTeamStats[i];
                }
                for (var i = 0; i < this.state.dataTeamStats.length; i++) {
                    newDataTeamsStatsAssists[i] = this.state.dataTeamStats[i];
                }
                for (var i = 0; i < this.state.dataTeamStats.length; i++) {
                    newDataTeamsStatsRebounds[i] = this.state.dataTeamStats[i];
                }
                for (var i = 0; i < this.state.dataTeamStats.length; i++) {
                    newDataTeamsStatsBlocks[i] = this.state.dataTeamStats[i];
                }
                for (var i = 0; i < this.state.dataTeamStats.length; i++) {
                    newDataTeamsStatsSteals[i] = this.state.dataTeamStats[i];
                }
                for (var i = 0; i < this.state.dataTeamStats.length; i++) {
                    newDataTeamsStatsFg[i] = this.state.dataTeamStats[i];
                }
                for (var i = 0; i < this.state.dataTeamStats.length; i++) {
                    newDataTeamsStatsThreePercentage[i] = this.state.dataTeamStats[i];
                }
                for (var i = 0; i < this.state.dataTeamStats.length; i++) {
                    newDataTeamsStatsThreePm[i] = this.state.dataTeamStats[i];
                }

                //function sort array api to 
                function sortByKey(array, key) {
                    return array.sort(function (a, b) {
                        var x = parseInt(a[key]); var y = parseInt(b[key]);
                        return ((y < x) ? -1 : ((y > x) ? 1 : 0));
                    });
                }

                //function sort array api to 
                function sortByKeyPercent(array, key) {
                    return array.sort(function (a, b) {
                        var x = parseFloat(a[key]); var y = parseFloat(b[key]);
                        return ((y < x) ? -1 : ((y > x) ? 1 : 0));
                    });
                }



                NewpointsTeams = sortByKey(newDataTeamsStatsPoints, "sPointsAverage");
                NewassistsTeams = sortByKey(newDataTeamsStatsAssists, "sAssistsAverage");
                NewreboundsTeams = sortByKey(newDataTeamsStatsRebounds, "sReboundsTotalAverage");
                NewblocksTeams = sortByKey(newDataTeamsStatsBlocks, "sBlocksAverage");
                NewstealsTeams = sortByKey(newDataTeamsStatsSteals, "sStealsAverage");
                NewfgTeams = sortByKeyPercent(newDataTeamsStatsFg, "sFieldGoalsPercentage");
                NewthreepercentageTeams = sortByKeyPercent(newDataTeamsStatsThreePercentage, "sThreePointersPercentage");
                NewthreepmTeams = sortByKey(newDataTeamsStatsThreePm, "sThreePointersMade");

                let shit = 1;

                let i;
                let j = 1;
                let newArrayPoints = [];

                for (i = 0; i < 4; i++) {
                    newArrayPoints[i] = NewpointsTeams[j];
                    j++;
                }

                let pointsLead = newArrayPoints.map((val, key) => {

                    shit++;

                    let testPpgAllTeams = val.sPointsAverage.toString();
                    let convertedPpgAllTeams;

                    if (testPpgAllTeams.length < 5) {
                        convertedPpgAllTeams = testPpgAllTeams.substring(0, 3);
                    } else if (testPpgAllTeams.length > 4) {
                        convertedPpgAllTeams = testPpgAllTeams.substring(0, 5);
                    } else {
                        convertedPpgAllTeams = testPpgAllTeams.substring(0, 4);
                    }

                    return <View style={styles.rowInfo}>
                        <View style={styles.authorPlayers}>
                            <View style={{ width: 19 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                <Text style={styles.number}>{shit}</Text>
                            </View>
                            <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center' }}>
                                <View>
                                    <Text style={styles.rosterName}>{val.teamName}</Text>
                                </View>
                                <View>
                                    <Text style={styles.rosterPosition}>PPG: {convertedPpgAllTeams}</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: "center" }}>
                            <Image
                                style={styles.userPicTeam}
                                source={{ uri: val.logo.logo.T1.url }}
                            />
                        </View>
                    </View>

                });

                let shitAssists = 1;

                let iAssists;
                let jAssists = 1;
                let newArrayAssists = [];

                for (iAssists = 0; iAssists < 4; iAssists++) {
                    newArrayAssists[iAssists] = NewassistsTeams[jAssists];
                    jAssists++;
                }


                let assistsLead = newArrayAssists.map((val, key) => {

                    shitAssists++;

                    let testSpgAllTeams = val.sAssistsAverage.toString();
                    let convertedSpgAllTeams;

                    if (testSpgAllTeams.length < 5) {
                        convertedSpgAllTeams = testSpgAllTeams.substring(0, 3);
                    } else {
                        convertedSpgAllTeams = testSpgAllTeams.substring(0, 4);
                    }

                    return <View style={styles.rowInfo}>
                        <View style={styles.authorPlayers}>
                            <View style={{ width: 19 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                <Text style={styles.number}>{shitAssists}</Text>
                            </View>
                            <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center' }}>
                                <View>
                                    <Text style={styles.rosterName}>{val.teamName}</Text>
                                </View>
                                <View>
                                    <Text style={styles.rosterPosition}>APG: {convertedSpgAllTeams}</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: "center" }}>
                            <Image
                                style={styles.userPicTeam}
                                source={{ uri: val.logo.logo.T1.url }}
                            />
                        </View>
                    </View>
                });

                let shitRebounds = 1;

                let iRebounds;
                let jRebounds = 1;
                let newArrayRebounds = [];

                for (iRebounds = 0; iRebounds < 4; iRebounds++) {
                    newArrayRebounds[iRebounds] = NewreboundsTeams[jRebounds];
                    jRebounds++;
                }

                let reboundsLead = newArrayRebounds.map((val, key) => {

                    shitRebounds++;

                    let testRpgAllTeams = val.sReboundsTotalAverage.toString();
                    let convertedRpgAllTeams;

                    if (testRpgAllTeams.length < 5) {
                        convertedRpgAllTeams = testRpgAllTeams.substring(0, 3);
                    } else {
                        convertedRpgAllTeams = testRpgAllTeams.substring(0, 4);
                    }

                    return <View style={styles.rowInfo}>
                        <View style={styles.authorPlayers}>
                            <View style={{ width: 19 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                <Text style={styles.number}>{shitRebounds}</Text>
                            </View>
                            <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center' }}>
                                <View>
                                    <Text style={styles.rosterName}>{val.teamName}</Text>
                                </View>
                                <View>
                                    <Text style={styles.rosterPosition}>RPG: {val.sReboundsTotalAverage}</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: "center" }}>
                            <Image
                                style={styles.userPicTeam}
                                source={{ uri: val.logo.logo.T1.url }}
                            />
                        </View>
                    </View>
                });

                let shitBlocks = 1;

                let iBlocks;
                let jBlocks = 1;
                let newArrayBlocks = [];

                for (iBlocks = 0; iBlocks < 4; iBlocks++) {
                    newArrayBlocks[iBlocks] = NewblocksTeams[jBlocks];
                    jBlocks++;
                }


                let blocksLead = newArrayBlocks.map((val, key) => {


                    shitBlocks++;

                    let testBpgAllTeams = val.sBlocksAverage.toString();
                    let convertedBpgAllTeams;

                    if (testBpgAllTeams.length < 5) {
                        convertedBpgAllTeams = testBpgAllTeams.substring(0, 3);
                    } else {
                        convertedBpgAllTeams = testBpgAllTeams.substring(0, 4);
                    }

                    return <View style={styles.rowInfo}>
                        <View style={styles.authorPlayers}>
                            <View style={{ width: 19 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                <Text style={styles.number}>{shitBlocks}</Text>
                            </View>
                            <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center' }}>
                                <View>
                                    <Text style={styles.rosterName}>{val.teamName}</Text>
                                </View>
                                <View>
                                    <Text style={styles.rosterPosition}>BPG: {convertedBpgAllTeams}</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: "center" }}>
                            <Image
                                style={styles.userPicTeam}
                                source={{ uri: val.logo.logo.T1.url }}
                            />
                        </View>
                    </View>
                });

                let shitSteals = 1;

                let iSteals;
                let jSteals = 1;
                let newArraySteals = [];

                for (iSteals = 0; iSteals < 4; iSteals++) {
                    newArraySteals[iSteals] = NewstealsTeams[jSteals];
                    jSteals++;
                }

                let stealsLead = newArraySteals.map((val, key) => {

                    shitSteals++;

                    let testSpgAllTeams = val.sStealsAverage.toString();
                    let convertedSpgAllTeams;

                    if (testSpgAllTeams.length < 5) {
                        convertedSpgAllTeams = testSpgAllTeams.substring(0, 3);
                    } else {
                        convertedSpgAllTeams = testSpgAllTeams.substring(0, 4);
                    }

                    return <View style={styles.rowInfo}>
                        <View style={styles.authorPlayers}>
                            <View style={{ width: 19 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                <Text style={styles.number}>{shitSteals}</Text>
                            </View>
                            <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center' }}>
                                <View>
                                    <Text style={styles.rosterName}>{val.teamName}</Text>
                                </View>
                                <View>
                                    <Text style={styles.rosterPosition}>SPG: {convertedSpgAllTeams}</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: "center" }}>
                            <Image
                                style={styles.userPicTeam}
                                source={{ uri: val.logo.logo.T1.url }}
                            />
                        </View>
                    </View>
                });

                let shitFg = 1;

                let iFg;
                let jFg = 1;
                let newArrayFg = [];

                for (iFg = 0; iFg < 4; iFg++) {
                    newArrayFg[iFg] = NewfgTeams[jFg];
                    jFg++;
                }


                let fgLead = newArrayFg.map((val, key) => {

                    shitFg++;

                    let fgFinal;
                    let fgFinalNa;

                    let postFg = val.sFieldGoalsPercentage.toString();           

                    if (postFg.length == 1) {
                        fgFinalNa = postFg + '00';
                    } else {           
    
                        fgFinal = postFg.substring(2, 4);
    
                        if (fgFinal.length == 1) {
                            fgFinalNa = fgFinal + '0';
                        } else {
                            fgFinalNa = fgFinal;
                        }
                    }

                    return <View style={styles.rowInfo}>
                        <View style={styles.authorPlayers}>
                            <View style={{ width: 19 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                <Text style={styles.number}>{shitFg}</Text>
                            </View>
                            <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center' }}>
                                <View>
                                    <Text style={styles.rosterName}>{val.teamName}</Text>
                                </View>
                                <View>
                                    <Text style={styles.rosterPosition}>FG: {fgFinalNa}%</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: "center" }}>
                            <Image
                                style={styles.userPicTeam}
                                source={{ uri: val.logo.logo.T1.url }}
                            />
                        </View>
                    </View>
                });

                let postThreePercentage = NewthreepercentageTeams[0].sThreePointersPercentage.toString();
                let threepercentageFinal = postThreePercentage.substring(2, 4);
                let threepercentageFinalNa;

                if (threepercentageFinal.length == 1) {
                    threepercentageFinalNa = threepercentageFinal + '0';
                } else {
                    threepercentageFinalNa = threepercentageFinal;
                }


                let shitThreePercentage = 1;

                let iThreePercentage;
                let jThreePercentage = 1;
                let newArrayThreePercentage = [];

                for (iThreePercentage = 0; iThreePercentage < 4; iThreePercentage++) {
                    newArrayThreePercentage[iThreePercentage] = NewthreepercentageTeams[jThreePercentage];
                    jThreePercentage++;
                }

                let threepercentageLead = newArrayThreePercentage.map((val, key) => {

                    shitThreePercentage++;


                    let postThreePercentage = val.sThreePointersPercentage.toString();
                    let threepercentageFinal = postThreePercentage.substring(2, 4);
                    let threepercentageFinalNa;

                    if (threepercentageFinal.length == 1) {
                        threepercentageFinalNa = threepercentageFinal + '0';
                    } else {
                        threepercentageFinalNa = threepercentageFinal;
                    }

                    return <View style={styles.rowInfo}>
                        <View style={styles.authorPlayers}>
                            <View style={{ width: 19 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                <Text style={styles.number}>{shitThreePercentage}</Text>
                            </View>
                            <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center' }}>
                                <View>
                                    <Text style={styles.rosterName}>{val.teamName}</Text>
                                </View>
                                <View>
                                    <Text style={styles.rosterPosition}>3P%: {threepercentageFinalNa}%</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: "center" }}>
                            <Image
                                style={styles.userPicTeam}
                                source={{ uri: val.logo.logo.T1.url }}
                            />
                        </View>
                    </View>
                });

                let postFg = NewfgTeams[0].sFieldGoalsPercentage.toString();
                let fgFinal = postFg.substring(2, 4);
                let fgFinalNa;

                if (fgFinal.length == 1) {
                    fgFinalNa = fgFinal + '0';
                } else {
                    fgFinalNa = fgFinal;
                }

                let shitThreePm = 1;

                let iThreePm;
                let jThreePm = 1;
                let newArrayThreePm = [];

                for (iThreePm = 0; iThreePm < 4; iThreePm++) {
                    newArrayThreePm[iThreePm] = NewthreepmTeams[jThreePm];
                    jThreePm++;
                }

                let threepmLead = newArrayThreePm.map((val, key) => {

                    shitThreePm++;


                    return <View style={styles.rowInfo}>
                        <View style={styles.authorPlayers}>
                            <View style={{ width: 19 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                <Text style={styles.number}>{shitThreePm}</Text>
                            </View>
                            <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center' }}>
                                <View>
                                    <Text style={styles.rosterName}>{val.teamName}</Text>
                                </View>
                                <View>
                                    <Text style={styles.rosterPosition}>3PM: {val.sThreePointersMade}%</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: "center" }}>
                            <Image
                                style={styles.userPicTeam}
                                source={{ uri: val.logo.logo.T1.url }}
                            />
                        </View>
                    </View>
                });

                let testPpgOneTeams = NewpointsTeams[0].sPointsAverage.toString();
                let convertedPpgOneTeams;

                if (testPpgOneTeams.length < 5) {
                    convertedPpgOneTeams = testPpgOneTeams.substring(0, 3);
                } else if (testPpgOneTeams.length > 4) {
                    convertedPpgOneTeams = testPpgOneTeams.substring(0, 5);
                } else {
                    convertedPpgOneTeams = testPpgOneTeams.substring(0, 4);
                }

                let testApgOneTeams = NewassistsTeams[0].sAssistsAverage.toString();
                let convertedApgOneTeams;

                if (testApgOneTeams.length < 5) {
                    convertedApgOneTeams = testApgOneTeams.substring(0, 3);
                } else {
                    convertedApgOneTeams = testApgOneTeams.substring(0, 4);
                }

                let testRpgOneTeams = NewreboundsTeams[0].sReboundsTotalAverage.toString();
                let convertedRpgOneTeams;

                if (testRpgOneTeams.length < 5) {
                    convertedRpgOneTeams = testRpgOneTeams.substring(0, 3);
                } else {
                    convertedRpgOneTeams = testRpgOneTeams.substring(0, 4);
                }

                let testBpgOneTeams = NewblocksTeams[0].sBlocksAverage.toString();
                let convertedBpgOneTeams;

                if (testBpgOneTeams.length < 5) {
                    convertedBpgOneTeams = testBpgOneTeams.substring(0, 3);
                } else {
                    convertedBpgOneTeams = testBpgOneTeams.substring(0, 4);
                }

                let testSpgOneTeams = NewstealsTeams[0].sStealsAverage.toString();
                let convertedSpgOneTeams;

                if (testSpgOneTeams.length < 5) {
                    convertedSpgOneTeams = testSpgOneTeams.substring(0, 3);
                } else {
                    convertedSpgOneTeams = testSpgOneTeams.substring(0, 4);
                }

                return (
                    <View style={{ backgroundColor: '#e3e3e3', }}>
                        <View style={styles.group}>
                            <View style={styles.rowPer}>
                                <Text style={styles.monthPer}>POINTS PER GAME</Text>
                            </View>
                            <View style={styles.row}>
                                <View style={[styles.rowInfo, { borderBottomWidth: 1, borderBottomColor: '#e3e3e3' }]}>
                                    <View style={styles.authorPlayers}>
                                        <View style={{ width: 19 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                            <Text style={styles.numberOne}>1</Text>
                                        </View>
                                        <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', }}>
                                            <View>
                                                <Text style={styles.rosterName}>{NewpointsTeams[0].teamName}</Text>
                                            </View>
                                            <View>
                                                <Text style={styles.rosterPositionFirst}>PPG: {convertedPpgOneTeams}</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: "center" }}>
                                        <Image
                                            style={styles.userPicTeam}
                                            source={{ uri: NewpointsTeams[0].logo.logo.T1.url }}
                                        />
                                    </View>
                                </View>
                                {pointsLead}
                            </View>
                            <View style={styles.rowPer}>
                                <Text style={styles.monthPer}>ASSISTS PER GAME</Text>
                            </View>
                            <View style={styles.row}>
                                <View style={[styles.rowInfo, { borderBottomWidth: 1, borderBottomColor: '#e3e3e3' }]}>
                                    <View style={styles.authorPlayers}>
                                        <View style={{ width: 19 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                            <Text style={styles.numberOne}>1</Text>
                                        </View>
                                        <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', }}>
                                            <View>
                                                <Text style={styles.rosterName}>{NewassistsTeams[0].teamName}</Text>
                                            </View>
                                            <View>
                                                <Text style={styles.rosterPositionFirst}>APG: {convertedApgOneTeams}</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: "center" }}>
                                        <Image
                                            style={styles.userPicTeam}
                                            source={{ uri: NewassistsTeams[0].logo.logo.T1.url }}
                                        />
                                    </View>
                                </View>
                                {assistsLead}
                            </View>
                            <View style={styles.rowPer}>
                                <Text style={styles.monthPer}>REBOUNDS PER GAME</Text>
                            </View>
                            <View style={styles.row}>
                                <View style={[styles.rowInfo, { borderBottomWidth: 1, borderBottomColor: '#e3e3e3' }]}>
                                    <View style={styles.authorPlayers}>
                                        <View style={{ width: 19 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                            <Text style={styles.numberOne}>1</Text>
                                        </View>
                                        <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', }}>
                                            <View>
                                                <Text style={styles.rosterName}>{NewreboundsTeams[0].teamName}</Text>
                                            </View>
                                            <View>
                                                <Text style={styles.rosterPositionFirst}>RPG: {convertedRpgOneTeams}</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: "center" }}>
                                        <Image
                                            style={styles.userPicTeam}
                                            source={{ uri: NewreboundsTeams[0].logo.logo.T1.url }}
                                        />
                                    </View>
                                </View>
                                {reboundsLead}
                            </View>
                            <View style={styles.rowPer}>
                                <Text style={styles.monthPer}>BLOCKS PER GAME</Text>
                            </View>
                            <View style={styles.row}>
                                <View style={[styles.rowInfo, { borderBottomWidth: 1, borderBottomColor: '#e3e3e3' }]}>
                                    <View style={styles.authorPlayers}>
                                        <View style={{ width: 19 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                            <Text style={styles.numberOne}>1</Text>
                                        </View>
                                        <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', }}>
                                            <View>
                                                <Text style={styles.rosterName}>{NewblocksTeams[0].teamName}</Text>
                                            </View>
                                            <View>
                                                <Text style={styles.rosterPositionFirst}>BPG: {convertedBpgOneTeams}</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: "center" }}>
                                        <Image
                                            style={styles.userPicTeam}
                                            source={{ uri: NewblocksTeams[0].logo.logo.T1.url }}
                                        />
                                    </View>
                                </View>
                                {blocksLead}
                            </View>
                            <View style={styles.rowPer}>
                                <Text style={styles.monthPer}>STEALS PER GAME</Text>
                            </View>
                            <View style={styles.row}>
                                <View style={[styles.rowInfo, { borderBottomWidth: 1, borderBottomColor: '#e3e3e3' }]}>
                                    <View style={styles.authorPlayers}>
                                        <View style={{ width: 19 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                            <Text style={styles.numberOne}>1</Text>
                                        </View>
                                        <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', }}>
                                            <View>
                                                <Text style={styles.rosterName}>{NewstealsTeams[0].teamName}</Text>
                                            </View>
                                            <View>
                                                <Text style={styles.rosterPositionFirst}>SPG: {convertedSpgOneTeams}</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: "center" }}>
                                        <Image
                                            style={styles.userPicTeam}
                                            source={{ uri: NewstealsTeams[0].logo.logo.T1.url }}
                                        />
                                    </View>
                                </View>
                                {stealsLead}
                            </View>
                            <View style={styles.rowPer}>
                                <Text style={styles.monthPer}>FIELD GOAL PERCENTAGE</Text>
                            </View>
                            <View style={styles.row}>
                                <View style={[styles.rowInfo, { borderBottomWidth: 1, borderBottomColor: '#e3e3e3' }]}>
                                    <View style={styles.authorPlayers}>
                                        <View style={{ width: 19 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                            <Text style={styles.numberOne}>1</Text>
                                        </View>
                                        <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', }}>
                                            <View>
                                                <Text style={styles.rosterName}>{NewfgTeams[0].teamName}</Text>
                                            </View>
                                            <View>
                                                <Text style={styles.rosterPositionFirst}>FG: {fgFinalNa}</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: "center" }}>
                                        <Image
                                            style={styles.userPicTeam}
                                            source={{ uri: NewfgTeams[0].logo.logo.T1.url }}
                                        />
                                    </View>
                                </View>
                                {fgLead}
                            </View>
                            <View style={styles.rowPer}>
                                <Text style={styles.monthPer}>3 POINTS MADE</Text>
                            </View>
                            <View style={styles.row}>
                                <View style={[styles.rowInfo, { borderBottomWidth: 1, borderBottomColor: '#e3e3e3' }]}>
                                    <View style={styles.authorPlayers}>
                                        <View style={{ width: 19 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                            <Text style={styles.numberOne}>1</Text>
                                        </View>
                                        <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', }}>
                                            <View>
                                                <Text style={styles.rosterName}>{NewthreepmTeams[0].teamName}</Text>
                                            </View>
                                            <View>
                                                <Text style={styles.rosterPositionFirst}>3PM: {NewthreepmTeams[0].sThreePointersMade}</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: "center" }}>
                                        <Image
                                            style={styles.userPicTeam}
                                            source={{ uri: NewthreepmTeams[0].logo.logo.T1.url }}
                                        />
                                    </View>
                                </View>
                                {threepmLead}
                            </View>

                            <View style={styles.rowPer}>
                                <Text style={styles.monthPer}>3 POINTS PERCENTAGE</Text>
                            </View>
                            <View style={styles.row}>
                                <View style={[styles.rowInfo, { borderBottomWidth: 1, borderBottomColor: '#e3e3e3' }]}>
                                    <View style={styles.authorPlayers}>
                                        <View style={{ width: 19 + '%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                            <Text style={styles.numberOne}>1</Text>
                                        </View>
                                        <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', }}>
                                            <View>
                                                <Text style={styles.rosterName}>{NewthreepercentageTeams[0].teamName}</Text>
                                            </View>
                                            <View>
                                                <Text style={styles.rosterPositionFirst}>3P%: {threepercentageFinalNa}%</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ width: 20 + '%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: "center" }}>
                                        <Image
                                            style={styles.userPicTeam}
                                            source={{ uri: NewthreepercentageTeams[0].logo.logo.T1.url }}
                                        />
                                    </View>
                                </View>
                                {threepercentageLead}
                            </View>
                        </View>
                    </View>
                )
            }
        }// end if  legnth > 0
        else {
            <View></View>
        }
    }// end activeindex 1

    render() {

        const shadowStyle = {
            shadowOffset: { width: 10, height: 10, },
            shadowColor: 'black',
            shadowOpacity: 1.0,
        }

        if (this.state.isLoading) {
            return (
                <View style={styles.containerLoading}>
                    <ActivityIndicator size="large" color="#fff" />
                    <View>
                    </View>
                </View>
            )
        } else {



            return (

                <ScrollView>
                    <View style={styles.group}>
                        <View style={{ flexDirection: 'row', paddingBottom: 20, paddingTop: 30, backgroundColor: '#e3e3e3' }}>
                            <View style={{ width: 50 + '%', flexDirection: 'row', justifyContent: 'flex-end', paddingRight: 5 }}>
                                <TouchableOpacity onPress={() => this.segmentClicked(0)} active={this.state.activeIndex == 0}>
                                    <Text style={{ backgroundColor: '#EE3026', color: '#fff', padding: 10, fontWeight: '700', borderRadius: 2 }}>PLAYER LEADERS</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ width: 50 + '%', flexDirection: 'row', justifyContent: 'flex-start', paddingLeft: 5 }}>
                                <TouchableOpacity onPress={() => this.segmentClicked(1)} active={this.state.activeIndex == 1}>
                                    <Text style={{ backgroundColor: '#EE3026', color: '#fff', padding: 10, fontWeight: '700', borderRadius: 2 }}>TEAM LEADERS</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        {this.renderSection()}
                    </View>


                </ScrollView>
            )
        }//end else
    }// end render
}
const styles = StyleSheet.create({
    group: {
        backgroundColor: '#e3e3e3',
    },
    rowInfoTabs: {
        flexDirection: 'row',
        paddingLeft: 15,
        paddingRight: 15,
        paddingBottom: 5,
        paddingTop: 5,
        borderBottomWidth: 1,
        borderBottomColor: '#e3e3e3'
    },
    number: {
        paddingLeft: 15,
    },
    numberOne: {
        paddingLeft: 15,
        fontWeight: '700',
    },
    monthPer: {
        textAlign: 'left',
        fontWeight: '700',
        color: '#1f1f1f',
        fontSize: 18
    },
    rowPer: {
        padding: 15,
    },
    container: {
        flex: 1,
        backgroundColor: '#e3e3e3',
    },
    row: {
        backgroundColor: 'white',
        marginBottom: 7,
        elevation: 2,
        shadowOffset: { width: 2, height: 2 },
        shadowColor: "grey",
        shadowOpacity: 0.5,
        shadowRadius: 2,
    },
    rosterPosition: {
        color: '#999',
    },
    rosterPositionFirst: {
        color: '#999',
        fontWeight: '700',
    },
    containerLoading: {
        flex: 1,
        backgroundColor: '#000000',
        justifyContent: 'center',
    },
    rosterName: {
        color: '#000',
        fontWeight: '700',
    },
    teamNick: {
        color: '#000',
        fontWeight: '700',
    },
    author: {
        flexDirection: "row",
        alignItems: "center",
        width: 50 + '%',
    },
    authorPlayers: {
        flexDirection: "row",
        alignItems: "center",
        width: 80 + '%',
    },
    userPic: {
        height: 40,
        width: 40,
        borderRadius: 40,
        marginRight: 10,
        borderRadius: 20,
    },
    userPicTeam: {
        height: 40,
        width: 40,
        marginRight: 10,
    },
    playerPic: {
        width: 40,
        height: 40,
        borderRadius: 20,
    },
    leadersScore: {
        fontSize: 18,
        color: '#1f1f1f'
    },
    subLabel: {
        fontSize: 15,
        fontWeight: '700',
        color: '#999'
    },
    recordsScore: {
        color: '#1f1f1f'
    },
    recordsLabel: {
        color: '#1f1f1f',
        fontWeight: '700'
    },
    rowLabel: {
        flexDirection: 'row',
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 10,
    },
    rowInfo: {
        flexDirection: 'row',
        paddingLeft: 15,
        paddingRight: 15,
        paddingBottom: 10,
        paddingTop: 10,
    },
    value: {
        color: '#999'
    },
    infoLabel: {
        color: '#999',
        fontWeight: '700'
    },
    label: {
        fontWeight: '700',
        color: '#1f1f1f',
        fontSize: 18
    },
    standing: {
        color: '#fff'
    },
    teamName: {
        color: '#ffffff',
        fontSize: 18,
        fontWeight: '700'
    },
    linearGradient: {
        backgroundColor: "transparent",
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0
    },
    teamPic: {
        width: 80,
        height: 80
    }
});