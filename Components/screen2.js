import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, ImageBackground, ScrollView, Image, StatusBar, TouchableOpacity, ActivityIndicator, FlatList, TouchableHighlight, RefreshControl, Button } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import moment from 'moment';
import groupBy from 'json-groupby';

export default class screen2 extends Component {

  static navigationOptions = ({ navigation }) => ({
    headerBackground: (
      <Image
        style={{ width: '100%', height: '100%' }}
        source={require('./src/bg-header.jpg')}
      />
    ),
    headerStyle: {
      backgroundColor: 'transparent',
      ...Platform.select({
        ios: {

        },
        android: {
          // height: 80,
          // paddingTop: 20
        },
      }),
    },
    headerTintColor: '#fff',
    title: 'GAMES',
    headerTitleStyle: {
      fontWeight: 'bold',
      fontSize: 18,
      textAlign: 'center',
      flex: 1,
      ...Platform.select({
        ios: {

        },
        android: {
          // marginTop: 200,
        },
      }),
    },
  });

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      dataSource: [],
      allTimeInfo: [],
      opening: false,
      showErrorPage: false,
      isRefreshing: false,
    }
  }

  async componentDidMount() {
    this.getData();
    this.props.navigation.setParams({
      scrollToTop: this.goToTop.bind(this),
    });
  }



  getData = async () => {

    let comid = 26702;

    var request_1_url = 'https://api.wh.geniussports.com/v1/basketball/competitions/' + comid + '/matcheslive?ak=eebd8ae256142ac3fd24bd2003d28782&fields=teamId,matchId,matchNumber,competitors.competitorId,matchTime,live,matchStatus,competitors.scoreString,competitors.images,competitors.teamNickname,venue&limit=150';
    var request_2_url = 'https://api.wh.geniussports.com/v1/basketball/competitions/' + comid + '/standings?ak=eebd8ae256142ac3fd24bd2003d28782&fields=competitorId,won,lost';

    return fetch(request_1_url).then((response) => response.json()).then((responseJson) => {
      let people;
      function sortByKey(array, key) {
        return array.sort(function (a, b) {
          var x = a[key]; var y = b[key];
          return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        });
      }
      people = sortByKey(responseJson.response.data, 'matchNumber');

      this.setState({
        dataSource: people,
        isRefreshing: false,
      });
    }).then(() => {
      return fetch(request_2_url).then((response) => response.json()).then((responseJson) => {
        this.setState({
          isLoading: false,
          isRefreshing: false,
          allTimeInfo: responseJson.response.data,
        });
      }).catch((error) => {
        console.error(error);
        this.setState({
          isLoading: false,
          isRefreshing: false,
          showErrorPage: true,
        });
      });
    }).catch((error) => {
      console.error(error);
      this.setState({
        isLoading: false,
        isRefreshing: false,
        showErrorPage: true,
      });
    });
  }

  _open(item) {
    if (!this.state.opening) {
      this.props.navigation.push('GameInfo', { matchstatus: item.matchStatus, matchid: item.matchId, matchtime: item.matchTime, lefteam: item.competitors[0].competitorId, rightteam: item.competitors[1].competitorId })
      this.setState({ opening: true })
      setTimeout(() => {
        this.setState({ opening: false })
      }, 1000)
    }
  }


  renderRow = ({ item, index }) => {

    let standingLeft;
    let standingRight;

    let standings = this.state.allTimeInfo.map((ey, key) => {
      if (item.competitors[0].competitorId == ey.competitorId) {
        standingLeft = ey.won + '-' + ey.lost;
      }
      if (item.competitors[1].competitorId == ey.competitorId) {
        standingRight = ey.won + '-' + ey.lost;
      }
    });

    let gameStatus;
    let gameStatusPm;
    let scoreLeft;
    let scoreRight;
    let scoreFinalLeft;
    let scoreFinalRight;

    let scoreZero;
    let scoreOne;
    let liveText;

    let scoreZeroFinal;
    let scoreOneFinal;
    let rightSide;

    if (item.matchStatus == 'SCHEDULED') {
      gameStatus = <Text style={styles.finalGray}>{moment(item.matchTime).format('h:mm')}</Text>;
      gameStatusPm = <Text style={styles.finalGray}>PM</Text>;
      liveText = <Text style={styles.date}>{moment(item.matchTime).format('ddd - MMM D').toUpperCase()} - {gameStatus} {gameStatusPm}</Text>;

      rightSide = <View style={{ justifyContent: 'center', alignItems: 'center', width: 100, height: 20 }}>
        <Icon name="chevron-right" size={25} />
      </View>;

      scoreFinalLeft = 0;
      scoreFinalRight = 0;
    }

    if (item.matchStatus == 'IN_PROGRESS') {
      liveText = <Text style={styles.dateLive}>Live &#8226; {moment(item.matchTime).format('ddd - MMM D').toUpperCase()} - {gameStatus} {gameStatusPm}</Text>;
      let currentTime = item.live.clock.substring(0, 5);
      gameStatus = <Text style={styles.finalBtn}>{currentTime}</Text>;
      if (item.live.periodType == "REGULAR") {
        gameStatusPm = <Text style={styles.live}>Q{item.live.period}</Text>;
      }
      if (item.live.periodType == "OVERTIME") {
        gameStatusPm = <Text style={styles.live}>OT{item.live.period}</Text>;
      }

      rightSide = <View style={{ justifyContent: 'center', alignItems: 'center', width: 100, height: 20 }}>
        {gameStatus}
      </View>;

      
      var one = 1;
      var two = 2;

      scoreFinalLeft = item.live.scores[1];
      scoreFinalRight = item.live.scores[2];

    }

    if (item.matchStatus == 'COMPLETE' || item.matchStatus == 'FINISHED') {
      liveText = <Text style={styles.date}>{moment(item.matchTime).format('ddd - MMM D').toUpperCase()}</Text>;
      gameStatus = <Text style={styles.finalBtn}>FINAL</Text>;
      gameStatusPm = <Text style={styles.final}></Text>;

      rightSide = <View style={{ justifyContent: 'center', alignItems: 'center', width: 100, height: 20 }}>
        {gameStatus}
        {gameStatusPm}
      </View>;

      scoreLeft = parseInt(item.competitors[0].scoreString);
      scoreRight = parseInt(item.competitors[1].scoreString);

      if (scoreLeft > scoreRight) {
        scoreFinalLeft = <Text style={{ color: 'black' }}>{item.competitors[0].scoreString}</Text>;
        scoreFinalRight = <Text style={{ color: '#999' }}>{item.competitors[1].scoreString}</Text>;
      } else {
        scoreFinalRight = <Text style={{ color: 'black' }}>{item.competitors[1].scoreString}</Text>;
        scoreFinalLeft = <Text style={{ color: '#999' }}>{item.competitors[0].scoreString}</Text>;
      }

      if (item.competitors[0].scoreString != '') {
        scoreZero = item.competitors[0].scoreString;
      } else {
        scoreZero = 0;
      }

      if (item.competitors[1].scoreString != '') {
        scoreOne = item.competitors[1].scoreString;
      } else {
        scoreOne = 0;
      }
    }

    if (index === 0) {

      return <View>
        <View style={styles.row}>
          <Text style={styles.month}>REGULAR SEASON</Text>
        </View>

        <TouchableOpacity onPress={this._open.bind(this, item)}>
          <View style={styles.card}>
            <View style={{ flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: '#e3e3e3' }}>
              {liveText}
            </View>
            <View style={{ flexDirection: 'row', paddingTop: 10 }}>
              <View style={{ width: 70 + '%' }}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ width: 70 + '%', height: 50 }}>
                    <View style={styles.author}>
                      <Image
                        style={styles.userPic}
                        source={{
                          uri: item.competitors[0].images.logo.T1.url
                        }}
                      />
                      <Text style={styles.team}>{item.competitors[0].teamNickname}</Text>
                    </View>
                  </View>
                  <View style={{ flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', width: 25 + '%', height: 40 }}>
                    <Text styles={styles.standing}>({standingLeft})</Text>
                    <Text style={styles.score}>{scoreFinalLeft}</Text>
                  </View>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ width: 70 + '%', height: 50 }}>
                    <View style={styles.author}>
                      <Image
                        style={styles.userPic}
                        source={{
                          uri: item.competitors[1].images.logo.T1.url
                        }}
                      />
                      <Text style={styles.team}>{item.competitors[1].teamNickname}</Text>
                    </View>
                  </View>
                  <View style={{ flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', width: 25 + '%', height: 40 }}>
                    <Text styles={styles.standing}>({standingRight})</Text>
                    <Text style={styles.score}>{scoreFinalRight}</Text>
                  </View>
                </View>
              </View>
              <View style={{ width: 30 + '%' }}>
                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                  <View style={{ width: 50, height: 15 }} />
                  {rightSide}
                </View>
              </View>
            </View>
            <View style={{ flexDirection: 'row', borderTopWidth: 1, borderTopColor: '#e3e3e3' }}>
              <Text style={styles.venue}>{item.venue.venueName}, {item.venue.surfaceName}</Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    }

    return <TouchableOpacity onPress={this._open.bind(this, item)}>
      <View style={styles.card}>
        <View style={{ flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: '#e3e3e3' }}>
          {liveText}
        </View>
        <View style={{ flexDirection: 'row', paddingTop: 10 }}>
          <View style={{ width: 70 + '%' }}>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ width: 70 + '%', height: 50 }}>
                <View style={styles.author}>
                  <Image
                    style={styles.userPic}
                    source={{
                      uri: item.competitors[0].images.logo.T1.url
                    }}
                  />
                  <Text style={styles.team}>{item.competitors[0].teamNickname}</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', width: 25 + '%', height: 40 }}>
                <Text styles={styles.standing}>({standingLeft})</Text>
                <Text style={styles.score}>{scoreFinalLeft}</Text>
              </View>
            </View>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ width: 70 + '%', height: 50 }}>
                <View style={styles.author}>
                  <Image
                    style={styles.userPic}
                    source={{
                      uri: item.competitors[1].images.logo.T1.url
                    }}
                  />
                  <Text style={styles.team}>{item.competitors[1].teamNickname}</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', width: 25 + '%', height: 40 }}>
                <Text styles={styles.standing}>({standingRight})</Text>
                <Text style={styles.score}>{scoreFinalRight}</Text>
              </View>
            </View>
          </View>
          <View style={{ width: 30 + '%' }}>
            <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
              <View style={{ width: 50, height: 15 }} />
              {rightSide}
            </View>
          </View>
        </View>
        <View style={{ flexDirection: 'row', borderTopWidth: 1, borderTopColor: '#e3e3e3' }}>
          <Text style={styles.venue}>{item.venue.venueName}, {item.venue.surfaceName}</Text>
        </View>
      </View>
    </TouchableOpacity>
  }

  goToTop = () => {
    this.flatListRef.scrollToOffset({ animated: true, offset: 0 });
  }

  _onRefresh = () => {
    this.setState({ isRefreshing: true, }, this.getData);
  }

  _onRefreshPage = () => {
    this.setState({ isRefreshing: true, showErrorPage: false }, this.getData);
  }

  render() {

    if (this.state.showErrorPage) {
      return (
        <View style={{ flex: 1, justifyContent: "center", alignItems: "center", paddingHorizontal: 25, backgroundColor: 'white' }}>
          <Image
            style={{ width: 80, height: 80, marginBottom: 15, }}
            source={require('./src/antenna.png')}
          />
          <Text style={styles.errorTitle}>No Internet Connection</Text>
          <Text style={styles.errorText}>An error has occurred. Try checking your network settings</Text>
          <Button
            onPress={this._onRefreshPage.bind(this)}
            title="Try Again"
            color="#CE1A19"
          />
        </View>
      )
    }

    const { navigate } = this.props.navigation

    if (this.state.isLoading) {
      return (
        <View style={styles.containerLoading}>
          <ActivityIndicator size="large" color="#fff" />
        </View>
      )
    } else {

      return (
        <View style={styles.container}>
          <FlatList
            style={styles.container}
            data={this.state.dataSource}
            renderItem={this.renderRow}
            keyExtractor={(item, index) => index.toString()}
            ref={(ref) => { this.flatListRef = ref; }}
            scrollEventThrottle={16}
            refreshControl={
              <RefreshControl
                refreshing={this.state.isRefreshing}
                onRefresh={this._onRefresh}
              />
            }
          />
        </View>
      )
    }
  }
}


const styles = StyleSheet.create({
  errorTitle: {
    fontWeight: '700',
    fontSize: 20,
    textAlign: 'center',
    color: '#626671',
  },
  errorText: {
    textAlign: 'center',
    paddingBottom: 10,
    color: '#626671',
  },
  redHa: {
    color: 'red',
  },
  blueHa: {
    color: 'blue',
  },
  containerLoading: {
    flex: 1,
    backgroundColor: '#000000',
    justifyContent: 'center',
  },
  container: {
    flex: 1,
    backgroundColor: '#e3e3e3',
  },
  headerText: {
    ...Platform.select({
      ios: {
        paddingBottom: 15,
        paddingTop: 30,
      },
      android: {
        padding: 17,
        paddingTop: 30,
      },
    }),
    fontSize: 18,
    color: 'white',
    fontWeight: '700',
    textAlign: 'center',
  },
  header: {
    backgroundColor: '#1f1f1f',
    alignItems: 'center',
    justifyContent: 'center',
  },
  row: {
    padding: 15,
  },
  month: {
    textAlign: 'left',
    fontWeight: '700',
    color: '#1f1f1f',
    fontSize: 18
  },
  card: {
    backgroundColor: '#fff',
    marginBottom: 7,
    paddingLeft: 15,
    paddingRight: 15,
    // paddingTop: 10,
    elevation: 2,
    shadowOffset: { width: 2, height: 2 },
    shadowColor: "grey",
    shadowOpacity: 0.5,
    shadowRadius: 2,
  },
  final: {
    ...Platform.select({
      ios: {
        paddingTop: 2,
      },
      android: {
      },
    }),
    color: '#1f1f1f',
    fontWeight: '700',
    fontSize: 13,
    justifyContent: 'center',
  },
  finalGray: {
    ...Platform.select({
      ios: {
        paddingTop: 2,
      },
      android: {
      },
    }),
    color: '#000',
    fontWeight: '700',
    fontSize: 13,
    justifyContent: 'center',
  },
  finalBtn: {
    color: '#EE3026',
    fontWeight: '700',
    fontSize: 13,
    justifyContent: 'center',
    borderRadius: 1,
    borderWidth: 1,
    borderColor: '#EE3026',
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 20,
    paddingRight: 20,
  },
  author: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: 'flex-start',
  },
  userPic: {
    height: 40,
    width: 40,
    marginRight: 10,
  },
  team: {
    fontWeight: '700',
    color: '#1f1f1f'
  },
  score: {
    color: '#1f1f1f',
    fontSize: 18,
    fontWeight: '700',
    paddingLeft: 5
  },
  standing: {
    fontSize: 8,
    color: '#999'
  },
  date: {
    fontWeight: '700',
    paddingTop: 5,
    paddingBottom: 5,
    color: '#1f1f1f'
  },
  dateLive: {
    fontWeight: '700',
    paddingTop: 5,
    paddingBottom: 5,
    color: '#EE3026'
  },
  venue: {
    fontWeight: '700',
    paddingTop: 5,
    paddingBottom: 5,
    color: '#999'
  },
  live: {
    color: 'red',
    fontWeight: '700',
    fontSize: 13,
  }
});
