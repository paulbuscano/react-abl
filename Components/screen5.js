import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,ImageBackground, StatusBar,ScrollView, Image, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class screen5 extends Component{

  static navigationOptions = ({ navigation }) => ( {
    headerBackground: (
      <Image
        style={{width: '100%', height: '100%'}}
        source={ require('./src/bg-header.jpg') }
      />
    ),
    headerStyle: {
      backgroundColor: 'transparent',
      ...Platform.select({
        ios: {     
        
        },
        android: {
          // height: 80,
          // paddingTop: 20
        },
      }),
    },
    headerTintColor: '#fff',
    title: 'MORE',
    headerTitleStyle: {
      fontWeight: 'bold',
      fontSize: 18,
      textAlign: 'center',
      flex: 1,
      ...Platform.select({
        ios: {     
        
        },
        android: {
         // marginTop: 200,
        },
      }),
    }, 
  });

  constructor(props) {
    super(props);
    this.state = {
      openingVideos: false,
      openingPlayers: false,
      openingStats: false,
      openingAbout: false,
      openingYoutube: false,
    }
  }

  // _openYoutube () {
  //   if (!this.state.openingYoutube) {
  //     this.props.navigation.push('Youtube')
  //     this.setState({ openingYoutube: true })
  //     setTimeout(() => {
  //       this.setState({ openingYoutube: false })
  //     }, 1000)
  //   }
  // }

  _openVideos () {
    if (!this.state.openingVideos) {
      this.props.navigation.push('Videos')
      this.setState({ openingVideos: true })
      setTimeout(() => {
        this.setState({ openingVideos: false })
      }, 1000)
    }
  }

  _openPlayers () {
    if (!this.state.openingPlayers) {
      this.props.navigation.push('Players')
      this.setState({ openingPlayers: true })
      setTimeout(() => {
        this.setState({ openingPlayers: false })
      }, 1000)
    }
  }

  _openStats () {
    if (!this.state.openingStats) {
      this.props.navigation.push('Leaders')
      this.setState({ openingStats: true })
      setTimeout(() => {
        this.setState({ openingStats: false })
      }, 1000)
    }
  }

  _openAbout () {
    if (!this.state.openingAbout) {
      this.props.navigation.push('About')
      this.setState({ openingAbout: true })
      setTimeout(() => {
        this.setState({ openingAbout: false })
      }, 1000)
    }
  }

  render() {
    return(
    <View style={styles.container}>
      <ScrollView>
        <TouchableOpacity onPress={this._openVideos.bind(this)}>
          <View style={ styles.row }>
            <View style={ styles.author }>
            <View  style={{  width: 50 + '%',  flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center',}}>
                <Icon style={ styles.moreIcon } name="youtube" size={40} color="#CE1A19" />
                <Text style={ styles.titlePage }>Videos</Text>   
            </View>
              <View  style={{  width: 50 + '%',flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center',}}>
                  <Icon name="chevron-right" size={30} color="#5a5a5a" />
              </View>  
            </View>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={this._openPlayers.bind(this)}>
        <View style={ styles.row }>
          <View style={ styles.author }>
          <View  style={{  width: 50 + '%',  flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center',}}>
              <Icon style={ styles.moreIcon } name="account-settings" size={40} color="#CE1A19" />
              <Text style={ styles.titlePage }>Players</Text>   
          </View>
            <View  style={{  width: 50 + '%',flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center',}}>
                <Icon name="chevron-right" size={30} color="#5a5a5a" />
            </View>  
          </View>
        </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={this._openStats.bind(this)}>
          <View style={ styles.row }>
            <View style={ styles.author }>
            <View  style={{  width: 50 + '%',  flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center',}}>
                <Icon style={ styles.moreIcon } name="chart-bar" size={40} color="#CE1A19" />
                <Text style={ styles.titlePage }>Stats Leaders</Text>   
            </View>
              <View  style={{  width: 50 + '%',flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center',}}>
                  <Icon name="chevron-right" size={30} color="#5a5a5a" />
              </View>  
            </View>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={this._openAbout.bind(this)}>
        <View style={ styles.row }>
          <View style={ styles.author }>
          <View  style={{  width: 50 + '%',  flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center',}}>
              <Icon style={ styles.moreIcon } name="basketball" size={40} color="#CE1A19" />
              <Text style={ styles.titlePage }>About ABL</Text>   
          </View>
                            <View  style={{  width: 50 + '%',flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center',}}>
                <Icon name="chevron-right" size={30} color="#5a5a5a" />
            </View>  
          </View>
        </View>
        </TouchableOpacity>
        {/* <TouchableOpacity onPress={this._openYoutube.bind(this)}>
        <View style={ styles.row }>
          <View style={ styles.author }>
          <View  style={{  width: 50 + '%',  flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center',}}>
              <Icon style={ styles.moreIcon } name="basketball" size={40} color="#CE1A19" />
              <Text style={ styles.titlePage }>Youtube</Text>   
          </View>
                            <View  style={{  width: 50 + '%',flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center',}}>
                <Icon name="chevron-right" size={30} color="#5a5a5a" />
            </View>  
          </View>
        </View>
        </TouchableOpacity> */}
      </ScrollView>
    </View>
    )
  }
  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#e3e3e3',
  },
  headerText: {
    ...Platform.select({
      ios: {     
        paddingBottom: 15,
        paddingTop: 30,
      },
      android: {
        padding: 17,
        paddingTop: 30,
      },
    }),
    fontSize: 18,
    color: 'white',
    fontWeight: '700',
    textAlign: 'center', 
  },
  header:  {
    backgroundColor: '#1f1f1f',
    alignItems: 'center',
    justifyContent: 'center',
  },
  row: {
    padding: 15,
    backgroundColor: 'white',
    marginBottom: 10,
    elevation:2,
    shadowOffset: { width: 2, height: 2 },
    shadowColor: "grey",
    shadowOpacity: 0.5,
    shadowRadius: 2,
  },
  author: {
    flexDirection: "row", 
    alignItems: "center",
    justifyContent: 'flex-start',
  },
  moreIcon: {
    marginRight: 10,
  },
  titlePage: {
    fontWeight: '700',
    color: '#1f1f1f'
  }
});
