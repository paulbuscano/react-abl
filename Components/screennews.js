import React, { Component } from 'react';
import { TouchableWithoutFeedback, Platform, StyleSheet, Text, View, ImageBackground, ScrollView, Image, StatusBar, Button, WebView, ActivityIndicator, TouchableOpacity, Linking, Clipboard, TouchableHighlight } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Modal from "react-native-modal";

export default class screennews extends Component {

  static navigationOptions = ({ navigation }) => ({
    headerBackground: (
      <Image
        style={{ width: '100%', height: '100%' }}
        source={require('./src/bg-header.jpg')}
      />
    ),
    headerStyle: {
      backgroundColor: 'transparent',
      ...Platform.select({
        ios: {

        },
        android: {
          // height: 80,
          // paddingTop: 20
        },
      }),
    },
    headerTintColor: '#fff',
    headerRight: <TouchableOpacity onPress={() => { navigation.state.params.openModal() }}><Icon style={{ paddingRight: 15 }} name="dots-horizontal" size={25} color="#fff" /></TouchableOpacity>,
    headerTitleStyle: {
      fontWeight: 'bold',
      ...Platform.select({
        ios: {

        },
        android: {
          // marginTop: 200,

        },
      }),
    },
  });

  constructor(props) {
    super(props)
    this.state = {
      slug: this.props.navigation.state.params.slug,
      visible: true,
      isModalVisible: false,
      opacity: 1,
      modalVisible: true,
    };
  }

  componentDidMount() {
    this.props.navigation.setParams({
      openModal: this._toggleModal.bind(this)
    });
    this.timeoutCheck = setTimeout(() => {
      this.toggleModalwaw();
    }, 5000);
  }

  toggleModalwaw = () => {
    this.setState({ modalVisible: false });
  };

  onBackPress() {
    this.props.nav.pop();
    return false;
  }


  onClipBoardCopy = () => {
    const sourceText = 'http://aseanbasketballleague.com/' + this.state.slug;
    Clipboard.setString(sourceText);
  }


  _toggleModal = () =>
    this.setState({ isModalVisible: !this.state.isModalVisible });


  hideSpinner() {
    this.setState({
      visible: false,
      opacity: 0,
    });
  }


  render() {

    let browser;

    if (Platform.OS === 'ios') {
      browser = 'Safari';
    } else {
      browser = 'Chrome';
    }

    return (

      <View style={styles.container}>
        {/* <StatusBar backgroundColor={'rgba(34, 34, 34, 0.80)'} translucent hidden={false} barStyle="light-content"/>           */}
        <Modal isVisible={this.state.modalVisible}>
          <View style={styles.screen}>
            <View style={styles.box}>
              <ActivityIndicator size="large" color="#000000" />
            </View>
          </View>
        </Modal>
        <WebView
          originWhitelist={['*']}
          source={{ uri: 'http://aseanbasketballleague.com/' + this.state.slug }}
          style={{ marginTop: 0, backgroundColor: '#000000', }}
          onLoad={() => this.hideSpinner()}
        // renderLoading={ ()=> {return(<ActivityIndicator size="large" color="#000" style = {{position: 'absolute',left: 0,right: 0, top: 0,bottom: 0,alignItems: 'center',justifyContent: 'center'}}/>)} }
        // startInLoadingState
        />
        {this.state.visible && (
          <ActivityIndicator
            animating={true}
            style={{ position: "absolute", top: 50 + '%', left: 45 + '%', alignItems: 'center', justifyContent: 'center', opacity: this.state.opacity }}
            size="large"
            color="#fff"
          />

        )}
        <Modal isVisible={this.state.isModalVisible}
          onBackdropPress={() => this.setState({ isModalVisible: false })}
          onBackButtonPress={() => this.setState({ isModalVisible: false })}
          backdropOpacity={0.1}
          animationIn="fadeIn"
          animationOut="fadeOut"
          animationInTiming={100}
          animationOutTiming={100}>
          <View style={{ flexDirection: 'row', justifyContent: 'flex-end', }}>
            <View style={styles.popUp}>
              <TouchableOpacity onPress={this.onClipBoardCopy}>
                <Text style={styles.text}>Copy Link</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => Linking.openURL('http://aseanbasketballleague.com/' + this.state.slug)}>
                <View style={styles.textBorder}>
                  <Text style={styles.text}>Open in {browser}</Text>
                </View>
              </TouchableOpacity>
              <Text style={[styles.text, { fontSize: 10 }]}>ABL Mobile Browser</Text>
            </View>
          </View>
        </Modal>
      </View>

    )

  }
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',

  },
  box: {
    width: 20 + '%',
    height: 10 + '%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
    borderRadius: 10,
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'center',
    width: 50 + '%',
  },
  popUp: {
    position: 'absolute',
    width: 50 + '%',
    backgroundColor: 'white',
    padding: 5,
    borderRadius: 5,
    elevation: 2,
    shadowOffset: { width: 2, height: 2 },
    shadowColor: "grey",
    shadowOpacity: 0.5,
    shadowRadius: 2,
    ...Platform.select({
      ios: {
        top: -300,
      },
      android: {
        top: -340,
      },
    }),
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    overflow: 'hidden',
  },
  text: {
    padding: 10,
  },
  textBorder: {
    borderBottomWidth: 1,
    borderBottomColor: '#e3e3e3'
  },
});