import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, ImageBackground, ScrollView, Image, StatusBar, Button, WebView, Dimensions, ActivityIndicator, Linking, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Flag from 'react-native-flags';
import moment from 'moment';

export default class playerinfo extends Component {

  static navigationOptions = ({ navigation }) => ({
    headerBackground: (
      <Image
        style={{ width: '100%', height: '100%' }}
        source={require('./src/bg-header.jpg')}
      />
    ),
    headerStyle: {
      backgroundColor: '#000000',
      ...Platform.select({
        ios: {

        },
        android: {
          // height: 80,
          // paddingTop: 20
        },
      }),
    },
    headerTintColor: '#fff',
    title: 'PLAYER INFO',
    headerTitleStyle: {
      fontWeight: 'bold',
      flex: 1,
      ...Platform.select({
        ios: {

        },
        android: {
          // marginTop: 200,

        },
      }),
    },
  });

  constructor(props) {
    super(props)
    this.state = {
      isLoading: true,
      playerid: this.props.navigation.state.params.playerid,
      teamid: this.props.navigation.state.params.teamid,
      playerInfo: [],
      teamInfo: [],
      statsInfo: [],
      historyInfo: [],
      teamHistoryInfo: [],
    };
  }

  componentDidMount() {
    this.getData();
  }

  getData = async () => {

    let comid = 26702;

    var player = 'https://api.wh.geniussports.com/v1/basketball/persons/' + this.state.playerid + '/linkages?ak=eebd8ae256142ac3fd24bd2003d28782&fields=images,defaultPlayingPosition,dob,height,weight,nationality,nickName,firstName,familyName,nationalityCode,defaultShirtNumber';
    var team = 'https://api.wh.geniussports.com/v1/basketball/teams/' + this.state.teamid + '?ak=eebd8ae256142ac3fd24bd2003d28782&fields=images,teamCode';
    var newStats = 'https://api.wh.geniussports.com/v1/basketball/statistics/personcompetition/competitions/' + comid + '/teams/' + this.state.teamid + '/persons/' + this.state.playerid + '?ak=eebd8ae256142ac3fd24bd2003d28782&fields=sPointsAverage,sReboundsTotalAverage,sAssistsAverage&limit=1';
    var history = 'https://api.wh.geniussports.com/v1/basketball/statistics/personcompetition/persons/' + this.state.playerid + '?ak=eebd8ae256142ac3fd24bd2003d28782&periodType=REGULAR&periodNumber=0&fields=year,teamId,sMinutesAverage,sPointsAverage,sAssistsAverage,sReboundsTotalAverage,sFieldGoalsPercentage,sBlocksAverage,sStealsAverage,sThreePointersPercentage,sFreeThrowsPercentage,sTurnoversAverage,sFoulsPersonalAverage';
    var teamHistory = 'https://api.wh.geniussports.com/v1/basketball/leagues/6/teams?ak=eebd8ae256142ac3fd24bd2003d28782&fields=teamId,teamCode&limit=100';

    return fetch(player).then((response) => response.json()).then((responseJson) => {
      this.setState({
        playerInfo: responseJson.response.data,
      });
    }).then(() => {
      return fetch(team).then((response) => response.json()).then((responseJson) => {
        this.setState({
          teamInfo: responseJson.response.data,
        });
      }).catch((error) => {
        console.error(error);
      });
    }).then(() => {
      return fetch(newStats).then((response) => response.json()).then((responseJson) => {
        this.setState({
          statsInfo: responseJson.response.data,
        });
      }).catch((error) => {
        console.error(error);
      });
    }).then(() => {
      return fetch(history).then((response) => response.json()).then((responseJson) => {
        let people;
        function sortByKey(array, key) {
          return array.sort(function (a, b) {
            var x = a[key]; var y = b[key];
            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
          });
        }

        people = sortByKey(responseJson.response.data, 'year');

        this.setState({
          historyInfo: people,
        });
      }).catch((error) => {
        console.error(error);
      });
    }).then(() => {
      return fetch(teamHistory).then((response) => response.json()).then((responseJson) => {
        this.setState({
          isLoading: false,
          teamHistoryInfo: responseJson.response.data,
        });
      }).catch((error) => {
        console.error(error);
      });
    }).catch((error) => {
      console.error(error);
    });
  }

  renderRow = ({ item, index }) => {

    let fullPosition;
    let nationalityTest;
    let nationalityFinal;

    if (item.defaultPlayingPosition == 'F') {
      fullPosition = 'FORWARD';
    }
    if (item.defaultPlayingPosition == 'G') {
      fullPosition = 'GUARD';
    }
    if (item.defaultPlayingPosition == 'C') {
      fullPosition = 'CENTER';
    }

    let playerBirthdate = moment(item.dob).format('ll');

    let realFeet = ((item.height * 0.393700) / 12);
    let feet = Math.floor(realFeet);
    let inches = Math.round((realFeet - feet) * 12);

    if (item.nationality == 'TAIWAN, PROVINCE OF CHINA') {
      nationalityTest = 'Taiwan';
      nationalityFinal = nationalityTest.charAt(0).toUpperCase() + nationalityTest.slice(1);
    } else {
      nationalityTest = item.nationality.toLowerCase();
      nationalityFinal = nationalityTest.charAt(0).toUpperCase() + nationalityTest.slice(1);
    }



    let testSocial = item.nickName.split("-");

    let finalFacebook = testSocial[0];
    let finalTwitter = testSocial[1];
    let finalInstagram = testSocial[2];

    let convertedValuePoints;
    let convertedValueRebounds;
    let convertedValueAssists;

    if (this.state.statsInfo.length > 0) {
      let valueTestPoints = this.state.statsInfo[0].sPointsAverage.toString();
      convertedValuePoints = valueTestPoints.substring(0, 4);

      let valueTestRebounds = this.state.statsInfo[0].sReboundsTotalAverage.toString();
      convertedValueRebounds = valueTestRebounds.substring(0, 4);

      let valueTestAssists = this.state.statsInfo[0].sAssistsAverage.toString();
      convertedValueAssists = valueTestAssists.substring(0, 4);
    } else {
      convertedValuePoints = '-';
      convertedValueRebounds = '-';
      convertedValueAssists = '-';
    }




    let year = this.state.historyInfo.map((val, key) => {

      let yearPlus = val.year + 1;
      let yeatEndTest = yearPlus.toString();
      let yearEnd = yeatEndTest.substring(2, 4);

      let yeatTest = val.year.toString();
      let yearFirst = yeatTest.substring(2, 4);

      return <View key={key}>
        <Text style={styles.recordsLabelOne}>{yearFirst}-{yearEnd}</Text>
      </View>
    });

    let teamCodeSection = this.state.historyInfo.map((val, key) => {

      let standings = this.state.teamHistoryInfo.map((ey, key) => {
        if (val.teamId == ey.teamId) {
          teamCodeFinal = ey.teamCode;
        }
      });

      return <View key={key}>
        <Text style={styles.recordsScoreStatsDiv}>{teamCodeFinal}</Text>
      </View>
    });

    let minutes = this.state.historyInfo.map((val, key) => {

      let minutesTest = val.sMinutesAverage.toString();
      let minutesFinal = minutesTest.substring(0, 4);

      return <View key={key}>
        <Text style={styles.recordsScoreStats}>{minutesFinal}</Text>
      </View>
    });

    let points = this.state.historyInfo.map((val, key) => {

      let pointsTest = val.sPointsAverage.toString();
      let pointsFinal = pointsTest.substring(0, 4);

      return <View key={key}>
        <Text style={styles.recordsScoreStats}>{pointsFinal}</Text>
      </View>
    });

    let assists = this.state.historyInfo.map((val, key) => {

      let assistsTest = val.sAssistsAverage.toString();
      let assistsFinal = assistsTest.substring(0, 4);

      return <View key={key}>
        <Text style={styles.recordsScoreStats}>{assistsFinal}</Text>
      </View>
    });

    let rebounds = this.state.historyInfo.map((val, key) => {

      let reboundsTest = val.sReboundsTotalAverage.toString();
      let reboundsFinal = reboundsTest.substring(0, 4);

      return <View key={key}>
        <Text style={styles.recordsScoreStats}>{reboundsFinal}</Text>
      </View>
    });

    let fieldgoals = this.state.historyInfo.map((val, key) => {

      let fieldFinal;

      if (val.sFieldGoalsPercentage == 0) {
        fieldFinal = 0;
      } else {
        if (val.sFieldGoalsPercentage == 1) {
          fieldFinal = 100;
        } else {
          let fieldTest = val.sFieldGoalsPercentage.toString();
          if (fieldTest.length == 3) {
            fieldFinal = fieldTest.substr(2, 2) + '0';
          } else {
            fieldFinal = fieldTest.substr(2, 2);
          }

        }
      }


      return <View key={key}>
        <Text style={styles.recordsScoreStats}>{fieldFinal}</Text>
      </View>
    });

    let blocks = this.state.historyInfo.map((val, key) => {

      let blocksTest = val.sBlocksAverage.toString();
      let blocksFinal = blocksTest.substring(0, 4);

      return <View key={key}>
        <Text style={styles.recordsScoreStats}>{blocksFinal}</Text>
      </View>
    });

    let steals = this.state.historyInfo.map((val, key) => {

      let stealsTest = val.sStealsAverage.toString();
      let stealsFinal = stealsTest.substring(0, 4);

      return <View key={key}>
        <Text style={styles.recordsScoreStats}>{stealsFinal}</Text>
      </View>
    });

    let threepercentage = this.state.historyInfo.map((val, key) => {

      let threeFinal;

      if (val.sThreePointersPercentage == 0) {
        threeFinal = 0;
      } else {
        if (val.sThreePointersPercentage == 1) {
          threeFinal = 100;
        } else {
          let threeTest = val.sThreePointersPercentage.toString();
          if (threeTest.length == 3) {
            threeFinal = threeTest.substr(2, 2) + '0';
          } else {
            threeFinal = threeTest.substr(2, 2);
          }
        }
      }

      return <View key={key}>
        <Text style={styles.recordsScoreStats}>{threeFinal}</Text>
      </View>
    });

    let freeThrowPercentage = this.state.historyInfo.map((val, key) => {

      let freeThrowPercentageFinal;

      if (val.sFreeThrowsPercentage == 0) {
        freeThrowPercentageFinal = 0;
      } else {
        if (val.sFreeThrowsPercentage == 1) {
          freeThrowPercentageFinal = 100;
        } else {
          let freeThrowPercentageTest = val.sFreeThrowsPercentage.toString();
          if (freeThrowPercentageTest.length == 3) {
            freeThrowPercentageFinal = freeThrowPercentageTest.substr(2, 2) + '0';
          } else {
            freeThrowPercentageFinal = freeThrowPercentageTest.substr(2, 2);
          }
        }

      }

      return <View key={key}>
        <Text style={styles.recordsScoreStats}>{freeThrowPercentageFinal}</Text>
      </View>
    });

    let turnovers = this.state.historyInfo.map((val, key) => {

      let turnoversTest = val.sTurnoversAverage.toString();
      let turnoversFinal = turnoversTest.substring(0, 4);

      return <View key={key}>
        <Text style={styles.recordsScoreStats}>{turnoversFinal}</Text>
      </View>
    });

    let personalFouls = this.state.historyInfo.map((val, key) => {

      let personalTest = val.sFoulsPersonalAverage.toString();
      let personalFinal = personalTest.substring(0, 4);

      return <View key={key}>
        <Text style={styles.recordsScoreStats}>{personalFinal}</Text>
      </View>
    });

    //career average
    var totalMinutes = 0;
    for (var i = 0; i < this.state.historyInfo.length; i++) {
      totalMinutes += this.state.historyInfo[i].sMinutesAverage;
    }
    let avgMinutes = totalMinutes / this.state.historyInfo.length;
    let postMinutes = avgMinutes.toString();
    let avgMinutesFinal = postMinutes.substring(0, 4);

    var totalPoints = 0;
    for (var i = 0; i < this.state.historyInfo.length; i++) {
      totalPoints += this.state.historyInfo[i].sPointsAverage;
    }
    let avgPoints = totalPoints / this.state.historyInfo.length;
    let postPoints = avgPoints.toString();
    let avgPointsFinal = postPoints.substring(0, 4);

    var totalAssists = 0;
    for (var i = 0; i < this.state.historyInfo.length; i++) {
      totalAssists += this.state.historyInfo[i].sAssistsAverage;
    }
    let avgAssists = totalAssists / this.state.historyInfo.length;
    let postAssists = avgAssists.toString();
    let avgAssistsFinal = postAssists.substring(0, 4);

    var totalRebounds = 0;
    for (var i = 0; i < this.state.historyInfo.length; i++) {
      totalRebounds += this.state.historyInfo[i].sReboundsTotalAverage;
    }
    let avgRebounds = totalRebounds / this.state.historyInfo.length;
    let postRebounds = avgRebounds.toString();
    let avgReboundsFinal = postRebounds.substring(0, 4);

    var totalFg = 0;
    for (var i = 0; i < this.state.historyInfo.length; i++) {
      totalFg += this.state.historyInfo[i].sFieldGoalsPercentage;
    }
    let avgFg = totalFg / this.state.historyInfo.length;
    let postFg = avgFg.toString();
    let avgFgFinal = postFg.substring(2, 4);
    let avgFgFinalNa;

    if (avgFgFinal.length == 1) {
      avgFgFinalNa = avgFgFinal + '0';
    } else if (avgFgFinal.length == 0) {
      avgFgFinalNa = 0;
    } else {
      avgFgFinalNa = avgFgFinal;
    }

    var totalBlocks = 0;
    for (var i = 0; i < this.state.historyInfo.length; i++) {
      totalBlocks += this.state.historyInfo[i].sBlocksAverage;
    }
    let avgBlocks = totalBlocks / this.state.historyInfo.length;
    let postBlocks = avgBlocks.toString();
    let avgBlocksFinal = postBlocks.substring(0, 4);

    var totalSteals = 0;
    for (var i = 0; i < this.state.historyInfo.length; i++) {
      totalSteals += this.state.historyInfo[i].sStealsAverage;
    }
    let avgSteals = totalSteals / this.state.historyInfo.length;
    let postSteals = avgSteals.toString();
    let avgStealsFinal = postSteals.substring(0, 4);

    var totalThreeFg = 0;
    for (var i = 0; i < this.state.historyInfo.length; i++) {
      totalThreeFg += this.state.historyInfo[i].sThreePointersPercentage;
    }
    let avgThreeFg = totalThreeFg / this.state.historyInfo.length;
    let postThreeFg = avgThreeFg.toString();
    let avgFgThreeFinal = postThreeFg.substring(2, 4);
    let avgFgThreeFinalNa;

    if (avgFgThreeFinal.length == 1) {
      avgFgThreeFinalNa = avgFgThreeFinal + '0';
    } else if (avgFgThreeFinal.length == 0) {
      avgFgThreeFinalNa = 0;
    } else {
      avgFgThreeFinalNa = avgFgThreeFinal;
    }

    var totalFreeThrowFg = 0;
    for (var i = 0; i < this.state.historyInfo.length; i++) {
      totalFreeThrowFg += this.state.historyInfo[i].sFreeThrowsPercentage;
    }
    let avgFreeThrowFg = totalFreeThrowFg / this.state.historyInfo.length;
    let postFreeThrowFg = avgFreeThrowFg.toString();
    let avgFgFreeThrowFinal = postFreeThrowFg.substring(2, 4);
    let avgFgFreeThrowFinalNa;

    if (avgFgFreeThrowFinal.length == 1) {
      avgFgFreeThrowFinalNa = avgFgFreeThrowFinal + '0';
    } else if (avgFgFreeThrowFinal.length == 0) {
      avgFgFreeThrowFinalNa = 0;
    } else {
      avgFgFreeThrowFinalNa = avgFgFreeThrowFinal;
    }

    var totalTurnover = 0;
    for (var i = 0; i < this.state.historyInfo.length; i++) {
      totalTurnover += this.state.historyInfo[i].sTurnoversAverage;
    }
    let avgTurnover = totalTurnover / this.state.historyInfo.length;
    let postTurnover = avgTurnover.toString();
    let avgTurnoverFinal = postTurnover.substring(0, 4);

    var totalPersonalFoul = 0;
    for (var i = 0; i < this.state.historyInfo.length; i++) {
      totalPersonalFoul += this.state.historyInfo[i].sFoulsPersonalAverage;
    }
    let avgPersonalFoul = totalPersonalFoul / this.state.historyInfo.length;
    let postPersonalFoul = avgPersonalFoul.toString();
    let avgPersonalFoulFinal = postPersonalFoul.substring(0, 4);

    let fbLink;
    let instagramlink;

    if (Platform.OS === 'ios') {
      fbLink = <Icon onPress={() => Linking.openURL('fb://profile/' + finalFacebook)} name="facebook" size={25} color="#000" />
      instagramlink = <Icon onPress={() => Linking.openURL('instagram://user?username=' + finalInstagram)} name="instagram" size={25} color="#000" />
    } else {
      fbLink = <Icon onPress={() => Linking.openURL('fb://profile/' + finalFacebook)} name="facebook" size={25} color="#000" />
      instagramlink = <Icon onPress={() => Linking.openURL('http://instagram.com/_u/' + finalInstagram)} name="instagram" size={25} color="#000" />
    }

    let convertedLastNameFinal; 
    let lastNameTest = item.familyName.toLowerCase();
    let convertedLastName = lastNameTest.toUpperCase();

    return <View>
      <View style={[styles.row, { backgroundColor: '#1f1f1f' }]}>
        <View style={{ width: 50 + '%', height: 150, flexDirection: 'column', justifyContent: 'center', }}>
          <Image
            style={styles.teamPic}
            source={{
              uri: this.state.teamInfo.images.logo.S1.url
            }}
          />
          <Text style={styles.teamName}>{item.firstName.toUpperCase()}</Text>
          <Text style={styles.teamName}>{convertedLastName}</Text>
          <Text style={styles.standing}>#{item.defaultShirtNumber} &#8226; {fullPosition} &#8226; {this.state.teamInfo.teamCode}</Text>
        </View>
        <View style={{ width: 50 + '%', height: 150, flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
          <Image
            style={styles.playerPic}
            source={{
              uri: item.images.photo.S1.url
            }}
          />
        </View>
      </View>

      <View style={styles.group}>
        <View style={styles.rowInfoBox}>
          <View style={{ width: 33 + '%', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', paddingRight: 5, borderRightWidth: 1, borderRightColor: '#e3e3e3' }}>
            <Text style={styles.recordsLabel}>PPG</Text>
            <Text style={styles.mainStatsValue}>{convertedValuePoints}</Text>
          </View>
          <View style={{ width: 33 + '%', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', paddingLeft: 5 }}>
            <Text style={styles.recordsLabel}>RPG</Text>
            <Text style={styles.mainStatsValue}>{convertedValueRebounds}</Text>
          </View>
          <View style={{ width: 33 + '%', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', paddingLeft: 5, borderLeftWidth: 1, borderLeftColor: '#e3e3e3' }}>
            <Text style={styles.recordsLabel}>APG</Text>
            <Text style={styles.mainStatsValue}>{convertedValueAssists}</Text>
          </View>
        </View>
      </View>
      <View style={styles.group}>
        <View style={styles.rowLabel}>
          <Text style={styles.label}>INFO</Text>
        </View>
        <View style={styles.rowInfo}>
          <View style={{ width: 30 + '%', flexDirection: 'column', justifyContent: 'center', }}>
            <Text style={styles.infoLabel}>Birthday: </Text>
          </View>
          <View style={{ width: 70 + '%', flexDirection: 'column', justifyContent: 'center', }}>
            <Text style={styles.value}> {playerBirthdate}</Text>
          </View>
        </View>
        <View style={styles.rowInfo}>
          <View style={{ width: 30 + '%', flexDirection: 'column', justifyContent: 'center', }}>
            <Text style={styles.infoLabel}>Height: </Text>
          </View>
          <View style={{ width: 70 + '%', flexDirection: 'column', justifyContent: 'center', }}>
            <Text style={styles.value}> {feet}'{inches}"</Text>
          </View>
        </View>
        <View style={styles.rowInfo}>
          <View style={{ width: 30 + '%', flexDirection: 'column', justifyContent: 'center', }}>
            <Text style={styles.infoLabel}>Weight: </Text>
          </View>
          <View style={{ width: 70 + '%', flexDirection: 'column', justifyContent: 'center', }}>
            <Text style={styles.value}> {item.weight} lbs.</Text>
          </View>
        </View>
        <View style={styles.rowInfo}>
          <View style={{ width: 30 + '%', flexDirection: 'column', justifyContent: 'center', }}>
            <Text style={styles.infoLabel}>Nationality: </Text>
          </View>
          <View style={{ width: 70 + '%', flexDirection: 'column', justifyContent: 'center', }}>
            <Text style={styles.value}> <Flag code={item.nationalityCode} size={16} type="flat" /> {nationalityFinal}</Text>
          </View>
        </View>
        <View style={styles.rowInfoBox}>
          <View style={{ width: 33.3 + '%', flexDirection: 'row', justifyContent: 'center', }}>
            {fbLink}
          </View>
          <View style={{ width: 33.3 + '%', flexDirection: 'row', justifyContent: 'center', }}>
            <Icon onPress={() => Linking.openURL('https://twitter.com/' + finalTwitter)} name="twitter" size={25} color="#000" />
          </View>
          <View style={{ width: 33.3 + '%', flexDirection: 'row', justifyContent: 'center', }}>
            {instagramlink}
          </View>
        </View>
      </View>

      <View style={styles.group}>
        <View style={styles.rowLabel}>
          <Text style={styles.label}>CAREER STATS</Text>
        </View>

        <View style={styles.rowView}>
          <View style={styles.columnView}>
            <Text style={styles.recordsLabelStatsOne}>YEAR</Text>
            {year}
            <Text style={styles.recordsLabelOne}>CAR</Text>
          </View>
          <View style={styles.columnViewStatsTeam}>
            <Text style={styles.recordsLabelStatsDiv}>TEAM</Text>
            {teamCodeSection}
            <Text style={styles.recordsScoreStatsDiv}></Text>
          </View>
          <ScrollView horizontal={true}>
            <View style={styles.columnViewStats}>
              <Text style={styles.recordsLabelStats}>MIN</Text>
              {minutes}
              <Text style={styles.recordsScoreStatsCar}>{avgMinutesFinal}</Text>
            </View>
            <View style={styles.columnViewStats}>
              <Text style={styles.recordsLabelStats}>PTS</Text>
              {points}
              <Text style={styles.recordsScoreStatsCar}>{avgPointsFinal}</Text>
            </View>
            <View style={styles.columnViewStats}>
              <Text style={styles.recordsLabelStats}>AST</Text>
              {assists}
              <Text style={styles.recordsScoreStatsCar}>{avgAssistsFinal}</Text>
            </View>
            <View style={styles.columnViewStats}>
              <Text style={styles.recordsLabelStats}>REB</Text>
              {rebounds}
              <Text style={styles.recordsScoreStatsCar}>{avgReboundsFinal}</Text>
            </View>
            <View style={styles.columnViewStats}>
              <Text style={styles.recordsLabelStats}>FG%</Text>
              {fieldgoals}
              <Text style={styles.recordsScoreStatsCar}>{avgFgFinalNa}</Text>
            </View>
            <View style={styles.columnViewStats}>
              <Text style={styles.recordsLabelStats}>BLK</Text>
              {blocks}
              <Text style={styles.recordsScoreStatsCar}>{avgBlocksFinal}</Text>
            </View>
            <View style={styles.columnViewStats}>
              <Text style={styles.recordsLabelStats}>STL</Text>
              {steals}
              <Text style={styles.recordsScoreStatsCar}>{avgStealsFinal}</Text>
            </View>
            <View style={styles.columnViewStats}>
              <Text style={styles.recordsLabelStats}>3PT%</Text>
              {threepercentage}
              <Text style={styles.recordsScoreStatsCar}>{avgFgThreeFinalNa}</Text>
            </View>
            <View style={styles.columnViewStats}>
              <Text style={styles.recordsLabelStats}>FT%</Text>
              {freeThrowPercentage}
              <Text style={styles.recordsScoreStatsCar}>{avgFgFreeThrowFinalNa}</Text>
            </View>
            <View style={styles.columnViewStats}>
              <Text style={styles.recordsLabelStats}>TO</Text>
              {turnovers}
              <Text style={styles.recordsScoreStatsCar}>{avgTurnoverFinal}</Text>
            </View>
            <View style={styles.columnViewStats}>
              <Text style={styles.recordsLabelStats}>PF</Text>
              {personalFouls}
              <Text style={styles.recordsScoreStatsCar}>{avgPersonalFoulFinal}</Text>
            </View>
          </ScrollView>
        </View>
      </View>
    </View>
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={styles.containerLoading}>
          <ActivityIndicator size="large" color="#fff" />
        </View>
      )
    } else {

      return (

        <View style={styles.container}>
          <FlatList
            style={styles.container}
            data={this.state.playerInfo}
            renderItem={this.renderRow}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>

      )
    }// end else
  }// end render
}
const styles = StyleSheet.create({
  containerLoading: {
    flex: 1,
    backgroundColor: '#000000',
    justifyContent: 'center',
  },
  rowView: {
    flexDirection: 'row',
    paddingLeft: 15,
    paddingRight: 15,
    paddingBottom: 10,
    paddingTop: 10,
  },
  columnView: {
    flexDirection: 'column',
  },
  columnViewStats: {
    flexDirection: 'column',
    paddingLeft: 15,
    paddingRight: 15,
  },
  columnViewStatsTeam: {
    flexDirection: 'column',
    paddingLeft: 15,
    paddingRight: 15,
    borderRightColor: '#e3e3e3',
    borderRightWidth: 1,
  },
  mainStatsValue: {
    fontSize: 22,
    fontWeight: '700',
    color: '#ee3026'
  },
  playerPic: {
    borderRadius: 65,
    borderWidth: 0.5,
    borderColor: '#e3e3e3',
    width: 130,
    height: 130,
    overflow: 'hidden',
  },
  leadersScore: {
    fontSize: 18,
    color: '#1f1f1f'
  },
  subLabel: {
    fontSize: 15,
    fontWeight: '700',
    color: '#999'
  },
  recordsScore: {
    color: '#1f1f1f'
  },
  recordsScoreStats: {
    color: '#1f1f1f',
    paddingBottom: 10,
    paddingTop: 10,
    textAlign: 'center',
  },
  recordsScoreStatsCar: {
    color: '#1f1f1f',
    paddingBottom: 10,
    paddingTop: 10,
    textAlign: 'center',
    fontWeight: '700',
  },
  recordsScoreStatsDiv: {
    color: '#1f1f1f',
    paddingBottom: 10,
    paddingTop: 10,
    textAlign: 'center',
  },
  recordsLabel: {
    color: '#1f1f1f',
    fontWeight: '700',
    paddingBottom: 10,
    paddingTop: 10,
  },
  recordsLabelOne: {
    color: '#1f1f1f',
    fontWeight: '700',
    paddingBottom: 10,
    paddingTop: 10,
  },
  recordsLabelStats: {
    color: '#1f1f1f',
    fontWeight: '700',
    paddingBottom: 10,
    paddingTop: 10,
    textAlign: 'center',
    width: 100 + '%',
  },
  recordsLabelStatsDiv: {
    color: '#1f1f1f',
    fontWeight: '700',
    paddingBottom: 10,
    paddingTop: 10,
    textAlign: 'center',

  },
  recordsLabelStatsOne: {
    color: '#1f1f1f',
    fontWeight: '700',
    paddingBottom: 10,
    paddingTop: 10,
  },
  rowLabel: {
    flexDirection: 'row',
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 10,
  },
  rowInfo: {
    flexDirection: 'row',
    paddingLeft: 15,
    paddingRight: 15,
    paddingBottom: 10,
    paddingTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#e3e3e3'
  },
  rowInfoBox: {
    flexDirection: 'row',
    paddingLeft: 15,
    paddingRight: 15,
    paddingBottom: 10,
    paddingTop: 10,
  },
  value: {
    color: '#000'
  },
  infoLabel: {
    color: '#000',
    fontWeight: '700'
  },
  group: {
    backgroundColor: "white",
    elevation: 2,
    shadowOffset: { width: 2, height: 2 },
    shadowColor: "grey",
    shadowOpacity: 0.5,
    shadowRadius: 2,
    marginBottom: 10,
  },
  label: {
    fontWeight: '700',
    color: '#1f1f1f',
    fontSize: 18
  },
  standing: {
    color: '#fff'
  },
  teamName: {
    color: '#ffffff',
    fontSize: 18,
    fontWeight: '700'
  },
  row: {
    flexDirection: 'row',
    padding: 15,
  },
  container: {
    flex: 1,
    backgroundColor: '#e3e3e3',
  },
  linearGradient: {
    backgroundColor: "transparent",
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0
  },
  teamPic: {
    width: 60,
    height: 60
  }
});